<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/1/2015
 * Time: 10:44 PM

     Sản phẩm mới */
?>
<div class="mc-feturepro">
    <div class="module-main">
        <?php $cate = get_term_by('id', 20, 'product_cat'); ?>
        <a href="<?php echo esc_url( get_term_link(20,'product_cat' ) );; ?>"><h2><?php echo $cate->name ?></h2></a>

        <div class="row">
            <?php
            $agrs = array(
                'post_type' => 'product',
                'product_cat' => 'san-pham-tieu-bieu',
                'order' => 'desc',
                'showposts'=> 8,
                'orderby' => 'rand'
            );
            $loop = new WP_Query($agrs);
            if ($loop->have_posts()) {
                while ($loop->have_posts()): $loop->the_post();
                    global $product;
                    ?>
                    <div class="col-3-x col-2-m">
                        <div class="item">
                            <a href="<?php the_permalink();?>"><?php echo woocommerce_get_product_thumbnail(array(100,100)); ?> </a>
                            <h4><a href="<?php the_permalink();?>" title="<?php echo the_title();?>"><?php echo wp_trim_words(get_the_title(), 8)?></a></h4>

                            <p class="price">
                                <?php if (($product->get_price_html() == null)) {
                                    echo "<span class='txt-b'>Liên hệ </span>";
                                } else {
                                    echo $product->get_price_html();
                                } ?>
                            </p>
                            <div class="mb-show woocommerce mb-20">
                                <?php  GLOBAL $product; echo '<div class="star-rating-container aggregate">' . $product->get_rating_html() . '</div>';?>
                            </div>
                            <p class="add-cart">
                                <a class="btn-add-cart" href="<?php echo $product->add_to_cart_url() ?>">
                                    <i class="fa fa-cart-arrow-down"></i>Mua hàng</a>
                            </p>
                        </div>
                    </div>
                <?php endwhile;
            } else {
                echo __('Sản phẩm đang được cập nhật');
            }
            ?>
        </div>
    </div>
</div>

