<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/1/2015
 * Time: 3:14 PM
 */ ?>
<div class="mc-news">
    <div class="module-main">
        <a href="<?php echo get_category_link(6); ?>"> <h2>Tin mới</h2></a>
        <div class="row">
            <?php
            global $post;
            $array = array('category_name' => 'goc-chia-se', 'orderrand' => 'desc', 'numberposts' => '6');
            $result = get_posts($array);
            if (have_posts()) {
                foreach ($result as $post): setup_postdata($post);
                    ?>
                    <div class="col-4-x col-2-m col-2-x">
                        <div class="items">
                            <p class="img-l"><a href="<?php echo the_permalink() ?>"><?php echo the_post_thumbnail(array(60,60)); ?></a></p>
                            <h4><a href="<?php echo the_permalink() ?>" title="<?php echo the_title() ?>"><?php echo wp_trim_words(get_the_title(),7) ?></a></h4>
                        </div>
                    </div>
                <?php endforeach;
            } ?>
        </div>
    </div>
</div>
