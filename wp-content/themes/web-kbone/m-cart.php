<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 10/28/2015
 * Time: 9:09 PM
 */ ?>
<div class="m-cart">
    <div class="module">
        <a href="<?php echo WC()->cart->get_cart_url(); ?>"><h3>Giỏ hàng của bạn</h3></a>
        <div class="m-content">
            <a href="<?php echo WC()->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart fa-2e"></i></a>
            <p>Số sản phẩm: <?php
                global $woocommerce;
                echo $woocommerce->cart->cart_contents_count; ?></p>
          
            <div>Thành tiền:
                <p class="center"><?php echo WC()->cart->get_cart_total(); ?></p>
            </div>
            <a href="<?php echo WC()->cart->get_cart_url(); ?>">Xem giỏ hàng</a>
        </div>
    </div>
</div>
