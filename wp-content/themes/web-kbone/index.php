<?php get_header() ?>

<section class="content">
  <div class="row">
    <div class="container">
      <div class="col-176 module-left col-hidden">
        <?php include 'm-product-hot.php' ?>
        <?php include 'm-product-sale.php' ?>
        <?php include 'm-kbone.php' ?>
        <?php include 'm-contact-us.php' ?>
      </div>
      <div class="col-598">
        <?php include 'mc-new-product.php' ?>
        <?php include 'mc-feture-product.php' ?>
        <?php include 'mc-perfume.php' ?>
        <?php include 'mc-new.php' ?>
      </div>
      <div class="col-176 module-right col-hidden">
        <?php include 'm-cart.php' ?>
        <?php include 'm-support.php' ?>
        <?php include 'm-product-best.php' ?>
        <?php include'm-facebook.php'?>
      </div>
    </div>
    <?php include 'slide-footer.php' ?>
  </div>
</section>
<?php get_footer() ?>
