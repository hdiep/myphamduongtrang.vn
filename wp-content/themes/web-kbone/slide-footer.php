<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/1/2015
 * Time: 8:04 PM
 */
?>

<div class="container slider-footer">
    <div class="col-12-x">
        <ul class="slider1">
            <?php
            $agrs = array(
                'category_name' => 'slide-footer',
                'order' => 'desc',
                'numberposts' => '10'
            );
            $loop = new WP_Query($agrs);
            if ($loop->have_posts()) {
                while ($loop->have_posts()): $loop->the_post();

                    ?>
                    <li><a href="<?php echo get_field('slide_url') ?>"> <?php echo the_post_thumbnail(array(100)); ?></a></li>
                <?php endwhile;
            } else {
                echo __('San pham dang duoc cap nhat');
            }
            ?>
        </ul>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.slider1').bxSlider({
            auto: true,
            slideWidth: 100,
            minSlides: 3,
            maxSlides: 13,
            slideMargin: 10,
            pager: false
        });
    });
</script>

