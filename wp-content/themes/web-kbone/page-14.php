<?php

/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/4/2015
 * Time: 10:37 PM
 */

get_header() ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"
        type="text/javascript"></script>
<script type="text/javascript">
    var conlic = jQuery.noConflict();

    conlic(document).ready(function ($) {
        conlic("ul.pagination1").quickPagination({
            pageSize: "9",
            currentPage: 1,
            holder: null,
            pagerLocation: "after"

        });
    });

</script>

<section class="content">
    <div class="row">
        <div class="container">
            <div class="col-176 module-left col-hidden">
                <?php include 'm-product-hot.php' ?>
                <?php include 'm-product-best.php' ?>
                <?php include 'm-facebook.php' ?>
            </div>
            <div class="col-520 categories">
                <div class="module-main">
                    <?php
                    $object = $wp_query->get_queried_object();

                    $parent_id = $object->category_parent;
                    $cat_breadcrumbs = '';
                    while ($parent_id) {
                        $category = get_category($parent_id);
                        $cat_breadcrumbs = '<a href="' . get_category_link($category->cat_ID) . '" class="home">' . $category->cat_name . '</a> &raquo; ' . $cat_breadcrumbs;

                        $parent_id = $category->category_parent;
                    }
                    $result = $cat_breadcrumbs . $object->cat_name;
                    ?>

                    <h2>
                        <a href="<?php bloginfo('home'); ?>" class="homes"><?php _e('Home'); ?></a> &raquo; <?php echo $result; ?>

                    </h2>
                    <ul class="list pagination1">
                        <?php
                        while (have_posts()) : the_post(); ?>
                            <li>
                                <a class="img-l"
                                   href="<?php echo the_permalink() ?>"><?php echo the_post_thumbnail(array(120, 90)); ?></a>

                                <div class="desc">
                                    <h4>
                                        <a href="<?php echo the_permalink() ?>"> <?php echo wp_trim_words(get_the_title(), 17) ?></a>
                                    </h4>
                                    <div class="short-content"> <?php echo wp_trim_words(get_the_excerpt(), 26); ?></div>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
            <div class="col-249 right"><?php include 'm-new-hot.php' ?></div>
        </div>
        <div class="row">
            <?php include 'slide-footer.php' ?>
        </div>
    </div>
</section>
<?php get_footer() ?>