<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 10/28/2015
 * Time: 10:16 PM
 */?>
<div class="m-facebook">
    <div class="module">
        <a href="https://www.facebook.com/kbone.and.zoley"> <h3>Liên kết facebook</h3></a>
        <div class="m-content txt-center">
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-page" data-href="https://www.facebook.com/kbone.and.zoley" data-width="165" data-height="240" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/kbone.and.zoley/"><a href="https://www.facebook.com/kbone.and.zoley/">Zoley &amp; KBone</a></blockquote></div></div>
        </div>
    </div>
</div>
