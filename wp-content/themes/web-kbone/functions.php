<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 3/8/14
 * Time: 3:29 PM
 */

if (!function_exists('kbone_setup')) :

    function kbone_setup()
    {

        add_theme_support('woocommerce'); //Your theme does not declare WooCommerce support

//END
//BEGIN Dang ky menu
        function register_my_menus()
        {
            register_nav_menus(
                array(
                    'category-menu' => __('Category menu'),
                    'extra-menu' => __('Extra Menu')
                )
            );
        }

        add_action('init', 'register_my_menus'); //END register menus


//get form search
        add_filter('get_product_search_form', 'woo_custom_product_searchform');

        function woo_custom_product_searchform($form)
        {

            $form = '<form class="frm-search mb-hidden" method="get" id="searchform" action="' . esc_url(home_url('/')) . '">

			<label class="screen-reader-text" for="s">' . __('', 'woocommerce') . '</label>
			<input type="text" title="Nhập từ khóa tìm" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __('Tìm kiếm', 'woocommerce') . '" />

			<input type="hidden" name="post_type" value="product" />
			<button><i class="fa fa-search"></i></button>

	</form>';

            return $form;
        }

//form button add to c?t
        function my_woocommerce_template_loop_add_to_cart()
        {
            global $product;
            echo '<form action="' . esc_url($product->get_permalink($product->id)) . '" class="cart" method="post" enctype="multipart/form-data">
                        <div class="quantity">
                            <input name="quantity" data-min="1" data-max="" value="1" size="4" title="Qty" class="input-text qty text" maxlength="12" />
                        </div>
                        <button type="submit" class="single_add_to_cart_button button alt">Add to cart</button>
                    </form>';
        }

//thay doi text button add to cart
        add_filter('add_to_cart_text', 'woo_custom_cart_button_text'); // < 2.1

        add_filter('woocommerce_product_add_to_cart_text', 'woo_custom_cart_button_text');


        function woo_custom_cart_button_text()
        {
            return __(' Mua hàng', 'woocommerce');

        }

        add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_single_cart_button_text'); // 2.1 +

        function woo_custom_single_cart_button_text()
        {

            return __(' Mua hàng ', 'woocommerce');

        }

//thay doi text checkout
// Hook in
        add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');

// Our hooked in function - $fields is passed via the filter!
        function custom_override_checkout_fields($fields)
        {
            // $fields['shipping']['shipping_address_1']['placeholder'] = 'Enter your Shipping Address';
            $fields['billing']['billing_country']['label'] = 'Quốc gia';
            $fields['billing']['billing_address_1']['label'] = 'Địa chỉ';
            $fields['billing']['billing_first_name']['label'] = 'Tên';
            $fields['billing']['billing_last_name']['label'] = 'Họ';
            $fields['billing']['billing_company']['label'] = 'Tên công ty';
            $fields['billing']['billing_city']['label'] = 'Thannh ph?';
            $fields['billing']['billing_state']['label'] = 'Th? tr?n';
            $fields['billing']['billing_phone']['label'] = 'Số điện thoại';
            $fields['billing']['billing_email']['label'] = 'Địa chỉ email';
            $fields['order']['order_comments']['label'] = 'Ghi chú thêm';
            unset($fields['billing']['billing_postcode']);
            return $fields;
        }

        /* Remove Checkout Fields */


        add_action('admin_menu', 'rename_woocoomerce_wpse_100758', 999);

        function rename_woocoomerce_wpse_100758()
        {
            global $menu;

            // Pinpoint menu item
            $woo = recursive_array_search_php_91365('WooCommerce', $menu);

            // Validate
            if (!$woo)
                return;

            $menu[$woo][0] = 'Quản lý';
        }


        function recursive_array_search_php_91365($needle, $haystack)
        {
            foreach ($haystack as $key => $value) {
                $current_key = $key;
                if (
                    $needle === $value
                    OR (
                        is_array($value)
                        && recursive_array_search_php_91365($needle, $value) !== false
                    )
                ) {
                    return $current_key;
                }
            }
            return false;
        }


        add_filter('woocommerce_sale_flash', 'woo_custom_hide_sales_flash');
        function woo_custom_hide_sales_flash()
        {
            return false;
        }


//Change alt of images <img alt=""/>
        add_filter('wp_get_attachment_image_attributes', 'change_attachement_image_attributes', 20, 2);

        function change_attachement_image_attributes($attr, $attachment)
        {
            // Get post parent
            $parent = get_post_field('post_parent', $attachment);

            // Get post type to check if it's product
            $type = get_post_field('post_type', $parent);
            if ($type != 'product') {
                return $attr;
            }
            /// Get title
            $title = get_post_field('post_title', $parent);
            $attr['alt'] = $title;
            return $attr;
        }

        function sigmatheme_register_widget_sidebars()
        {

            register_sidebar(array(
                'name' => 'right-sidebar',
                'id' => 'right-sidebar',
                'description' => 'Widgets in this area will be shown in the footer.',
                'before_widget' => '<div id="%1$s" class="widget one-third column %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3>',
                'after_title' => '</h3>'
            ));

        }

        add_action('widgets_init', 'sigmatheme_register_widget_sidebars');

        function visiter_register_widget_sidebars()
        {

            register_sidebar(array(
                'name' => 'visiters-sidebar',
                'id' => 'visiters-sidebar',
                'description' => 'Widgets in this area will be shown in the footer.',
                'before_widget' => '<div id="%1$s" class="widget one-third column %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3>',
                'after_title' => '</h3>'
            ));

        }

        add_action('widgets_init', 'visiter_register_widget_sidebars');
//Apply do_shortcode() to widgets so that shortcodes will be executed in widgets
        add_filter('widget_text', 'do_shortcode');


        // hide coupon field on cart page
        function hide_coupon_field_on_cart($enabled)
        {
            if (is_cart()) {
                $enabled = false;
            }
            return $enabled;
        }

        add_filter('woocommerce_coupons_enabled', 'hide_coupon_field_on_cart');
        add_theme_support('title-tag');

        /***rewite url .html*/

//        add_action('rewrite_rules_array', 'rewrite_rules');
//        function rewrite_rules($rules) {
//            $new_rules = array();
//            foreach (get_post_types() as $t)
//                $new_rules[$t . '/(.+?)\.html$'] = 'index.php?post_type=' . $t . '&name=$matches[1]';
//            return $new_rules + $rules;
//        }
//
//        add_filter('post_type_link', 'custom_post_permalink'); // for cpt post_type_link (rather than post_link)
//        function custom_post_permalink ($post_link) {
//            global $post;
//            $type = get_post_type($post->ID);
//            return home_url() . '/' . $type . '/' . $post->post_name . '.html';
//        }

    }


endif; //

add_action('after_setup_theme', 'kbone_setup');

