<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 10/28/2015
 * Time: 9:21 PM
 */ ?>
<div class="m-pro-best">
    <div class="module">
        <a href="<?php echo esc_url( get_term_link( 12,'product_cat' ) );; ?>"><h3>Sản phẩm khuyến mãi</h3></a>
        <div class="m-content">
            <?php
            $agrs = array(
                'post_type' => 'product',
                'product_cat' => 'san-pham-khuyen-mai',
                'order' => 'desc',
                'orderby' => 'rand',
                'showposts' => 4,
            );
            $loop = new WP_Query($agrs);
            if ($loop->have_posts()) { ?>
                <ul class="list clearfix slider2">
                    <?php while ($loop->have_posts()): $loop->the_post();
                        global $product;
                        ?>
                        <li>
                            <p class="img-l"><a
                                    href="<?php the_permalink(); ?>"><?php echo woocommerce_get_product_thumbnail(array(50)); ?></a>
                            </p>
                            <h4><a href="<?php the_permalink(); ?>"
                                   title="<?php echo the_title(); ?>"><?php echo wp_trim_words(get_the_title(), 3) ?></a>
                            </h4>
                            <p class="price">
                                <?php if (($product->get_price_html() == null)) {
                                    echo "<span class='txt-b'>Liên hệ </span>";
                                } else {
                                    echo $product->get_price_html();
                                } ?>
                            </p>
                        </li>
                    <?php endwhile; ?>
                </ul>
                <?php
            } else {
                echo __('Sản phẩm đang được cập nhật');
            }
            ?>
        </div>	
    </div>
</div>