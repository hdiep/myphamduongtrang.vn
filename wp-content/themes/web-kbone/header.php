﻿<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

<meta name="robots" content="INDEX,FOLLOW">
<meta name="keywords" content="<?php echo wp_title(''); ?>"/>
<meta property="fb:app_id" content="862476203850462"/>
<meta property="og:type" content="article" />

<meta property="og:title" content="<?php echo wp_title(''); ?>"/>
<meta property="og:image" content="<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>"/>
<?php if (is_single() || is_page()) : if (have_posts()) : while (have_posts()) : the_post(); ?>
<meta name="og:description" content="<?php echo wp_filter_nohtml_kses(get_the_excerpt()) ?>"/>
<?php endwhile; endif;    elseif (is_home()) : ?>
<meta name="og:description" content="<?php bloginfo('description'); ?>"/>
<?php endif; ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="info@myphamduongtrang.com">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css">
<link type='text/css' href='<?php echo get_template_directory_uri() ?>/css/repons.css' rel='stylesheet'/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/custom-awesome.css">
<link type='text/css' rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/jquery.bxslider.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link type='text/css' href='<?php echo get_template_directory_uri() ?>/css/basic_ie.css' rel='stylesheet' media='screen'/>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/menu.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<script> window.fbAsyncInit = function() {FB.init({appId : '862476203850462',  xfbml  : true, version : 'v2.5' }); }; (function(d, s, id){ var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id;   js.src ="//connect.facebook.net/en_US/sdk.js"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
</body>
<header>
  <div class="head">
    <div class="container">
      <div class="col-12-x pb-0">
        <div class="right searchbox"><?php echo woo_custom_product_searchform('') ?>
		<a class="link-shopping col-hidden-x" href="<?php echo WC()->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart fa-2e mb-shopping"></i><span>
          <?php global $woocommerce; echo $woocommerce->cart->cart_contents_count; ?>
          </span></a><img class="mb-hidden col-hidden" alt="en" src="<?php echo get_template_directory_uri(); ?>/images/gb.png"><img class="mb-hidden col-hidden" src="<?php echo get_template_directory_uri(); ?>/images/vn.png" alt="vi"></div>
      </div>
      <div class="col-12-x right">
        <h1 class="left"><a href="<?php echo esc_url(home_url('/')); ?>"><img class="img-l logo" src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="zoleykbone.com" height="55"><img src="<?php echo get_template_directory_uri() ?>/images/logo2.jpg" class="txt-logo wp-post-image" alt="Kbone&ampZoley"></a></h1>
        <?php include 'menu-header.php' ?>
      </div>
    </div>
  </div>
</header>
<div class="slide-main">
  <div class="row">
    <div class="container">
      <?php if (function_exists('meteor_slideshow')) {meteor_slideshow(); } ?>
    </div>
  </div>
</div>
