<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
    return;
}

$related = $product->get_related( $posts_per_page );

if ( sizeof( $related ) == 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
    'post_type'            => 'product',
    'ignore_sticky_posts'  => 1,
    'no_found_rows'        => 1,
    'posts_per_page'       => $posts_per_page,
    'orderby'              => $orderby,
    'post__in'             => $related,
    'post__not_in'         => array( $product->id )
) );

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;

if ( $products->have_posts() ) : ?>

    <div class="related products">

        <h2><?php _e( 'Related Products', 'woocommerce' ); ?></h2>

        <?php woocommerce_product_loop_start(); ?>

        <?php while ( $products->have_posts() ) : $products->the_post();
            global $product;
            ?>

            <div class="col-3-x col-2-m">
                <div class="item">
                    <a href="<?php the_permalink();?>"><?php echo woocommerce_get_product_thumbnail(); ?> </a>
                    <h4><a href="<?php the_permalink();?>" title="<?php echo the_title();?>"><?php echo wp_trim_words(get_the_title(), 8)?></a></h4>

                    <p class="price">
                        <?php if (($product->get_price_html() == null)) {
                            echo "<span class='txt-b'>Liên hệ </span>";
                        } else {
                            echo $product->get_price_html();
                        } ?>
                    </p>

                    <p class="add-cart">
                        <a class="btn-add-cart" href="<?php echo $product->add_to_cart_url() ?>">
                            <i class="fa fa-cart-arrow-down"></i>Mua hàng</a>
                    </p>
                </div>
            </div>

        <?php endwhile; // end of the loop. ?>

        <?php woocommerce_product_loop_end(); ?>

    </div>

<?php endif;

wp_reset_postdata();
