<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 10/28/2015
 * Time: 11:00 PM
 */ ?>
<div class="m-pro-hot">
    <div class="module">
        <a href="<?php echo esc_url( get_term_link( 55,'product_cat' ) ); ?>"> <h3>Sản phẩm hot</h3></a>

        <div class="m-content">
            <?php
            $agrs = array(
                'post_type' => 'product',
                'product_cat' => 'san-pham-hot',
                'order' => 'desc',
                'showposts' => 10,
                'orderby' => 'rand'
            );
            $loop = new WP_Query($agrs);
            if ($loop->have_posts()) { ?>
                <ul class="list clearfix txt-center slider4">
                    <?php while ($loop->have_posts()): $loop->the_post();
                        global $product;
                        ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php echo woocommerce_get_product_thumbnail(array(100,100)); ?> </a>
                            <h4><a href="<?php the_permalink(); ?>"
                                   title="<?php echo the_title(); ?>"><?php echo wp_trim_words(get_the_title(), 5) ?></a>
                            </h4>
                            <p class="price">
                                <?php if (($product->get_price_html() == null)) {
                                    echo "<span class='txt-b'>Liên hệ </span>";
                                } else {
                                    echo $product->get_price_html();
                                } ?>
                            </p>
                            <div class="woocommerce mb-20">
                                <?php  GLOBAL $product; echo '<div class="star-rating-container aggregate">' . $product->get_rating_html() . '</div>';?>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
                <?php
            } else {
                echo __('Sản phẩm đang được cập nhật');
            }
            ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.slider4').bxSlider({
            minSlides: 1,
            slideWidth: 190,
            auto: true,
            ticker: false,
            infiniteLoop: true,
            pager: false,
            controls: false
        });
    });
</script>