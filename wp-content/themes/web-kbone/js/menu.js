/**
 * Created by bach on 10/3/2015.
 */

$(function () {

   $screen = screen.width;

    if($screen<=800){
        $('#common').addClass('menu-phone');
        $('#common').removeClass('common');
    }

    //menu click

    $('a.btn_menu').click(function () {

        $('.menu-header').slideToggle();

    });

    $('.menu-header .plus_phone').click(function(){

        $(this).toggleClass('sub_phone');
        $('.menu-header li ul').slideUp();


        if ($(this).hasClass('sub_phone')) {
            $(this).text('-');
        }
        else {
            $(this).text('+');
        }


        if(!$(this).next().is(":visible"))
        {
            $(this).next().slideDown();
        }

    });


});

