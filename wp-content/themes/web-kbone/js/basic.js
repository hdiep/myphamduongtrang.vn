/*
 * SimpleModal Basic Modal Dialog
 * http://simplemodal.com
 *
 * Copyright (c) 2013 Eric Martin - http://ericmmartin.com
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

jQuery(function ($) {
	// Load dialog on page load
	//$('#basic-modal-content').modal();

	// Load dialog on click
	$('#basic-modal .basic').click(function (e) {
		$('#basic-modal-content').modal();

		return false;
	});
	$('#basic-modall .basicc').click(function (e) {
		$('#basic-modal-contentt').modal();

		return false;
	});
    $('#basic-model-cart .basic-cart').click(function (e) {
        $('#basic-modal-content-cart').modal();

        return false;
    });
});