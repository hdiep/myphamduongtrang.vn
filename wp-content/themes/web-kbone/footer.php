<footer>
    <div class="container common">
    <?php echo wp_nav_menu(array('menu' => 'menu-footer', 'menu_class'=> 'menu')); ?>
        <address>
            <div class="left"><?php echo get_option('copyright_option_left'); ?></div>
            <div class="right"><a href=""><img src="<?php echo get_template_directory_uri() ?>/images/logo-foot.png" alt="footer-img" class="img-l"></a>
                <p class="desc">
                <?php echo get_option('copyright_option_right'); ?>
                </p>
            </div>
        </address>
    </div>
</footer>
<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
</body>
</html>