<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop'); ?>
<div class="content">
    <div class="row">
        <div class="container">
            <div class="col-176 module-left col-hidden">
		<?php include 'm-cart.php' ?>                 
		<?php include 'm-support.php' ?>
		<?php include 'm-kbone.php' ?>
		<?php include 'm-facebook.php' ?>                               
            </div>
            <div class="col-770">
                <?php
                $category = get_queried_object();
                $id_cate = $category->term_id; ?>
                <?php global $post;
                $array = array('category_name' => 'product-vs-posts', 'orderrand' => 'desc');
                $result = get_posts($array);
                foreach ($result as $post): setup_postdata($post);
                    ?>
                    <?php $id_related = get_field('nhap_id') ?>
                    <?php if ($id_related == $id_cate) { ?>
                        <div class="module-main archive-desc" id="portforprod">
                            <div class="col-6-x">
                                <a href="<?php echo the_permalink() ?>"> <?php echo the_post_thumbnail(array(350, 350), array('class' => 'img-l')) ?>
                                </a>
                            </div>
                            <div class="col-6-x">
                                <div class="short">
                                    <h2 class="center"><?php the_title() ?></h2>
                                    <?php the_excerpt() ?>
                                    <div class="share-post mt-20 mb-20">
                                        <div class="share-face socials">
                                            <div class="fb-like " data-href="<?php echo the_permalink() ?>"
                                                 data-layout="button_count" data-action="like" data-show-faces="false"
                                                 data-share="true"></div>

                                            <!--end share facebook-->
                                            <div class="share-google socials">
                                                <!-- Place this tag in your head or just before your close body tag. -->
                                                <script src="https://apis.google.com/js/platform.js" async defer>
                                                    {
                                                        lang: 'vi'
                                                    }

                                                </script>

                                                <!-- Place this tag where you want the share button to render. -->
                                                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="center"><a href="<?php echo the_permalink() ?>" class="btn btn-click">XEM
                                            THÊM</a></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php endforeach ?>
                <div class="module-main">
                    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"
                            type="text/javascript"></script>
                    <script type="text/javascript">
                        var con = jQuery.noConflict();
                        con(document).ready(function ($) {
                            if (document.getElementById('portforprod')) {
                                con(".pagination02").quickPagination({
                                    pageSize: "16",
                                    currentPage: 1,
                                    holder: null,
                                    pagerLocation: "after"
                                });
                            } else {
                                con(".pagination02").quickPagination({
                                    pageSize: "16",
                                    currentPage: 1,
                                    holder: null,
                                    pagerLocation: "after"
                                });
                            }
                        });
                    </script>
                    <?php do_action('woocommerce_before_main_content'); ?>
                    <div class="row pagination02">
                        <?php
                        if (have_posts()):?>
                            <?php while (have_posts()) : the_post();
                                global $product; ?>
                                <div class="col-3-x col-2-m">
                                    <div class="item">
                                        <a href="<?php the_permalink(); ?>"><?php echo woocommerce_get_product_thumbnail(); ?> </a>
                                        <h4><a href="<?php the_permalink(); ?>"
                                               title="<?php echo the_title(); ?>"><?php echo wp_trim_words(get_the_title(), 8) ?></a>

                                        </h4>

                                        <p class="price">
                                            <?php if (($product->get_price_html() == null)) {
                                                echo "<span class='txt-b'>Liên hệ </span>";
                                            } else {
                                                echo $product->get_price_html();
                                            } ?>
                                        </p>

                                        <p class="add-cart">
                                            <a class="btn-add-cart"
                                               href="<?php echo $product->add_to_cart_url() ?>">
                                                <i class="fa fa-cart-arrow-down"></i>Mua hàng</a>
                                        </p>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                            <?php woocommerce_product_loop_end(); ?>
                            <?php do_action('woocommerce_after_shop_loop'); ?>
                        <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
                            <?php wc_get_template('loop/no-products-found.php'); ?>
                        <?php endif; ?>
                    </div>
                    <?php wp_reset_query(); ?>
                    <?php do_action('woocommerce_after_main_content'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php include 'slide-footer.php' ?>
    </div>
</div>
<?php get_footer('shop'); ?>