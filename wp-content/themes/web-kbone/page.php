<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 10/27/2015
 * Time: 11:19 PM
 */ ?>
<?php get_header(); ?>
    <section class="content">
        <div class="row">
            <div class="container">
                <div class="col-176 module-left col-hidden">
                    <?php include 'm-kbone.php' ?>
                    <?php include 'm-takecare.php' ?>
                </div>
                <div class="col-770">
                    <div class="module-main details">
                        <?php
                        if (have_posts()) :
                            // Start the loop.
                            while (have_posts()) : the_post(); ?>
                                <h2>
                                    <a href="<?php bloginfo('home'); ?>"
                                       class="home"><?php _e('Home'); ?></a> &raquo; <?php echo the_title(); ?></h2>
                                <?php the_content();
                            endwhile;
                        else :?><?php
                            echo 'Nội dung chờ cập nhật'; ?><?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>