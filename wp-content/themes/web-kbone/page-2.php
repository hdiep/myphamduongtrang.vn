<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/10/2015
 * Time: 10:52 PM
 */
get_header() ?>
<section class="content">
    <div class="row">
        <div class="container">
            <div class="col-176 module-left col-hidden">
                <?php include 'm-kbone.php' ?>
                <?php include 'm-takecare.php' ?>
            </div>
            <div class="col-770">
                <div class="module-main details contacts">
                    <?php
                    // Start the Loop.
                    while (have_posts()) : the_post();?>
                        <h2>
                            <a href="<?php bloginfo('home'); ?>" class="home fonts-roto"><?php _e('Home'); ?></a> &raquo;
                            <?php echo the_title(); ?>
                        </h2>
                        <?php the_content(); ?>
                    <?php endwhile;
                    ?>
                    <div class="google-map"><?php echo get_option(google_maps_option) ?></div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
