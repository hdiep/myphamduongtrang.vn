<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/19/2015
 * Time: 11:06 PM
Sitemap page
 */
get_header()?>
<section class="content sitemap">
    <div class="row">
        <div class="container">
            <div class="col-176 module-left col-hidden">
                <?php include 'm-kbone.php' ?>
                <?php include 'm-takecare.php' ?>
            </div>
            <div class="col-770">
                <div class="module-main details contacts">
                    <?php
                    // Start the Loop.
                    while (have_posts()) : the_post();?>
                        <h2>
                            <a href="<?php bloginfo('home'); ?>"><?php _e('Home'); ?></a> &raquo; <?php echo the_title(); ?>
                        </h2>
                    <?php endwhile;
                    ?>
                    <div class="col-6-x">
                        <ul class="list-sitemap">
                            <li><a class="text-uppercase" href="<?php echo esc_url(home_url('/')); ?>">Trang chủ</a></li>
                            <li><a class="text-uppercase"  href="<?php echo get_permalink(94) ?>"><?php echo get_the_title(94) ?></a>
                                <?php
                                $taxonomy = 'product_cat';
                                $orderby = 'name';
                                $show_count = 0; // 1 for yes, 0 for no
                                $pad_counts = 0; // 1 for yes, 0 for no
                                $hierarchical = 1; // 1 for yes, 0 for no
                                $title = '';
                                $empty = 0;
                                $args = array(
                                    'taxonomy' => $taxonomy,
                                    'orderby' => $orderby,
                                    'show_count' => $show_count,
                                    'pad_counts' => $pad_counts,
                                    'hierarchical' => $hierarchical,
                                    'title_li' => $title,
                                    'hide_empty' => $empty
                                );
                                ?>
                                <?php $all_categories = get_categories($args);

                                foreach ($all_categories as $cat) {

                                    if ($cat->category_parent == 0) {
                                        ?>
                                        <?php
                                        $category_id = $cat->term_id;
                                        $cat_thumb_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                                        $cat_thumb_url = wp_get_attachment_thumb_url($cat_thumb_id);
                                        $args2 = array(
                                            'taxonomy' => $taxonomy,
                                            'child_of' => 0,
                                            'parent' => $category_id,
                                            'orderby' => $orderby,
                                            'show_count' => $show_count,
                                            'pad_counts' => $pad_counts,
                                            'hierarchical' => $hierarchical,
                                            'title_li' => $title,
                                            'hide_empty' => $empty

                                        );
                                        $sub_cats = get_categories($args2);
                                        if ($sub_cats) {
                                            ?>
                                            <ul>
                                                <?php foreach ($sub_cats as $sub_category_2) {
                                                    if ($sub_cats->$sub_category_2 == 0) {
                                                        $thumbnail_id = get_woocommerce_term_meta($sub_category->term_id, 'thumbnail_id', true);
                                                        $image = wp_get_attachment_url($thumbnail_id); ?>
                                                        <li>
                                                            <a href=" <?php echo get_term_link($sub_category_2->slug, 'product_cat') ?>"> <?php echo $sub_category_2->name; ?></a>
                                                            <?php
                                                            $catesub_id_3 = $sub_category_2->term_id;;
                                                            $args3 = array(
                                                                'taxonomy' => $taxonomy,
                                                                'child_of' => 0,
                                                                'parent' => $catesub_id_3,
                                                                'orderby' => $orderby,
                                                                'show_count' => $show_count,
                                                                'pad_counts' => $pad_counts,
                                                                'hierarchical' => $hierarchical,
                                                                'title_li' => $title,
                                                                'hide_empty' => $empty
                                                            );
                                                            ?>
                                                            <?php $sub_cats_3 = get_categories($args3); ?>
                                                            <?php if ($sub_cats_3) { ?>
                                                                <ul>
                                                                    <?php foreach ($sub_cats_3 as $sub_category_3) { ?>
                                                                        <?php if ($sub_cats_3->$sub_category_3 == 0) { ?>
                                                                            <li>
                                                                                <a href="<?php echo get_term_link($sub_category_3->slug, 'product_cat') ?>">
                                                                                    <?php echo $sub_category_3->name; ?></a>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                    } ?>
                                                                </ul>

                                                            <?php } ?>

                                                        </li>
                                                        <?php
                                                    }
                                                } ?>
                                            </ul>
                                            <?php
                                        }
                                    }
                                } ?>

                            </li>
                            <li>
                                <a href="<?php echo get_category_link(6) ?>"><?php echo get_cat_name(6) ?></a>
                                <ul>
                                    <li><a href="<?php echo get_category_link( 14 ); ?>"> <?php echo get_cat_name( 14 ) ?> </a></li>
                                    <li><a href="<?php echo get_category_link( 15 ); ?>"> <?php echo get_cat_name( 15 ) ?> </a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo get_permalink(2) ?>"><?php echo get_the_title(2) ?></a></li>
                        </ul>
                    </div>
                    <div class="col-6-x">
                        <h4>Mỹ phẩm dưỡng trắng</h4>
                        <?php echo wp_nav_menu(array('menu' => 'kbone', 'menu_class' => 'sitemap-right')); ?>
                        <h4>Chăm sóc khách hàng</h4>
                        <?php echo wp_nav_menu(array('menu' => 'take-care','menu_class'=> 'sitemap-right')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer()?>
