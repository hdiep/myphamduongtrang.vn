<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/6/2015
 * Time: 10:18 PM
 */
?>
<div class="m-relative-post mc-news">
    <div class="module-main">
        <h2> <?php echo get_cat_name(26) ?> </h2>
        <div class="row">
            <?php
            global $post;
            $array = array('category_name' => 'tin-lien-quan', 'orderby' => 'rand', 'numberposts' => '6');
            $result = get_posts($array);
            if (have_posts()) {

                foreach ($result as $post): setup_postdata($post);
                    ?>
                    <div class="col-4-x col-2 col-2-x">
                        <div class="items">
                            <p class="img-l"><a href="<?php echo the_permalink() ?>"><?php echo the_post_thumbnail(array(60,60),array( 'alt'=>get_the_title())) ; ?></a>
                            </p>
                            <h4><a href="<?php echo the_permalink() ?>" title="<?php echo the_title() ?>"><?php echo wp_trim_words(get_the_title(), 9) ?></a>
                            </h4>
                        </div>
                    </div>
                <?php endforeach;
            } else {
                echo __('Bài viết đang cập nhật');
            } ?>
        </div>
    </div>
</div>
