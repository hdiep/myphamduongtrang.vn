<div class="m-hot txt-center">
    <h2> <?php echo get_cat_name(46); ?></h2>
    <ul>
        <?php
        global $post;
        $array = array('category_name' => 'tin-hot', 'orderrand' => 'desc', 'numberposts' => '4');
        $result = get_posts($array);
        if (have_posts() ){
            foreach ($result as $post): setup_postdata($post);
                ?>
                <li>
                    <a href="<?php echo the_permalink()?>"> <?php echo  the_post_thumbnail(array(220,220)); ?></a>
                    <h3 class="italic"><a href="<?php echo the_permalink()?>"><?php echo wp_trim_words(get_the_title(), 6) ?></a></h3>
                </li>
            <?php endforeach;
        }else{
            echo __('Sản phẩm đang được cập nhật');
        }
        ?>
    </ul>
</div>