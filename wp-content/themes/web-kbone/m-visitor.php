<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 10/28/2015
 * Time: 10:53 PM
 */ ?>
<div class="m-visitor">
    <div class="module">
        <h3>Thống kê truy cập</h3>
        <div class="m-content">
            <?php //dynamic_sidebar('visiters-sidebar'); ?>
            <ul>
                <li><img alt="Dang truy cap" src="<?php echo get_template_directory_uri(); ?>/images/tk_1.png"> Đang truy cập: <?php echo do_shortcode('[wpstatistics stat=usersonline]')?></li>
                <li><img src="<?php echo get_template_directory_uri(); ?>/images/tk_4.png" alt="Trong ngay"> Trong ngày: <span id="wtc_lds"></span><?php echo do_shortcode('[wpstatistics stat=visits time=today]')?></li>
                <li><img alt="Trong tuan" src="<?php echo get_template_directory_uri(); ?>/images/tk_2.png"> Hôm qua: <span id="wtc_lws"></span><?php echo do_shortcode('[wpstatistics stat=visits time=yesterday]')?></li>
                <li><img alt="Trong thang" src="<?php echo get_template_directory_uri(); ?>/images/tk_3.png"> Lượt truy cập: <span id="wtc_lms"><?php echo do_shortcode('[wpstatistics stat=visits time=total]')?></span></li>
            </ul>
        </div>
    </div>
</div>
