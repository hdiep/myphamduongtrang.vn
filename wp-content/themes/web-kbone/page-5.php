<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 11/14/2015
 * Time: 7:53 PM
 * trang gio hang
 */
?>
<?php get_header() ?>
<section class="content" id="cart-page">
    <div class="row">
        <div class="container border">
            <div class="col-12-x">
                <div class="module-main details">
                    <?php
                    // Start the Loop.
                    while (have_posts()) : the_post(); ?>
                        <h2>
                            <a href="<?php bloginfo('home'); ?>"
                               class="home fonts-roto"><?php _e('Home'); ?></a> &raquo;</a>
                            <?php echo the_title(); ?>
                        </h2>
                        <?php the_content(); ?>
                    <?php endwhile;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>
