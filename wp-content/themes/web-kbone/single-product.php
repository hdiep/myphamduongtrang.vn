<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
get_header('shop'); ?>
<section class="content">
    <div class="row">
        <div class="container" role="main">
            <div class="col-770">
                <div class="module-main details product-details">
                    <?php
                    /**
                     * woocommerce_before_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                     * @hooked woocommerce_breadcrumb - 20
                     */
                    do_action('woocommerce_before_main_content');
                    ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <?php wc_get_template_part('content', 'single-product'); ?>

                    <?php endwhile; // end of the loop. ?>

                    <?php
                    /**
                     * woocommerce_after_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
                    do_action('woocommerce_after_main_content');
                    ?>

                </div>
            </div>
            <div class="col-176 module-right col-hidden">
                <?php include 'm-cart.php' ?>
                <?php include 'm-support.php' ?>
                <?php include 'm-product-hot.php' ?>
                <?php include 'm-product-sale.php' ?>
		<?php include 'm-cham-soc-khach-hang.php' ?>
                <?php include 'm-facebook.php' ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer('shop'); ?>