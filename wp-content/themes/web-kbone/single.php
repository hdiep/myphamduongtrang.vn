<?php get_header(); ?>
<section class="content">
    <div class="row">
        <div class="container" role="main">
            <div class="col-176 module-left col-hidden">
                <?php include 'm-product-hot-2.php' ?>
		<?php include 'm-product-sale.php' ?>
                <?php include 'm-product-best.php' ?>
                <?php include 'm-facebook.php' ?>                
            </div>
            <div class="col-770">
                <div class="module-main details">
                    <?php
                    // Start the loop.
                    while (have_posts()) : the_post(); ?>
                        <h2><a href="<?php bloginfo('home'); ?>" class="home"><?php _e('Home'); ?></a> &raquo; <a href="<?php echo get_the_permalink(21) ?>" class="cate"> <?php echo the_title(); ?></a></h2>
                        <?php
                        the_content();
                        // End the loop.
                    endwhile;?>
                </div>
                <?php include 'relative-post.php'?>
            </div>
        </div>
    </div>
</section><!-- .content-area -->
<?php get_footer(); ?>