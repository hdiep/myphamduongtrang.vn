<?php
/**
 * Created by PhpStorm.
 * User: thangnm
 * Date: 4/24/14
 * Time: 4:25 PM
 */
?>
<script>
    jQuery(document).ready(function($) {
        $('#upload_logo_button').click(function() {
            tb_show('Upload a logo', 'media-upload.php?referer=mt-setting-kbone&type=image&TB_iframe=true&post_id=0', false);
            return false;
        });
        window.send_to_editor = function(html) {
            var image_url = $('img',html).attr('src');
            $('#logo_url').val(image_url);
            $('#preview_img').src(image_url);
            tb_remove();
        }
    });

</script>
<div class="wrap">
<h2>Cài đặt thông tin website</h2>
    <?php if ( $_GET['settings-updated'] ) echo '<div id="message" class="updated fade"><p>Thông tin đã được lưu.</p></div>';?>
<form method="post" action="options.php" enctype="multipart/form-data">
    <?php settings_fields('mt-settings-group'); ?>
    <?php do_settings_sections('mt-settings-group'); ?>

    <table class="form-table">

        <tr valign="top">
            <th scope="row">Logo:</th>
            <td>
                <input type="text" id="logo_url" name="logo_option" value="<?php echo  get_option('logo_option'); ?>" />
                <a id="upload_logo_button" class="button"><?php _e( 'Media' ); ?></a>
                <span class="description"><?php _e('Upload an image for the banner.' ); ?></span><br>
                <img id="preview_img" src="<?php echo  get_option('logo_option'); ?>" >
            </td>
        </tr>

        <tr valign="top">
            <th scope="row">Địa chỉ liên hệ:</th>
            <td>
                <textarea rows="3" cols="30" name="contact_option" id="contact_option"
                          value=""><?php echo get_option('contact_option'); ?></textarea>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Copyright trái :</th>
            <td>
                <textarea rows="3" cols="30" name="copyright_option_left" id="copyright_option_left"
                          value=""><?php echo get_option('copyright_option_left'); ?></textarea>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Copyright phải:</th>
            <td>
                <textarea rows="3" cols="30" name="copyright_option_right" id="copyright_option_right"
                          value=""><?php echo get_option('copyright_option_right'); ?></textarea>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Alo chat:</th>
            <td>
                <textarea rows="3" cols="30" name="alo_chat_option" id="alo_chat_option"
                          value=""><?php echo get_option('alo_chat_option'); ?></textarea>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Số điện thoại liên hệ:</th>
            <td><input type="text" name="contact_phone_option" value="<?php echo get_option('contact_phone_option'); ?>"/>
            </td>
        </tr>

        <tr valign="top">
            <th scope="row">Hotline</th>
            <td><input type="text" name="hotline_option" value="<?php echo get_option('hotline_option'); ?>"/></td>
        </tr>

        <tr valign="top">
            <th scope="row">Yahoo</th>
            <td><input type="text" name="yahoo_option" value="<?php echo get_option('yahoo_option'); ?>"/></td>
        </tr>

        <tr valign="top">
            <th scope="row">Yahoo 2</th>
            <td><input type="text" name="yahoo_option2" value="<?php echo get_option('yahoo_option2'); ?>"/></td>
        </tr>

        <tr valign="top">
            <th scope="row">Google maps</th>
            <td>
                <textarea maxlength="5000" rows="4" cols="30" name="google_maps_option" id="google_maps_option"
                          value=""><?php echo get_option('google_maps_option'); ?></textarea>
            </td>
        </tr>
    </table>

    <?php submit_button(); ?>
</form>

</div>

