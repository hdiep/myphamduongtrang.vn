<?php
/*
Plugin Name: web-kbone Setting
Plugin URI:
Description: setting for website web-kbone.
Version: 1.0
Author: 
Author URI:
License: Free License
*/


add_action('admin_menu', 'mt_setting_create_menu');

function mt_setting_create_menu()
{
    if (is_admin()) { // admin actions
        add_menu_page('Cài đặt Website', 'Kbone © 2015', 'manage_woocommerce', 'mt-setting-kbone', 'mt_setting_settings_page', 'dashicons-admin-settings', 6);
        add_action('admin_init', 'mt_setting_register_settings');
    } else {
        // non-admin enqueues, actions, and filters
    }
}

function mt_setting_register_settings()
{
    //register our settings
    register_setting('mt-settings-group', 'logo_option');
    register_setting('mt-settings-group', 'contact_option');
    register_setting('mt-settings-group', 'copyright_option_left');
    register_setting('mt-settings-group', 'copyright_option_right');
    register_setting('mt-settings-group', 'alo_chat_option');
    register_setting('mt-settings-group', 'contact_phone_option');
    register_setting('mt-settings-group', 'hotline_option');
    register_setting('mt-settings-group', 'yahoo_option');
    register_setting('mt-settings-group', 'yahoo_option2');
    register_setting('mt-settings-group', 'skype_option');
    register_setting('mt-settings-group', 'google_maps_option');
}

function mt_setting_settings_page()
{
    if (!current_user_can('manage_woocommerce')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    require_once(plugin_dir_path(__FILE__) . 'mt_setting_settings_page.php');
}

function mt_setting_enqueue_scripts()
{
//        wp_enqueue_script('jquery');

    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');

    wp_enqueue_script('media-upload');

}

add_action('admin_enqueue_scripts', 'mt_setting_enqueue_scripts');
