-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2016 at 05:24 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myphamdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_ai_contact`
--

DROP TABLE IF EXISTS `wp_ai_contact`;
CREATE TABLE `wp_ai_contact` (
  `user_id` int(10) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 2, 'verified', '0'),
(2, 3, 'rating', '4'),
(3, 3, 'verified', '0'),
(4, 6, 'rating', '3'),
(5, 6, 'verified', '0'),
(6, 7, 'verified', '0'),
(7, 8, 'rating', '2'),
(8, 8, 'verified', '0'),
(9, 9, 'rating', '1'),
(10, 9, 'verified', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Mr WordPress', '', 'https://wordpress.org/', '', '2016-04-13 06:11:29', '2016-04-13 05:11:29', 'Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.', 0, '1', '', '', 0, 0),
(2, 29, 'diepbnh', 'hongdiepbach@gmail.com', '', '::1', '2016-04-18 09:41:42', '2016-04-18 02:41:42', 'fdf', 0, '1', 'Mozilla/5.0 (Windows NT 6.3; rv:45.0) Gecko/20100101 Firefox/45.0', '', 0, 1),
(3, 29, 'diepbnh', 'hongdiepbach@gmail.com', '', '::1', '2016-04-18 09:42:01', '2016-04-18 02:42:01', 'ok', 0, '1', 'Mozilla/5.0 (Windows NT 6.3; rv:45.0) Gecko/20100101 Firefox/45.0', '', 0, 1),
(4, 175, 'WooCommerce', '', '', '', '2016-04-29 12:58:15', '2016-04-29 05:58:15', 'Thực hiện thanh toán khi giao hàng Trạng thái đơn hàng đã được chuyển từ Thanh toán chờ xử lý sang Đang xử lý.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(5, 175, 'WooCommerce', '', '', '', '2016-04-29 12:58:19', '2016-04-29 05:58:19', 'Item 31 stock reduced from 2 to 0.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(6, 168, 'diepbnh', 'hongdiepbach@gmail.com', '', '::1', '2016-05-09 03:19:18', '2016-05-08 20:19:18', 'good', 0, '1', 'Mozilla/5.0 (Windows NT 6.3; rv:46.0) Gecko/20100101 Firefox/46.0', '', 0, 1),
(7, 31, 'hong', 'hoadiepdo@gmail.com', '', '::1', '2016-05-09 05:24:05', '2016-05-08 22:24:05', 'binh luan', 0, '0', 'Mozilla/5.0 (Windows NT 6.3; rv:46.0) Gecko/20100101 Firefox/46.0', '', 0, 0),
(8, 27, 'diepbnh', 'hongdiepbach@gmail.com', '', '::1', '2016-05-10 12:48:48', '2016-05-10 05:48:48', 'ok', 0, '1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36', '', 0, 1),
(9, 158, 'diepbnh', 'hongdiepbach@gmail.com', '', '::1', '2016-05-10 13:28:28', '2016-05-10 06:28:28', 'dd', 0, '1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36', '', 0, 1),
(10, 210, 'WooCommerce', '', '', '', '2016-06-02 12:31:56', '2016-06-02 05:31:56', 'Thực hiện thanh toán khi giao hàng Trạng thái đơn hàng đã được chuyển từ Thanh toán chờ xử lý sang Đang xử lý.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(11, 210, 'WooCommerce', '', '', '', '2016-06-02 12:32:03', '2016-06-02 05:32:03', 'Số lượng sản phẩm 29 giảm từ 20 xuống 19', 0, '1', 'WooCommerce', 'order_note', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/mypham', 'yes'),
(2, 'home', 'http://localhost/mypham', 'yes'),
(3, 'blogname', 'My pham', 'yes'),
(4, 'blogdescription', '- My pham', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hongdiepbach@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'closed', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '100', 'yes'),
(23, 'date_format', 'd/m/Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'jS F Y g:i a', 'yes'),
(26, 'comment_moderation', '1', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '', 'yes'),
(29, 'hack_file', '0', 'yes'),
(30, 'blog_charset', 'UTF-8', 'yes'),
(31, 'moderation_keys', '', 'no'),
(32, 'active_plugins', 'a:9:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:36:"contact-form-7/wp-contact-form-7.php";i:2;s:38:"meteor-slides/meteor-slides-plugin.php";i:3;s:37:"mt_setting_kbone/mt_setting_kbone.php";i:4;s:54:"responsive-contact-form/ai-responsive-contact-form.php";i:5;s:37:"tinymce-advanced/tinymce-advanced.php";i:6;s:62:"woocommerce-dynamic-gallery/wc_dynamic_gallery_woocommerce.php";i:7;s:27:"woocommerce/woocommerce.php";i:8;s:29:"wp-mail-smtp/wp_mail_smtp.php";}', 'yes'),
(33, 'category_base', '', 'yes'),
(34, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(35, 'comment_max_links', '2', 'yes'),
(36, 'gmt_offset', '', 'yes'),
(37, 'default_email_category', '1', 'yes'),
(38, 'recently_edited', 'a:2:{i:0;s:78:"C:\\xampp\\htdocs\\mypham/wp-content/plugins/cloud-zoom-for-woocommerce/index.php";i:1;s:0:"";}', 'no'),
(39, 'template', 'myphamduongtrang', 'yes'),
(40, 'stylesheet', 'myphamduongtrang', 'yes'),
(41, 'comment_whitelist', '1', 'yes'),
(42, 'blacklist_keys', '', 'no'),
(43, 'comment_registration', '', 'yes'),
(44, 'html_type', 'text/html', 'yes'),
(45, 'use_trackback', '0', 'yes'),
(46, 'default_role', 'subscriber', 'yes'),
(47, 'db_version', '36686', 'yes'),
(48, 'uploads_use_yearmonth_folders', '1', 'yes'),
(49, 'upload_path', '', 'yes'),
(50, 'blog_public', '1', 'yes'),
(51, 'default_link_category', '2', 'yes'),
(52, 'show_on_front', 'posts', 'yes'),
(53, 'tag_base', '', 'yes'),
(54, 'show_avatars', '1', 'yes'),
(55, 'avatar_rating', 'G', 'yes'),
(56, 'upload_url_path', '', 'yes'),
(57, 'thumbnail_size_w', '150', 'yes'),
(58, 'thumbnail_size_h', '150', 'yes'),
(59, 'thumbnail_crop', '1', 'yes'),
(60, 'medium_size_w', '300', 'yes'),
(61, 'medium_size_h', '300', 'yes'),
(62, 'avatar_default', 'mystery', 'yes'),
(63, 'large_size_w', '1024', 'yes'),
(64, 'large_size_h', '1024', 'yes'),
(65, 'image_default_link_type', 'none', 'yes'),
(66, 'image_default_size', '', 'yes'),
(67, 'image_default_align', '', 'yes'),
(68, 'close_comments_for_old_posts', '', 'yes'),
(69, 'close_comments_days_old', '14', 'yes'),
(70, 'thread_comments', '', 'yes'),
(71, 'thread_comments_depth', '5', 'yes'),
(72, 'page_comments', '', 'yes'),
(73, 'comments_per_page', '50', 'yes'),
(74, 'default_comments_page', 'newest', 'yes'),
(75, 'comment_order', 'asc', 'yes'),
(76, 'sticky_posts', 'a:0:{}', 'yes'),
(77, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(78, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'uninstall_plugins', 'a:2:{s:54:"responsive-contact-form/ai-responsive-contact-form.php";s:25:"ai_contact_form_uninstall";s:36:"cloud-zoom-for-woocommerce/index.php";s:25:"wcz_delete_plugin_options";}', 'no'),
(81, 'timezone_string', 'Asia/Bangkok', 'yes'),
(82, 'page_for_posts', '0', 'yes'),
(83, 'page_on_front', '0', 'yes'),
(84, 'default_post_format', '0', 'yes'),
(85, 'link_manager_enabled', '0', 'yes'),
(86, 'finished_splitting_shared_terms', '1', 'yes'),
(87, 'site_icon', '0', 'yes'),
(88, 'medium_large_size_w', '768', 'yes'),
(89, 'medium_large_size_h', '0', 'yes'),
(90, 'initial_db_version', '35700', 'yes'),
(91, 'wp_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:131:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}}', 'yes'),
(92, 'WPLANG', 'vi', 'yes'),
(93, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:0:{}s:13:"right-sidebar";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:16:"visiters-sidebar";N;s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'cron', 'a:8:{i:1465535491;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1465535598;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1465536535;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1465537680;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1465540284;a:2:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1465542000;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1467504000;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(141, '_transient_twentysixteen_categories', '1', 'yes'),
(163, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1460612876;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(164, 'current_theme', 'myphamduongtrang', 'yes'),
(165, 'theme_mods_mypham', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1463954234;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(166, 'theme_switched', '', 'yes'),
(187, 'recently_activated', 'a:0:{}', 'yes'),
(217, 'woocommerce_default_country', 'VN', 'yes'),
(218, 'woocommerce_allowed_countries', 'specific', 'yes'),
(219, 'woocommerce_specific_allowed_countries', 'a:1:{i:0;s:2:"VN";}', 'yes'),
(220, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(221, 'woocommerce_demo_store', 'no', 'yes'),
(222, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes &mdash; no orders shall be fulfilled.', 'no'),
(223, 'woocommerce_currency', 'VND', 'yes'),
(224, 'woocommerce_currency_pos', 'right', 'yes'),
(225, 'woocommerce_price_thousand_sep', ',', 'yes'),
(226, 'woocommerce_price_decimal_sep', '.', 'yes'),
(227, 'woocommerce_price_num_decimals', '3', 'yes'),
(228, 'woocommerce_weight_unit', 'kg', 'yes'),
(229, 'woocommerce_dimension_unit', 'm', 'yes'),
(230, 'woocommerce_enable_review_rating', 'yes', 'no'),
(231, 'woocommerce_review_rating_required', 'no', 'no'),
(232, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(233, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(234, 'woocommerce_shop_page_id', '18', 'yes'),
(235, 'woocommerce_shop_page_display', '', 'yes'),
(236, 'woocommerce_category_archive_display', '', 'yes'),
(237, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(238, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(239, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(240, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"400";s:6:"height";s:3:"400";s:4:"crop";i:0;}', 'yes'),
(241, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"600";s:6:"height";s:3:"600";s:4:"crop";i:0;}', 'yes'),
(242, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:3:"180";s:6:"height";s:3:"180";s:4:"crop";i:0;}', 'yes'),
(243, 'woocommerce_enable_lightbox', 'yes', 'yes'),
(244, 'woocommerce_manage_stock', 'yes', 'yes'),
(245, 'woocommerce_hold_stock_minutes', '60', 'no'),
(246, 'woocommerce_notify_low_stock', 'no', 'no'),
(247, 'woocommerce_notify_no_stock', 'no', 'no'),
(248, 'woocommerce_stock_email_recipient', 'hongdiepbach@gmail.com', 'no'),
(249, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(250, 'woocommerce_notify_no_stock_amount', '0', 'no'),
(251, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(252, 'woocommerce_stock_format', '', 'yes'),
(253, 'woocommerce_file_download_method', 'force', 'no'),
(254, 'woocommerce_downloads_require_login', 'no', 'no'),
(255, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(256, 'woocommerce_calc_taxes', 'no', 'yes'),
(257, 'woocommerce_prices_include_tax', 'no', 'yes'),
(258, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(259, 'woocommerce_shipping_tax_class', 'title', 'yes'),
(260, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(261, 'woocommerce_tax_classes', 'Reduced Rate\r\nZero Rate', 'yes'),
(262, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(263, 'woocommerce_tax_display_cart', 'excl', 'no'),
(264, 'woocommerce_price_display_suffix', '', 'yes'),
(265, 'woocommerce_tax_total_display', 'itemized', 'no'),
(266, 'woocommerce_enable_coupons', 'no', 'no'),
(267, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(268, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(269, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(270, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(271, 'woocommerce_cart_page_id', '19', 'yes'),
(272, 'woocommerce_checkout_page_id', '20', 'yes'),
(273, 'woocommerce_terms_page_id', '', 'no'),
(274, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(275, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(276, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(277, 'woocommerce_calc_shipping', 'no', 'yes'),
(278, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(279, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(280, 'woocommerce_ship_to_destination', 'billing', 'no'),
(281, 'woocommerce_ship_to_countries', 'specific', 'yes'),
(282, 'woocommerce_specific_ship_to_countries', 'a:1:{i:0;s:2:"VN";}', 'yes'),
(283, 'woocommerce_myaccount_page_id', '21', 'yes'),
(284, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(285, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(286, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(287, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(288, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(289, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(290, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(291, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(292, 'woocommerce_registration_generate_username', 'yes', 'no'),
(293, 'woocommerce_registration_generate_password', 'no', 'no'),
(294, 'woocommerce_email_from_name', 'My pham', 'no'),
(295, 'woocommerce_email_from_address', 'hongdiepbach@gmail.com', 'no'),
(296, 'woocommerce_email_header_image', '', 'no'),
(297, 'woocommerce_email_footer_text', 'My pham - Powered by WooCommerce', 'no'),
(298, 'woocommerce_email_base_color', '#557da1', 'no'),
(299, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(300, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(301, 'woocommerce_email_text_color', '#505050', 'no'),
(302, 'woocommerce_api_enabled', 'yes', 'yes'),
(304, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(307, 'woocommerce_db_version', '2.5.5', 'yes'),
(308, 'woocommerce_version', '2.5.5', 'yes'),
(311, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(312, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(313, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(314, 'widget_woocommerce_layered_nav', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(315, 'widget_woocommerce_price_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(316, 'widget_woocommerce_product_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(317, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(318, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(319, 'widget_woocommerce_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(320, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(321, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(322, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(323, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(325, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(333, 'woocommerce_paypal_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(334, 'woocommerce_cheque_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(335, 'woocommerce_cod_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(336, 'woocommerce_bacs_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(337, 'woocommerce_allow_tracking', 'no', 'yes'),
(365, '_transient_timeout_plugin_slugs', '1464934261', 'no'),
(366, '_transient_plugin_slugs', 'a:10:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:36:"contact-form-7/wp-contact-form-7.php";i:2;s:45:"meteor-slides - Copy/meteor-slides-plugin.php";i:3;s:38:"meteor-slides/meteor-slides-plugin.php";i:4;s:54:"responsive-contact-form/ai-responsive-contact-form.php";i:5;s:37:"tinymce-advanced/tinymce-advanced.php";i:6;s:37:"mt_setting_kbone/mt_setting_kbone.php";i:7;s:27:"woocommerce/woocommerce.php";i:8;s:62:"woocommerce-dynamic-gallery/wc_dynamic_gallery_woocommerce.php";i:9;s:29:"wp-mail-smtp/wp_mail_smtp.php";}', 'no'),
(369, '_transient_product_query-transient-version', '1462864491', 'yes'),
(370, '_transient_product-transient-version', '1462864491', 'yes'),
(377, 'woocommerce_permalinks', 'a:4:{s:13:"category_base";s:0:"";s:8:"tag_base";s:0:"";s:14:"attribute_base";s:0:"";s:12:"product_base";s:0:"";}', 'yes'),
(388, 'product_measurement_options', '', 'yes'),
(389, 'product_rating_options', '', 'yes'),
(397, '_transient_orders-transient-version', '1464845522', 'yes'),
(398, '_transient_timeout_wc_cbp_e0bdfa3f2c39f7199c1e34daf3120455', '1463539302', 'no'),
(399, '_transient_wc_cbp_e0bdfa3f2c39f7199c1e34daf3120455', 'a:0:{}', 'no'),
(450, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(456, 'wpcf7', 'a:2:{s:7:"version";s:5:"4.4.2";s:13:"bulk_validate";a:4:{s:9:"timestamp";d:1461045428;s:7:"version";s:5:"4.4.1";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(459, 'ai_email_address_setting', 'hongdiepbach@gmail.com', 'yes'),
(460, 'ai_subject_text', 'Dang tin', 'yes'),
(461, 'ai_reply_user_message', 'Email của bạn đã được ghi nhận đăng tin', 'yes'),
(462, 'ai_enable_captcha', '', 'yes'),
(463, 'ai_error_setting', '', 'yes'),
(464, 'ai_visible_name', '', 'yes'),
(465, 'ai_enable_require_name', '', 'yes'),
(466, 'ai_visible_phone', '', 'yes'),
(467, 'ai_enable_require_phone', '', 'yes'),
(468, 'ai_visible_email', '', 'yes'),
(469, 'ai_visible_subject', '', 'yes'),
(470, 'ai_enable_require_subject', '', 'yes'),
(471, 'ai_visible_website', '', 'yes'),
(472, 'ai_enable_require_website', '', 'yes'),
(473, 'ai_visible_comment', '', 'yes'),
(474, 'ai_enable_require_comment', '', 'yes'),
(475, 'ai_visible_sendcopy', '', 'yes'),
(476, 'ai_custom_css', '', 'yes'),
(477, 'ai_rm_user_list', '', 'yes'),
(478, 'ai_success_message', 'on', 'yes'),
(487, 'meteorslides_options', 'a:7:{s:18:"slideshow_quantity";s:1:"5";s:12:"slide_height";s:3:"440";s:11:"slide_width";s:0:"";s:16:"transition_style";s:10:"scrollLeft";s:16:"transition_speed";s:1:"2";s:14:"slide_duration";s:1:"5";s:20:"slideshow_navigation";s:11:"navprevnext";}', 'yes'),
(488, 'widget_meteor-slides-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(490, 'slideshow_children', 'a:0:{}', 'yes'),
(499, 'product_shipping_class_children', 'a:0:{}', 'yes'),
(506, 'product_cat_children', 'a:3:{i:6;a:4:{i:0;i:7;i:1;i:8;i:2;i:9;i:3;i:10;}i:9;a:2:{i:0;i:17;i:1;i:19;}i:7;a:1:{i:0;i:18;}}', 'yes'),
(509, 'logo_option', '', 'yes'),
(510, 'contact_option', '<b>Mỹ Phẩm Hoàng Dung</b><br>\r\n<b>Địa chỉ: </b>1063 Huỳnh Tấn Phát, Phường Phú Thuận, Quận 7, HCM', 'yes'),
(511, 'copyright_option_left', 'infor@myphamhoangdung.com', 'yes'),
(512, 'copyright_option_right', '', 'yes'),
(513, 'alo_chat_option', '', 'yes'),
(514, 'contact_phone_option', '0903 665 795 – 0912 883137', 'yes'),
(515, 'hotline_option', '0967 510 510', 'yes'),
(516, 'yahoo_option', 'minhsang', 'yes'),
(517, 'yahoo_option2', '', 'yes'),
(518, 'skype_option', 'minhsang', 'yes'),
(519, 'google_maps_option', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3920.2584148655474!2d106.73473651420395!3d10.714540692363533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752544ebd1e61d%3A0x1d2c08dfce016661!2zMTA2MyBIdeG7s25oIFThuqVuIFBow6F0LCBQaMO6IFRodeG6rW4sIFF14bqtbiA3LCBI4buTIENow60gTWluaCwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1461044769581" width="800" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>', 'yes'),
(528, 'email_option', 'infor@myphamhoangdung.com', 'yes'),
(529, 'hours_option', '<b>Giờ làm việc: </b>9:00h - 17:00h', 'yes'),
(540, 'tadv_settings', 'a:6:{s:9:"toolbar_1";s:143:"bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,unlink,table,fullscreen,undo,redo,wp_adv,fontsizeselect,fontselect";s:9:"toolbar_2";s:143:"formatselect,alignjustify,strikethrough,outdent,indent,pastetext,removeformat,charmap,wp_more,emoticons,forecolor,wp_help,media,underline,image";s:9:"toolbar_3";s:0:"";s:9:"toolbar_4";s:0:"";s:7:"options";s:21:"advlist,menubar,image";s:7:"plugins";s:107:"anchor,code,insertdatetime,nonbreaking,print,searchreplace,table,visualblocks,visualchars,emoticons,advlist";}', 'yes'),
(541, 'tadv_admin_settings', 'a:2:{s:7:"options";s:8:"no_autop";s:16:"disabled_plugins";s:0:"";}', 'yes'),
(542, 'tadv_version', '4000', 'yes'),
(566, 'catalog_options', '', 'yes'),
(567, 'image_options', '', 'yes'),
(646, 'acf_version', '4.4.7', 'yes'),
(698, 'checkout_process_options', '', 'yes'),
(699, 'checkout_page_options', '', 'yes'),
(700, 'account_endpoint_options', '', 'yes'),
(701, 'checkout_endpoint_options', '', 'yes'),
(702, 'payment_gateways_options', '', 'yes'),
(703, 'woocommerce_gateway_order', 'a:4:{s:4:"bacs";i:0;s:6:"cheque";i:1;s:3:"cod";i:2;s:6:"paypal";i:3;}', 'yes'),
(705, 'shipping_options', '', 'yes'),
(706, 'woocommerce_shipping_method_selection_priority', 'a:5:{s:9:"flat_rate";i:1;s:13:"free_shipping";i:1;s:22:"international_delivery";i:1;s:14:"local_delivery";i:1;s:12:"local_pickup";i:1;}', 'yes'),
(707, 'woocommerce_shipping_method_order', 'a:5:{s:9:"flat_rate";i:0;s:13:"free_shipping";i:1;s:22:"international_delivery";i:2;s:14:"local_delivery";i:3;s:12:"local_pickup";i:4;}', 'yes'),
(708, '_transient_shipping-transient-version', '1461648956', 'yes'),
(822, 'general_options', '', 'yes'),
(823, 'pricing_options', '', 'yes'),
(825, 'product_inventory_options', '', 'yes'),
(827, 'account_page_options', '', 'yes'),
(828, 'account_registration_options', '', 'yes'),
(857, '_transient_timeout_geoip_::1', '1463185249', 'no'),
(858, '_transient_geoip_::1', '', 'no'),
(861, '_transient_timeout_geoip_183.81.9.125', '1463185253', 'no'),
(862, '_transient_geoip_183.81.9.125', '', 'no'),
(873, '_site_transient_timeout_browser_334dd499e4416be113cbb08faa3117bd', '1463198279', 'yes'),
(874, '_site_transient_browser_334dd499e4416be113cbb08faa3117bd', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"50.0.2661.94";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(890, 'rewrite_rules', '', 'yes'),
(891, 'db_upgraded', '', 'yes'),
(894, 'can_compress_scripts', '1', 'yes'),
(896, '_site_transient_timeout_wordpress_credits_vi', '1462683554', 'yes'),
(897, '_site_transient_wordpress_credits_vi', 'a:2:{s:6:"groups";a:8:{s:15:"project-leaders";a:4:{s:4:"name";s:15:"Project Leaders";s:4:"type";s:6:"titles";s:7:"shuffle";b:1;s:4:"data";a:6:{s:4:"matt";a:4:{i:0;s:14:"Matt Mullenweg";i:1;s:32:"767fc9c115a1b989744c755db47feb60";i:2;s:4:"matt";i:3;s:23:"Cofounder, Project Lead";}s:5:"nacin";a:4:{i:0;s:12:"Andrew Nacin";i:1;s:32:"01cfe9feaafb068590891bbd1f6a7f5a";i:2;s:5:"nacin";i:3;s:14:"Lead Developer";}s:11:"markjaquith";a:4:{i:0;s:12:"Mark Jaquith";i:1;s:32:"097a87a525e317519b5ee124820012fb";i:2;s:11:"markjaquith";i:3;s:14:"Lead Developer";}s:6:"azaozz";a:4:{i:0;s:10:"Andrew Ozz";i:1;s:32:"4e84843ebff0918d72ade21c6ee7b1e4";i:2;s:6:"azaozz";i:3;s:14:"Lead Developer";}s:5:"helen";a:4:{i:0;s:16:"Helen Hou-Sandí";i:1;s:32:"6506162ada6b700b151ad8a187f65842";i:2;s:5:"helen";i:3;s:14:"Lead Developer";}s:4:"dd32";a:4:{i:0;s:10:"Dion Hulse";i:1;s:32:"4af2825655b46fb1206b08d9041d8e3e";i:2;s:4:"dd32";i:3;s:14:"Lead Developer";}}}s:15:"core-developers";a:4:{s:4:"name";s:23:"Contributing Developers";s:4:"type";s:6:"titles";s:7:"shuffle";b:0;s:4:"data";a:12:{s:12:"mikeschroder";a:4:{i:0;s:13:"Mike Schroder";i:1;s:32:"76424a001dc6b3ebb4faca0c567800c4";i:2;s:12:"mikeschroder";i:3;s:12:"Release Lead";}s:15:"adamsilverstein";a:4:{i:0;s:16:"Adam Silverstein";i:1;s:32:"fddbd6c3e1c3d971aa732b9346aeb433";i:2;s:15:"adamsilverstein";i:3;s:14:"Release Deputy";}s:9:"melchoyce";a:4:{i:0;s:11:"Mel Choyce ";i:1;s:32:"ed4f856405e64c1492839d2bf4913baa";i:2;s:9:"melchoyce";i:3;s:19:"Release Design Lead";}s:6:"jorbin";a:4:{i:0;s:12:"Aaron Jorbin";i:1;s:32:"b3e2b94eb305bf95a1bee11bc7705fb7";i:2;s:6:"jorbin";i:3;s:14:"Core Developer";}s:12:"boonebgorges";a:4:{i:0;s:15:"Boone B. Gorges";i:1;s:32:"9cf7c4541a582729a5fc7ae484786c0c";i:2;s:12:"boonebgorges";i:3;s:14:"Core Developer";}s:7:"ocean90";a:4:{i:0;s:17:"Dominik Schilling";i:1;s:32:"3e8e161d97d793bd8fc2dcd62583bb76";i:2;s:7:"ocean90";i:3;s:14:"Core Developer";}s:12:"DrewAPicture";a:4:{i:0;s:11:"Drew Jaynes";i:1;s:32:"95c934fa2c3362794bf62ff8c59ada08";i:2;s:12:"DrewAPicture";i:3;s:14:"Core Developer";}s:5:"pento";a:4:{i:0;s:15:"Gary Pendergast";i:1;s:32:"1ad9e5c98d81c6815a65dab5b6e1f669";i:2;s:5:"pento";i:3;s:14:"Core Developer";}s:10:"jeremyfelt";a:4:{i:0;s:11:"Jeremy Felt";i:1;s:32:"d1759b1c669981b7c52ec9a97d19e6bd";i:2;s:10:"jeremyfelt";i:3;s:14:"Core Developer";}s:11:"johnbillion";a:4:{i:0;s:15:"John Blackbourn";i:1;s:32:"0000ba6dd1b089e1746abbfe6281ee3b";i:2;s:11:"johnbillion";i:3;s:14:"Core Developer";}s:14:"wonderboymusic";a:4:{i:0;s:12:"Scott Taylor";i:1;s:32:"112ca15732a80bb928c52caec9d2c8dd";i:2;s:14:"wonderboymusic";i:3;s:14:"Core Developer";}s:14:"SergeyBiryukov";a:4:{i:0;s:15:"Sergey Biryukov";i:1;s:32:"750b7b0fcd855389264c2b1294d61bd6";i:2;s:14:"SergeyBiryukov";i:3;s:14:"Core Developer";}}}s:23:"contributing-developers";a:4:{s:4:"name";b:0;s:4:"type";s:6:"titles";s:7:"shuffle";b:1;s:4:"data";a:12:{s:8:"obenland";a:4:{i:0;s:19:"Konstantin Obenland";i:1;s:32:"2370ea5912750f4cb0f3c51ae1cbca55";i:2;s:8:"obenland";i:3;s:0:"";}s:7:"iseulde";a:4:{i:0;s:22:"Ella Iseulde Van Dorpe";i:1;s:32:"1a847a68778b8344b6aa106a8861984f";i:2;s:7:"iseulde";i:3;s:0:"";}s:11:"westonruter";a:4:{i:0;s:12:"Weston Ruter";i:1;s:32:"22ed378fbf1d918ef43a45b2a1f34634";i:2;s:11:"westonruter";i:3;s:0:"";}s:7:"afercia";a:4:{i:0;s:13:"Andrea Fercia";i:1;s:32:"074af62ea5ff218b6a6eeab89104f616";i:2;s:7:"afercia";i:3;s:0:"";}s:6:"rmccue";a:4:{i:0;s:10:"Ryan McCue";i:1;s:32:"08818120f223035a0857c2a0ec417f93";i:2;s:6:"rmccue";i:3;s:0:"";}s:10:"karmatosed";a:4:{i:0;s:13:"Tammie Lister";i:1;s:32:"ca7d4273a689cdbf524d8332771bb1ca";i:2;s:10:"karmatosed";i:3;s:0:"";}s:10:"swissspidy";a:4:{i:0;s:15:"Pascal Birchler";i:1;s:32:"609d6ee54dc13a51270cb99769535b82";i:2;s:10:"swissspidy";i:3;s:0:"";}s:11:"rachelbaker";a:4:{i:0;s:12:"Rachel Baker";i:1;s:32:"634b37a53babc18a5bda19722d5b41a3";i:2;s:11:"rachelbaker";i:3;s:0:"";}s:8:"joehoyle";a:4:{i:0;s:9:"Joe Hoyle";i:1;s:32:"0ceb885cc3d306af93c9764b2936d618";i:2;s:8:"joehoyle";i:3;s:0:"";}s:9:"ericlewis";a:4:{i:0;s:17:"Eric Andrew Lewis";i:1;s:32:"7e524cf1c5e8d108658899a497dc4bd4";i:2;s:9:"ericlewis";i:3;s:0:"";}s:9:"joemcgill";a:4:{i:0;s:10:"Joe McGill";i:1;s:32:"7cef1c9108207ec24db7a40f142db676";i:2;s:9:"joemcgill";i:3;s:0:"";}s:7:"Ipstenu";a:4:{i:0;s:12:"Mika Epstein";i:1;s:32:"f4c134eb021e026414a1bd23d3c5c927";i:2;s:7:"Ipstenu";i:3;s:0:"";}}}s:16:"recent-rockstars";a:4:{s:4:"name";b:0;s:4:"type";s:6:"titles";s:7:"shuffle";b:1;s:4:"data";a:9:{s:9:"chriscct7";a:4:{i:0;s:15:"Chris Christoff";i:1;s:32:"d0931e57862048dabb0bea3a71ce6229";i:2;s:9:"chriscct7";i:3;s:0:"";}s:15:"danielbachhuber";a:4:{i:0;s:16:"Daniel Bachhuber";i:1;s:32:"a304ad0084a78fe52f6b93e00871754e";i:2;s:15:"danielbachhuber";i:3;s:0:"";}s:16:"celloexpressions";a:4:{i:0;s:12:"Nick Halsey ";i:1;s:32:"c5fe6b97c0f4d32a41346582d4e894db";i:2;s:16:"celloexpressions";i:3;s:0:"";}s:7:"dnewton";a:4:{i:0;s:12:"David Newton";i:1;s:32:"2b5c483d8d2a5fb69cb5b27c784edea3";i:2;s:7:"dnewton";i:3;s:0:"";}s:8:"ebinnion";a:4:{i:0;s:12:"Eric Binnion";i:1;s:32:"e8d172977181f0f3e105a2f4553cccc2";i:2;s:8:"ebinnion";i:3;s:0:"";}s:10:"grantpalin";a:4:{i:0;s:11:"Grant Palin";i:1;s:32:"21ec4a8be46b7367360f2808fb5524c9";i:2;s:10:"grantpalin";i:3;s:0:"";}s:10:"rockwell15";a:4:{i:0;s:15:"Andrew Rockwell";i:1;s:32:"a34eb01ca391ad54fe02fe370501a5fa";i:2;s:10:"rockwell15";i:3;s:0:"";}s:7:"gitlost";a:4:{i:0;s:12:"Martin Burke";i:1;s:32:"d2ebc5603eed26b523436a486c6d6c5d";i:2;s:7:"gitlost";i:3;s:0:"";}s:6:"kwight";a:4:{i:0;s:10:"Kirk Wight";i:1;s:32:"e589ec6c9b981d9da221f206f49dec3d";i:2;s:6:"kwight";i:3;s:0:"";}}}s:5:"props";a:4:{s:4:"name";s:33:"Core Contributors to WordPress %s";s:12:"placeholders";a:1:{i:0;s:3:"4.5";}s:4:"type";s:4:"list";s:4:"data";a:264:{s:7:"mercime";s:8:"@mercime";s:13:"aaroncampbell";s:17:"Aaron D. Campbell";s:9:"uglyrobot";s:13:"Aaron Edwards";s:8:"ahockley";s:13:"Aaron Hockley";s:13:"abiralneupane";s:14:"Abiral Neupane";s:12:"mrahmadawais";s:11:"Ahmad Awais";s:9:"aidanlane";s:9:"aidanlane";s:8:"ambrosey";s:12:"Alice Brosey";s:5:"arush";s:11:"Amanda Rush";s:4:"andg";s:14:"Andrea Gandino";s:7:"andizer";s:4:"Andy";s:13:"ankit-k-gupta";s:13:"Ankit K Gupta";s:7:"atimmer";s:16:"Anton Timmermans";s:7:"apaliku";s:7:"apaliku";s:6:"aramzs";s:19:"Aram Zucker-Scharff";s:11:"ashmatadeen";s:12:"ash.matadeen";s:11:"bappidgreat";s:16:"Ashok Kumar Nath";s:12:"bandonrandon";s:12:"BandonRandon";s:11:"barryceelen";s:12:"Barry Ceelen";s:13:"empireoflight";s:10:"Ben Dunkle";s:12:"berengerzyla";s:12:"berengerzyla";s:5:"neoxx";s:14:"Bernhard Riedl";s:8:"thisisit";s:16:"Bhushan S. Jawle";s:7:"birgire";s:27:"Birgir Erlendsson (birgire)";s:11:"williamsba1";s:13:"Brad Williams";s:12:"bradyvercher";s:13:"Brady Vercher";s:15:"thebrandonallen";s:13:"Brandon Allen";s:8:"bhubbard";s:15:"Brandon Hubbard";s:7:"kraftbj";s:13:"Brandon Kraft";s:9:"krogsgard";s:15:"Brian Krogsgard";s:11:"borgesbruno";s:12:"Bruno Borges";s:5:"chmac";s:16:"Callum Macdonald";s:8:"camikaos";s:9:"Cami Kaos";s:12:"chandrapatel";s:13:"Chandra Patel";s:9:"mackensen";s:14:"Charles Fulton";s:13:"chetanchauhan";s:14:"Chetan Chauhan";s:6:"chouby";s:6:"Chouby";s:6:"chrico";s:6:"ChriCo";s:9:"chris_dev";s:9:"Chris Mok";s:13:"christophherr";s:13:"christophherr";s:8:"ckoerner";s:8:"ckoerner";s:14:"claudiosanches";s:15:"Claudio Sanches";s:7:"compute";s:7:"Compute";s:13:"coreymcollins";s:13:"coreymcollins";s:8:"d4z_c0nf";s:8:"d4z_c0nf";s:11:"extendwings";s:17:"Daisuke Takahashi";s:11:"danhgilmore";s:11:"danhgilmore";s:17:"scarinessreported";s:13:"Daniel Bailey";s:5:"mte90";s:21:"Daniele Scasciafratte";s:10:"redsweater";s:27:"Daniel Jalkut (Red Sweater)";s:9:"diddledan";s:16:"Daniel Llewellyn";s:12:"danielpataki";s:12:"danielpataki";s:10:"dvankooten";s:16:"Danny van Kooten";s:16:"thewanderingbrit";s:13:"Dave Clements";s:13:"davidakennedy";s:16:"David A. Kennedy";s:14:"dbrumbaugh10up";s:15:"David Brumbaugh";s:8:"folletto";s:24:"Davide ''Folletto'' Casali";s:3:"dlh";s:13:"David Herrera";s:8:"dshanske";s:13:"David Shanske";s:17:"denis-de-bernardy";s:17:"Denis de Bernardy";s:7:"realloc";s:15:"Dennis Ploetner";s:12:"valendesigns";s:12:"Derek Herman";s:7:"dmsnell";s:7:"dmsnell";s:5:"dossy";s:14:"Dossy Shiobara";s:10:"dotancohen";s:11:"Dotan Cohen";s:11:"drebbitsweb";s:9:"Dreb Bits";s:10:"duaneblake";s:10:"duaneblake";s:6:"kucrut";s:11:"Dzikri Aziz";s:10:"eliorivero";s:11:"Elio Rivero";s:7:"codex-m";s:15:"Emerson Maningo";s:4:"enej";s:4:"enej";s:9:"ericdaams";s:10:"Eric Daams";s:8:"ethitter";s:12:"Erick Hitter";s:9:"eherman24";s:11:"Evan Herman";s:6:"fab1en";s:17:"Fabien Quatravaux";s:7:"faishal";s:7:"faishal";s:12:"fantasyworld";s:12:"fantasyworld";s:8:"flixos90";s:11:"Felix Arntz";s:5:"finnj";s:5:"finnj";s:10:"firebird75";s:10:"firebird75";s:8:"frozzare";s:14:"Fredrik Forsmo";s:11:"fusillicode";s:11:"fusillicode";s:5:"garyj";s:10:"Gary Jones";s:5:"gblsm";s:5:"gblsm";s:15:"georgestephanis";s:16:"George Stephanis";s:7:"garusky";s:15:"Giuseppe Mamone";s:8:"jubstuff";s:22:"Giustino Borzacchiello";s:11:"groovecoder";s:11:"groovecoder";s:4:"wido";s:13:"Guido Scialfa";s:7:"bordoni";s:15:"Gustavo Bordoni";s:5:"hakre";s:5:"hakre";s:11:"henrywright";s:12:"Henry Wright";s:4:"hnle";s:7:"Hinaloe";s:11:"hlashbrooke";s:15:"Hugh Lashbrooke";s:9:"hugobaeta";s:10:"Hugo Baeta";s:12:"polevaultweb";s:12:"Iain Poulson";s:7:"igmoweb";s:19:"Ignacio Cruz Moreno";s:5:"imath";s:5:"imath";s:6:"iamntz";s:12:"Ionut Staicu";s:14:"ivankristianto";s:15:"Ivan Kristianto";s:8:"jdgrimes";s:11:"J.D. Grimes";s:5:"jadpm";s:5:"jadpm";s:12:"jamesdigioia";s:13:"James DiGioia";s:15:"jason_the_adams";s:5:"Jason";s:14:"jaspermdegroot";s:15:"Jasper de Groot";s:9:"cheffheid";s:14:"Jeffrey de Wit";s:15:"jeffpyebrookcom";s:17:"Jeffrey Schutzman";s:6:"jmdodd";s:16:"Jennifer M. Dodd";s:7:"jeherve";s:12:"Jeremy Herve";s:4:"jpry";s:10:"Jeremy Pry";s:5:"jesin";s:7:"Jesin A";s:13:"ardathksheyna";s:7:"Jess G.";s:6:"boluda";s:11:"Joan Boluda";s:7:"joelerr";s:7:"joelerr";s:15:"johnjamesjacoby";s:17:"John James Jacoby";s:9:"johnnypea";s:9:"JohnnyPea";s:8:"jbrinley";s:16:"Jonathan Brinley";s:12:"spacedmonkey";s:12:"Jonny Harris";s:7:"keraweb";s:13:"Jory Hogeveen";s:8:"joefusco";s:12:"Joseph Fusco";s:12:"joshlevinson";s:13:"Josh Levinson";s:7:"shelob9";s:12:"Josh Pollock";s:7:"jrchamp";s:7:"jrchamp";s:3:"jrf";s:3:"jrf";s:7:"juanfra";s:16:"Juanfra Aldasoro";s:6:"juhise";s:11:"Juhi Saxena";s:8:"juliobox";s:12:"Julio Potier";s:10:"katieburch";s:10:"katieburch";s:6:"ryelle";s:10:"Kelly Dwan";s:5:"khag7";s:13:"Kevin Hagerty";s:13:"kiranpotphode";s:14:"Kiran Potphode";s:7:"ixkaito";s:4:"Kite";s:6:"kjbenk";s:6:"kjbenk";s:9:"kovshenin";s:20:"Konstantin Kovshenin";s:10:"kouratoras";s:23:"Konstantinos Kouratoras";s:8:"krissiev";s:8:"KrissieV";s:12:"lancewillett";s:13:"Lance Willett";s:6:"leemon";s:6:"leemon";s:7:"layotte";s:10:"Lew Ayotte";s:11:"liamdempsey";s:12:"Liam Dempsey";s:10:"luan-ramos";s:10:"Luan Ramos";s:10:"luciole135";s:10:"luciole135";s:7:"lpawlik";s:12:"Lukas Pawlik";s:4:"latz";s:17:"Lutz Schr&#246;er";s:6:"madvic";s:6:"madvic";s:11:"marcochiesi";s:12:"Marco Chiesi";s:5:"tyxla";s:14:"Marin Atanasov";s:9:"nofearinc";s:12:"Mario Peshev";s:11:"mark8barnes";s:11:"Mark Barnes";s:12:"markoheijnen";s:13:"Marko Heijnen";s:4:"mapk";s:11:"Mark Uraine";s:10:"mattfelten";s:11:"Matt Felten";s:8:"mattgeri";s:8:"MattGeri";s:9:"mattwiebe";s:10:"Matt Wiebe";s:7:"maweder";s:7:"maweder";s:9:"mayukojpn";s:13:"Mayo Moriyama";s:9:"mcapybara";s:9:"mcapybara";s:13:"mehulkaklotar";s:14:"Mehul Kaklotar";s:6:"meitar";s:6:"Meitar";s:11:"mensmaximus";s:11:"mensmaximus";s:15:"michael-arestad";s:15:"Michael Arestad";s:11:"michalzuber";s:11:"michalzuber";s:8:"micropat";s:8:"micropat";s:4:"mdgl";s:16:"Mike Glendinning";s:12:"mikehansenme";s:11:"Mike Hansen";s:10:"mikejolley";s:11:"Mike Jolley";s:7:"dimadin";s:12:"Milan Dinić";s:11:"morganestes";s:12:"Morgan Estes";s:6:"mt8biz";s:22:"moto hachi ( mt8.biz )";s:10:"usermrpapa";s:7:"Mr Papa";s:8:"mwidmann";s:8:"mwidmann";s:8:"nexurium";s:8:"nexurium";s:12:"niallkennedy";s:13:"Niall Kennedy";s:8:"nicdford";s:8:"Nic Ford";s:8:"rabmalin";s:15:"Nilambar Sharma";s:9:"ninos-ego";s:5:"Ninos";s:5:"oaron";s:5:"oaron";s:9:"overclokk";s:9:"overclokk";s:10:"obrienlabs";s:11:"Pat O''Brien";s:7:"pbearne";s:11:"Paul Bearne";s:13:"pauldewouters";s:15:"Paul de Wouters";s:10:"sirbrillig";s:12:"Payton Swick";s:9:"perezlabs";s:10:"Perez Labs";s:10:"gungeekatx";s:11:"Pete Nelson";s:7:"cadeyrn";s:11:"petermolnar";s:13:"peterwilsoncc";s:12:"Peter Wilson";s:5:"walbo";s:32:"Petter Walb&#248; Johnsg&#229;rd";s:8:"wizzard_";s:6:"Pieter";s:7:"mordauk";s:17:"Pippin Williamson";s:10:"ptahdunbar";s:13:"Pirate Dunbar";s:11:"prettyboymp";s:11:"prettyboymp";s:8:"profforg";s:8:"Profforg";s:10:"programmin";s:10:"programmin";s:14:"rahalaboulfeth";s:15:"rahal.aboulfeth";s:5:"ramiy";s:14:"Rami Yushuvaev";s:7:"lamosty";s:15:"Rastislav Lamos";s:8:"rickalee";s:20:"Ricky Lee Whittemore";s:12:"ritteshpatel";s:12:"Ritesh Patel";s:3:"rob";s:3:"rob";s:8:"rogerhub";s:10:"Roger Chen";s:9:"romsocial";s:9:"RomSocial";s:8:"ruudjoyo";s:9:"Ruud Laan";s:4:"ryan";s:10:"Ryan Boren";s:12:"ryankienstra";s:13:"Ryan Kienstra";s:7:"welcher";s:12:"Ryan Welcher";s:11:"sagarjadhav";s:12:"Sagar Jadhav";s:7:"salcode";s:14:"Sal Ferrarello";s:14:"salvoaranzulla";s:14:"salvoaranzulla";s:12:"samhotchkiss";s:13:"Sam Hotchkiss";s:7:"rosso99";s:10:"Sara Rosso";s:12:"sarciszewski";s:17:"Scott Arciszewski";s:20:"scottbrownconsulting";s:20:"scottbrownconsulting";s:11:"sc0ttkclark";s:20:"Scott Kingsley Clark";s:11:"coffee2code";s:12:"Scott Reilly";s:6:"scribu";s:6:"scribu";s:15:"sebastianpisula";s:16:"Sebastian Pisula";s:13:"sergejmueller";s:18:"Sergej M&#252;ller";s:7:"shamess";s:5:"Shane";s:9:"shinichin";s:18:"Shinichi Nishikawa";s:6:"sidati";s:6:"Sidati";s:7:"siobhan";s:7:"Siobhan";s:12:"aargh-a-knot";s:3:"sky";s:8:"slushman";s:8:"slushman";s:9:"smerriman";s:9:"smerriman";s:14:"stephanethomas";s:14:"stephanethomas";s:6:"netweb";s:13:"Stephen Edgar";s:13:"stephenharris";s:14:"Stephen Harris";s:13:"stevegrunwell";s:14:"Steve Grunwell";s:11:"stevenkword";s:11:"Steven Word";s:12:"charlestonsw";s:18:"Store Locator Plus";s:11:"subharanjan";s:11:"Subharanjan";s:5:"sudar";s:11:"Sudar Muthu";s:5:"5um17";s:11:"Sumit Singh";s:9:"tacoverdo";s:16:"Taco Verdonschot";s:8:"tahteche";s:8:"tahteche";s:10:"iamtakashi";s:12:"Takashi Irie";s:12:"takayukister";s:16:"Takayuki Miyoshi";s:12:"tharsheblows";s:12:"tharsheblows";s:8:"themiked";s:8:"theMikeD";s:8:"thomaswm";s:8:"thomaswm";s:17:"timothyblynjacobs";s:14:"Timothy Jacobs";s:11:"timplunkett";s:11:"timplunkett";s:7:"tmuikku";s:7:"tmuikku";s:8:"skithund";s:17:"Toni Viemer&#246;";s:9:"toro_unit";s:25:"Toro_Unit (Hiroshi Urabe)";s:8:"liljimmi";s:14:"Tracy Levesque";s:6:"rilwis";s:18:"Tran Ngoc Tuan Anh";s:7:"wpsmith";s:12:"Travis Smith";s:7:"tywayne";s:10:"Ty Carlson";s:14:"grapplerulrich";s:6:"Ulrich";s:12:"utkarshpatel";s:7:"Utkarsh";s:8:"vhomenko";s:8:"vhomenko";s:11:"virgodesign";s:11:"virgodesign";s:9:"vladolaru";s:10:"vlad.olaru";s:14:"voldemortensen";s:14:"voldemortensen";s:5:"vtieu";s:5:"vtieu";s:8:"webaware";s:8:"webaware";s:7:"wesleye";s:14:"Wesley Elfring";s:9:"wisdmlabs";s:9:"WisdmLabs";s:12:"wp-architect";s:12:"wp-architect";s:11:"wpdelighter";s:12:"WP Delighter";s:7:"xavortm";s:7:"xavortm";s:16:"yetanotherdaniel";s:16:"yetAnotherDaniel";s:7:"zinigor";s:7:"zinigor";}}s:10:"validators";a:4:{s:4:"name";s:11:"Translators";s:4:"type";s:7:"compact";s:7:"shuffle";b:1;s:4:"data";a:6:{s:5:"htdat";a:3:{i:0;s:5:"htdat";i:1;s:32:"13dcfc284d916a2bfc735bf521943ed2";i:2;s:5:"htdat";}s:10:"huynetbase";a:3:{i:0;s:10:"huynetbase";i:1;s:32:"0ad158f76567fab398480c8645f7505f";i:2;s:10:"huynetbase";}s:17:"philiparthurmoore";a:3:{i:0;s:19:"Philip Arthur Moore";i:1;s:32:"575b9511eb725e354e03ede7123fac4f";i:2;s:17:"philiparthurmoore";}s:9:"tonybui-1";a:3:{i:0;s:15:"Tony WooRockets";i:1;s:32:"c9f8cb6da18358d57e26f90cb0440cf9";i:2;s:9:"tonybui-1";}s:6:"rilwis";a:3:{i:0;s:18:"Tran Ngoc Tuan Anh";i:1;s:32:"7dd32111fd3b1efa31f93d98bd72172c";i:2;s:6:"rilwis";}s:4:"tucq";a:3:{i:0;s:4:"tucq";i:1;s:32:"547f6e8d42c67dbabe796a5162acd53e";i:2;s:4:"tucq";}}}s:11:"translators";a:3:{s:4:"name";b:0;s:4:"type";s:4:"list";s:4:"data";a:10:{s:6:"buboda";s:6:"buboda";s:7:"mcjambi";s:8:"Jam Viet";s:11:"luanthien97";s:11:"luanthien97";s:6:"luanvn";s:6:"luanvn";s:5:"mdsmj";s:5:"mdsmj";s:11:"nguyentu229";s:11:"nguyentu229";s:6:"phuchm";s:6:"phuchm";s:6:"sevent";s:6:"SevenT";s:10:"vptunguyen";s:9:"Tu Nguyen";s:11:"vungocson94";s:15:"Vũ Ngọc Sơn";}}s:9:"libraries";a:3:{s:4:"name";s:18:"External Libraries";s:4:"type";s:9:"libraries";s:4:"data";a:31:{i:0;a:2:{i:0;s:11:"Backbone.js";i:1;s:22:"http://backbonejs.org/";}i:1;a:2:{i:0;s:10:"Class POP3";i:1;s:24:"http://squirrelmail.org/";}i:2;a:2:{i:0;s:16:"Color Animations";i:1;s:32:"http://plugins.jquery.com/color/";}i:3;a:2:{i:0;s:8:"getID3()";i:1;s:30:"http://getid3.sourceforge.net/";}i:4;a:2:{i:0;s:15:"Horde Text Diff";i:1;s:22:"http://pear.horde.org/";}i:5;a:2:{i:0;s:11:"hoverIntent";i:1;s:57:"http://cherne.net/brian/resources/jquery.hoverIntent.html";}i:6;a:2:{i:0;s:13:"imgAreaSelect";i:1;s:42:"http://odyniec.net/projects/imgareaselect/";}i:7;a:2:{i:0;s:4:"Iris";i:1;s:34:"https://github.com/Automattic/Iris";}i:8;a:2:{i:0;s:6:"jQuery";i:1;s:18:"http://jquery.com/";}i:9;a:2:{i:0;s:9:"jQuery UI";i:1;s:20:"http://jqueryui.com/";}i:10;a:2:{i:0;s:14:"jQuery Hotkeys";i:1;s:41:"https://github.com/tzuryby/jquery.hotkeys";}i:11;a:2:{i:0;s:22:"jQuery serializeObject";i:1;s:49:"http://benalman.com/projects/jquery-misc-plugins/";}i:12;a:2:{i:0;s:12:"jQuery.query";i:1;s:39:"http://plugins.jquery.com/query-object/";}i:13;a:2:{i:0;s:14:"jQuery.suggest";i:1;s:41:"http://plugins.jquery.com/project/suggest";}i:14;a:2:{i:0;s:21:"jQuery UI Touch Punch";i:1;s:27:"http://touchpunch.furf.com/";}i:15;a:2:{i:0;s:5:"json2";i:1;s:43:"https://github.com/douglascrockford/JSON-js";}i:16;a:2:{i:0;s:7:"Masonry";i:1;s:28:"http://masonry.desandro.com/";}i:17;a:2:{i:0;s:15:"MediaElement.js";i:1;s:26:"http://mediaelementjs.com/";}i:18;a:2:{i:0;s:6:"PclZip";i:1;s:33:"http://www.phpconcept.net/pclzip/";}i:19;a:2:{i:0;s:6:"PemFTP";i:1;s:50:"http://www.phpclasses.org/browse/package/1743.html";}i:20;a:2:{i:0;s:6:"phpass";i:1;s:31:"http://www.openwall.com/phpass/";}i:21;a:2:{i:0;s:9:"PHPMailer";i:1;s:38:"https://github.com/PHPMailer/PHPMailer";}i:22;a:2:{i:0;s:8:"Plupload";i:1;s:24:"http://www.plupload.com/";}i:23;a:2:{i:0;s:13:"random_compat";i:1;s:42:"https://github.com/paragonie/random_compat";}i:24;a:2:{i:0;s:9:"SimplePie";i:1;s:21:"http://simplepie.org/";}i:25;a:2:{i:0;s:27:"The Incutio XML-RPC Library";i:1;s:34:"http://scripts.incutio.com/xmlrpc/";}i:26;a:2:{i:0;s:8:"Thickbox";i:1;s:32:"http://codylindley.com/thickbox/";}i:27;a:2:{i:0;s:7:"TinyMCE";i:1;s:23:"http://www.tinymce.com/";}i:28;a:2:{i:0;s:7:"Twemoji";i:1;s:34:"https://github.com/twitter/twemoji";}i:29;a:2:{i:0;s:13:"Underscore.js";i:1;s:24:"http://underscorejs.org/";}i:30;a:2:{i:0;s:6:"zxcvbn";i:1;s:33:"https://github.com/dropbox/zxcvbn";}}}}s:4:"data";a:2:{s:8:"profiles";s:33:"https://profiles.wordpress.org/%s";s:7:"version";s:3:"4.5";}}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(902, 'wpgs_option', 'a:9:{s:9:"wpgs_nb_1";s:0:"";s:8:"wpgs_nav";s:4:"true";s:11:"wpgs_nav_cl";s:7:"#ff6b00";s:15:"wpgs_nav_ico_cl";s:4:"#fff";s:13:"wpgs_autoplay";s:4:"true";s:15:"wpgs_thumbanils";s:1:"4";s:16:"wpgs_thum_margin";s:2:"10";s:17:"wpgs_stagepadding";s:2:"10";s:9:"wpgs_nb_2";s:0:"";}', 'yes'),
(911, '_site_transient_timeout_available_translations', '1462609752', 'yes'),
(912, '_site_transient_available_translations', 'a:80:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-10 15:55:55";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:3:"ary";a:8:{s:8:"language";s:3:"ary";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 14:44:00";s:12:"english_name";s:15:"Moroccan Arabic";s:11:"native_name";s:31:"العربية المغربية";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.5.1/ary.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:3;s:3:"ary";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 22:48:01";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:3:"azb";a:8:{s:8:"language";s:3:"azb";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-11 22:42:10";s:12:"english_name";s:17:"South Azerbaijani";s:11:"native_name";s:29:"گؤنئی آذربایجان";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/azb.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:3;s:3:"azb";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-03 14:05:41";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:12:"Напред";}}s:5:"bn_BD";a:8:{s:8:"language";s:5:"bn_BD";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-08 13:17:04";s:12:"english_name";s:7:"Bengali";s:11:"native_name";s:15:"বাংলা";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/bn_BD.zip";s:3:"iso";a:1:{i:1;s:2:"bn";}s:7:"strings";a:1:{s:8:"continue";s:23:"এগিয়ে চল.";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-19 23:16:37";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-11 06:38:51";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:3:"ceb";a:8:{s:8:"language";s:3:"ceb";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-16 15:34:57";s:12:"english_name";s:7:"Cebuano";s:11:"native_name";s:7:"Cebuano";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/ceb.zip";s:3:"iso";a:2:{i:2;s:3:"ceb";i:3;s:3:"ceb";}s:7:"strings";a:1:{s:8:"continue";s:7:"Padayun";}}s:5:"cs_CZ";a:8:{s:8:"language";s:5:"cs_CZ";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-11 18:32:36";s:12:"english_name";s:5:"Czech";s:11:"native_name";s:12:"Čeština‎";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/cs_CZ.zip";s:3:"iso";a:2:{i:1;s:2:"cs";i:2;s:3:"ces";}s:7:"strings";a:1:{s:8:"continue";s:11:"Pokračovat";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-11 14:21:06";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-11 15:42:12";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 19:26:41";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-01 18:17:12";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:12:"de_DE_formal";a:8:{s:8:"language";s:12:"de_DE_formal";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-01 18:17:51";s:12:"english_name";s:15:"German (Formal)";s:11:"native_name";s:13:"Deutsch (Sie)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.5.1/de_DE_formal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:14:"de_CH_informal";a:8:{s:8:"language";s:14:"de_CH_informal";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 20:03:25";s:12:"english_name";s:23:"(Switzerland, Informal)";s:11:"native_name";s:21:"Deutsch (Schweiz, Du)";s:7:"package";s:73:"https://downloads.wordpress.org/translation/core/4.5.1/de_CH_informal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 21:14:17";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_NZ";a:8:{s:8:"language";s:5:"en_NZ";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-26 02:00:05";s:12:"english_name";s:21:"English (New Zealand)";s:11:"native_name";s:21:"English (New Zealand)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/en_NZ.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 06:26:11";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_ZA";a:8:{s:8:"language";s:5:"en_ZA";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-28 11:29:02";s:12:"english_name";s:22:"English (South Africa)";s:11:"native_name";s:22:"English (South Africa)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/en_ZA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 12:51:07";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-10 05:23:57";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-11 10:58:49";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 21:06:55";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_VE";a:8:{s:8:"language";s:5:"es_VE";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-28 13:08:25";s:12:"english_name";s:19:"Spanish (Venezuela)";s:11:"native_name";s:21:"Español de Venezuela";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/es_VE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-28 13:34:35";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_GT";a:8:{s:8:"language";s:5:"es_GT";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 12:43:00";s:12:"english_name";s:19:"Spanish (Guatemala)";s:11:"native_name";s:21:"Español de Guatemala";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/es_GT.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CO";a:8:{s:8:"language";s:5:"es_CO";s:7:"version";s:6:"4.3-RC";s:7:"updated";s:19:"2015-08-04 06:10:33";s:12:"english_name";s:18:"Spanish (Colombia)";s:11:"native_name";s:20:"Español de Colombia";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.3-RC/es_CO.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 01:09:28";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-16 17:35:43";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_AR";a:8:{s:8:"language";s:5:"es_AR";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-19 21:32:12";s:12:"english_name";s:19:"Spanish (Argentina)";s:11:"native_name";s:21:"Español de Argentina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/es_AR.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"et";a:8:{s:8:"language";s:2:"et";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 11:11:25";s:12:"english_name";s:8:"Estonian";s:11:"native_name";s:5:"Eesti";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/et.zip";s:3:"iso";a:2:{i:1;s:2:"et";i:2;s:3:"est";}s:7:"strings";a:1:{s:8:"continue";s:6:"Jätka";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-23 22:05:23";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-01-31 19:24:20";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-10 18:44:50";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_BE";a:8:{s:8:"language";s:5:"fr_BE";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-11 07:33:47";s:12:"english_name";s:16:"French (Belgium)";s:11:"native_name";s:21:"Français de Belgique";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/fr_BE.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_CA";a:8:{s:8:"language";s:5:"fr_CA";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-29 19:30:46";s:12:"english_name";s:15:"French (Canada)";s:11:"native_name";s:19:"Français du Canada";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/fr_CA.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-29 13:55:46";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:5:"4.3.3";s:7:"updated";s:19:"2015-09-24 15:25:30";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.3.3/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-22 23:06:30";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-05 00:59:09";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip";s:3:"iso";a:1:{i:3;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-16 13:14:11";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:8:"המשך";}}s:5:"hi_IN";a:8:{s:8:"language";s:5:"hi_IN";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-28 07:29:36";s:12:"english_name";s:5:"Hindi";s:11:"native_name";s:18:"हिन्दी";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/hi_IN.zip";s:3:"iso";a:2:{i:1;s:2:"hi";i:2;s:3:"hin";}s:7:"strings";a:1:{s:8:"continue";s:12:"जारी";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-25 09:54:06";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-03 06:34:38";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:10:"Folytatás";}}s:2:"hy";a:8:{s:8:"language";s:2:"hy";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-04 07:13:54";s:12:"english_name";s:8:"Armenian";s:11:"native_name";s:14:"Հայերեն";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/hy.zip";s:3:"iso";a:2:{i:1;s:2:"hy";i:2;s:3:"hye";}s:7:"strings";a:1:{s:8:"continue";s:20:"Շարունակել";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-21 16:17:50";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-30 15:18:26";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-02 17:37:00";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-27 00:36:15";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ka_GE";a:8:{s:8:"language";s:5:"ka_GE";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-11 09:29:35";s:12:"english_name";s:8:"Georgian";s:11:"native_name";s:21:"ქართული";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/ka_GE.zip";s:3:"iso";a:2:{i:1;s:2:"ka";i:2;s:3:"kat";}s:7:"strings";a:1:{s:8:"continue";s:30:"გაგრძელება";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-02 03:21:50";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-10 06:34:16";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:2:"mr";a:8:{s:8:"language";s:2:"mr";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-29 09:37:07";s:12:"english_name";s:7:"Marathi";s:11:"native_name";s:15:"मराठी";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/mr.zip";s:3:"iso";a:2:{i:1;s:2:"mr";i:2;s:3:"mar";}s:7:"strings";a:1:{s:8:"continue";s:25:"सुरु ठेवा";}}s:5:"ms_MY";a:8:{s:8:"language";s:5:"ms_MY";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-01-28 05:41:39";s:12:"english_name";s:5:"Malay";s:11:"native_name";s:13:"Bahasa Melayu";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/ms_MY.zip";s:3:"iso";a:2:{i:1;s:2:"ms";i:2;s:3:"msa";}s:7:"strings";a:1:{s:8:"continue";s:8:"Teruskan";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:6:"4.1.10";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.10/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ဆောင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 12:35:50";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-02 15:16:51";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:12:"nl_NL_formal";a:8:{s:8:"language";s:12:"nl_NL_formal";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-01-20 13:35:50";s:12:"english_name";s:14:"Dutch (Formal)";s:11:"native_name";s:20:"Nederlands (Formeel)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.4.2/nl_NL_formal.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-11 07:36:04";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-04-26 06:46:10";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-27 07:33:40";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:6:"4.1.10";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.10/ps.zip";s:3:"iso";a:2:{i:1;s:2:"ps";i:2;s:3:"pus";}s:7:"strings";a:1:{s:8:"continue";s:19:"دوام ورکړه";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-04 11:44:03";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-27 10:21:39";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-05 13:00:51";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-13 18:04:14";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-27 07:36:55";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-11-26 00:00:18";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:8:"Nadaljuj";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 10:47:53";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-10 08:00:57";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 18:15:27";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-22 14:05:41";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:2:"tl";a:8:{s:8:"language";s:2:"tl";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-11-27 15:51:36";s:12:"english_name";s:7:"Tagalog";s:11:"native_name";s:7:"Tagalog";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/tl.zip";s:3:"iso";a:2:{i:1;s:2:"tl";i:2;s:3:"tgl";}s:7:"strings";a:1:{s:8:"continue";s:10:"Magpatuloy";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-21 01:31:12";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:6:"4.1.10";s:7:"updated";s:19:"2015-03-26 16:45:38";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.10/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-05-03 13:19:01";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.1/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:2:"vi";a:8:{s:8:"language";s:2:"vi";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-09 01:01:25";s:12:"english_name";s:10:"Vietnamese";s:11:"native_name";s:14:"Tiếng Việt";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/vi.zip";s:3:"iso";a:2:{i:1;s:2:"vi";i:2;s:3:"vie";}s:7:"strings";a:1:{s:8:"continue";s:12:"Tiếp tục";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-12 09:08:07";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.5.1";s:7:"updated";s:19:"2016-04-17 03:29:01";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.1/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}}', 'yes'),
(919, 'auto_core_update_notified', 'a:4:{s:4:"type";s:7:"success";s:5:"email";s:22:"hongdiepbach@gmail.com";s:7:"version";s:5:"4.5.2";s:9:"timestamp";i:1462649401;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(933, 'woo_dynamic_gallery_google_font_list', 'a:712:{i:0;a:2:{s:4:"name";s:7:"ABeeZee";s:7:"variant";s:15:":regular,italic";}i:1;a:2:{s:4:"name";s:4:"Abel";s:7:"variant";s:8:":regular";}i:2;a:2:{s:4:"name";s:13:"Abril Fatface";s:7:"variant";s:8:":regular";}i:3;a:2:{s:4:"name";s:8:"Aclonica";s:7:"variant";s:8:":regular";}i:4;a:2:{s:4:"name";s:4:"Acme";s:7:"variant";s:8:":regular";}i:5;a:2:{s:4:"name";s:5:"Actor";s:7:"variant";s:8:":regular";}i:6;a:2:{s:4:"name";s:7:"Adamina";s:7:"variant";s:8:":regular";}i:7;a:2:{s:4:"name";s:10:"Advent Pro";s:7:"variant";s:32:":100,200,300,regular,500,600,700";}i:8;a:2:{s:4:"name";s:15:"Aguafina Script";s:7:"variant";s:8:":regular";}i:9;a:2:{s:4:"name";s:7:"Akronim";s:7:"variant";s:8:":regular";}i:10;a:2:{s:4:"name";s:6:"Aladin";s:7:"variant";s:8:":regular";}i:11;a:2:{s:4:"name";s:7:"Aldrich";s:7:"variant";s:8:":regular";}i:12;a:2:{s:4:"name";s:4:"Alef";s:7:"variant";s:12:":regular,700";}i:13;a:2:{s:4:"name";s:8:"Alegreya";s:7:"variant";s:43:":regular,italic,700,700italic,900,900italic";}i:14;a:2:{s:4:"name";s:11:"Alegreya SC";s:7:"variant";s:43:":regular,italic,700,700italic,900,900italic";}i:15;a:2:{s:4:"name";s:13:"Alegreya Sans";s:7:"variant";s:99:":100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,800,800italic,900,900italic";}i:16;a:2:{s:4:"name";s:16:"Alegreya Sans SC";s:7:"variant";s:99:":100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,800,800italic,900,900italic";}i:17;a:2:{s:4:"name";s:10:"Alex Brush";s:7:"variant";s:8:":regular";}i:18;a:2:{s:4:"name";s:13:"Alfa Slab One";s:7:"variant";s:8:":regular";}i:19;a:2:{s:4:"name";s:5:"Alice";s:7:"variant";s:8:":regular";}i:20;a:2:{s:4:"name";s:5:"Alike";s:7:"variant";s:8:":regular";}i:21;a:2:{s:4:"name";s:13:"Alike Angular";s:7:"variant";s:8:":regular";}i:22;a:2:{s:4:"name";s:5:"Allan";s:7:"variant";s:12:":regular,700";}i:23;a:2:{s:4:"name";s:7:"Allerta";s:7:"variant";s:8:":regular";}i:24;a:2:{s:4:"name";s:15:"Allerta Stencil";s:7:"variant";s:8:":regular";}i:25;a:2:{s:4:"name";s:6:"Allura";s:7:"variant";s:8:":regular";}i:26;a:2:{s:4:"name";s:8:"Almendra";s:7:"variant";s:29:":regular,italic,700,700italic";}i:27;a:2:{s:4:"name";s:16:"Almendra Display";s:7:"variant";s:8:":regular";}i:28;a:2:{s:4:"name";s:11:"Almendra SC";s:7:"variant";s:8:":regular";}i:29;a:2:{s:4:"name";s:8:"Amarante";s:7:"variant";s:8:":regular";}i:30;a:2:{s:4:"name";s:8:"Amaranth";s:7:"variant";s:29:":regular,italic,700,700italic";}i:31;a:2:{s:4:"name";s:9:"Amatic SC";s:7:"variant";s:12:":regular,700";}i:32;a:2:{s:4:"name";s:9:"Amethysta";s:7:"variant";s:8:":regular";}i:33;a:2:{s:4:"name";s:5:"Amiri";s:7:"variant";s:29:":regular,italic,700,700italic";}i:34;a:2:{s:4:"name";s:5:"Amita";s:7:"variant";s:12:":regular,700";}i:35;a:2:{s:4:"name";s:7:"Anaheim";s:7:"variant";s:8:":regular";}i:36;a:2:{s:4:"name";s:6:"Andada";s:7:"variant";s:8:":regular";}i:37;a:2:{s:4:"name";s:6:"Andika";s:7:"variant";s:8:":regular";}i:38;a:2:{s:4:"name";s:6:"Angkor";s:7:"variant";s:8:":regular";}i:39;a:2:{s:4:"name";s:24:"Annie Use Your Telescope";s:7:"variant";s:8:":regular";}i:40;a:2:{s:4:"name";s:13:"Anonymous Pro";s:7:"variant";s:29:":regular,italic,700,700italic";}i:41;a:2:{s:4:"name";s:5:"Antic";s:7:"variant";s:8:":regular";}i:42;a:2:{s:4:"name";s:12:"Antic Didone";s:7:"variant";s:8:":regular";}i:43;a:2:{s:4:"name";s:10:"Antic Slab";s:7:"variant";s:8:":regular";}i:44;a:2:{s:4:"name";s:5:"Anton";s:7:"variant";s:8:":regular";}i:45;a:2:{s:4:"name";s:6:"Arapey";s:7:"variant";s:15:":regular,italic";}i:46;a:2:{s:4:"name";s:7:"Arbutus";s:7:"variant";s:8:":regular";}i:47;a:2:{s:4:"name";s:12:"Arbutus Slab";s:7:"variant";s:8:":regular";}i:48;a:2:{s:4:"name";s:19:"Architects Daughter";s:7:"variant";s:8:":regular";}i:49;a:2:{s:4:"name";s:13:"Archivo Black";s:7:"variant";s:8:":regular";}i:50;a:2:{s:4:"name";s:14:"Archivo Narrow";s:7:"variant";s:29:":regular,italic,700,700italic";}i:51;a:2:{s:4:"name";s:5:"Arimo";s:7:"variant";s:29:":regular,italic,700,700italic";}i:52;a:2:{s:4:"name";s:8:"Arizonia";s:7:"variant";s:8:":regular";}i:53;a:2:{s:4:"name";s:6:"Armata";s:7:"variant";s:8:":regular";}i:54;a:2:{s:4:"name";s:8:"Artifika";s:7:"variant";s:8:":regular";}i:55;a:2:{s:4:"name";s:4:"Arvo";s:7:"variant";s:29:":regular,italic,700,700italic";}i:56;a:2:{s:4:"name";s:4:"Arya";s:7:"variant";s:12:":regular,700";}i:57;a:2:{s:4:"name";s:4:"Asap";s:7:"variant";s:29:":regular,italic,700,700italic";}i:58;a:2:{s:4:"name";s:5:"Asset";s:7:"variant";s:8:":regular";}i:59;a:2:{s:4:"name";s:7:"Astloch";s:7:"variant";s:12:":regular,700";}i:60;a:2:{s:4:"name";s:4:"Asul";s:7:"variant";s:12:":regular,700";}i:61;a:2:{s:4:"name";s:10:"Atomic Age";s:7:"variant";s:8:":regular";}i:62;a:2:{s:4:"name";s:6:"Aubrey";s:7:"variant";s:8:":regular";}i:63;a:2:{s:4:"name";s:9:"Audiowide";s:7:"variant";s:8:":regular";}i:64;a:2:{s:4:"name";s:10:"Autour One";s:7:"variant";s:8:":regular";}i:65;a:2:{s:4:"name";s:7:"Average";s:7:"variant";s:8:":regular";}i:66;a:2:{s:4:"name";s:12:"Average Sans";s:7:"variant";s:8:":regular";}i:67;a:2:{s:4:"name";s:19:"Averia Gruesa Libre";s:7:"variant";s:8:":regular";}i:68;a:2:{s:4:"name";s:12:"Averia Libre";s:7:"variant";s:43:":300,300italic,regular,italic,700,700italic";}i:69;a:2:{s:4:"name";s:17:"Averia Sans Libre";s:7:"variant";s:43:":300,300italic,regular,italic,700,700italic";}i:70;a:2:{s:4:"name";s:18:"Averia Serif Libre";s:7:"variant";s:43:":300,300italic,regular,italic,700,700italic";}i:71;a:2:{s:4:"name";s:10:"Bad Script";s:7:"variant";s:8:":regular";}i:72;a:2:{s:4:"name";s:9:"Balthazar";s:7:"variant";s:8:":regular";}i:73;a:2:{s:4:"name";s:7:"Bangers";s:7:"variant";s:8:":regular";}i:74;a:2:{s:4:"name";s:5:"Basic";s:7:"variant";s:8:":regular";}i:75;a:2:{s:4:"name";s:10:"Battambang";s:7:"variant";s:12:":regular,700";}i:76;a:2:{s:4:"name";s:7:"Baumans";s:7:"variant";s:8:":regular";}i:77;a:2:{s:4:"name";s:5:"Bayon";s:7:"variant";s:8:":regular";}i:78;a:2:{s:4:"name";s:8:"Belgrano";s:7:"variant";s:8:":regular";}i:79;a:2:{s:4:"name";s:7:"Belleza";s:7:"variant";s:8:":regular";}i:80;a:2:{s:4:"name";s:9:"BenchNine";s:7:"variant";s:16:":300,regular,700";}i:81;a:2:{s:4:"name";s:7:"Bentham";s:7:"variant";s:8:":regular";}i:82;a:2:{s:4:"name";s:15:"Berkshire Swash";s:7:"variant";s:8:":regular";}i:83;a:2:{s:4:"name";s:5:"Bevan";s:7:"variant";s:8:":regular";}i:84;a:2:{s:4:"name";s:13:"Bigelow Rules";s:7:"variant";s:8:":regular";}i:85;a:2:{s:4:"name";s:11:"Bigshot One";s:7:"variant";s:8:":regular";}i:86;a:2:{s:4:"name";s:5:"Bilbo";s:7:"variant";s:8:":regular";}i:87;a:2:{s:4:"name";s:16:"Bilbo Swash Caps";s:7:"variant";s:8:":regular";}i:88;a:2:{s:4:"name";s:7:"Biryani";s:7:"variant";s:32:":200,300,regular,600,700,800,900";}i:89;a:2:{s:4:"name";s:6:"Bitter";s:7:"variant";s:19:":regular,italic,700";}i:90;a:2:{s:4:"name";s:13:"Black Ops One";s:7:"variant";s:8:":regular";}i:91;a:2:{s:4:"name";s:5:"Bokor";s:7:"variant";s:8:":regular";}i:92;a:2:{s:4:"name";s:6:"Bonbon";s:7:"variant";s:8:":regular";}i:93;a:2:{s:4:"name";s:8:"Boogaloo";s:7:"variant";s:8:":regular";}i:94;a:2:{s:4:"name";s:10:"Bowlby One";s:7:"variant";s:8:":regular";}i:95;a:2:{s:4:"name";s:13:"Bowlby One SC";s:7:"variant";s:8:":regular";}i:96;a:2:{s:4:"name";s:7:"Brawler";s:7:"variant";s:8:":regular";}i:97;a:2:{s:4:"name";s:10:"Bree Serif";s:7:"variant";s:8:":regular";}i:98;a:2:{s:4:"name";s:14:"Bubblegum Sans";s:7:"variant";s:8:":regular";}i:99;a:2:{s:4:"name";s:11:"Bubbler One";s:7:"variant";s:8:":regular";}i:100;a:2:{s:4:"name";s:4:"Buda";s:7:"variant";s:4:":300";}i:101;a:2:{s:4:"name";s:7:"Buenard";s:7:"variant";s:12:":regular,700";}i:102;a:2:{s:4:"name";s:10:"Butcherman";s:7:"variant";s:8:":regular";}i:103;a:2:{s:4:"name";s:14:"Butterfly Kids";s:7:"variant";s:8:":regular";}i:104;a:2:{s:4:"name";s:5:"Cabin";s:7:"variant";s:57:":regular,italic,500,500italic,600,600italic,700,700italic";}i:105;a:2:{s:4:"name";s:15:"Cabin Condensed";s:7:"variant";s:20:":regular,500,600,700";}i:106;a:2:{s:4:"name";s:12:"Cabin Sketch";s:7:"variant";s:12:":regular,700";}i:107;a:2:{s:4:"name";s:15:"Caesar Dressing";s:7:"variant";s:8:":regular";}i:108;a:2:{s:4:"name";s:10:"Cagliostro";s:7:"variant";s:8:":regular";}i:109;a:2:{s:4:"name";s:14:"Calligraffitti";s:7:"variant";s:8:":regular";}i:110;a:2:{s:4:"name";s:6:"Cambay";s:7:"variant";s:29:":regular,italic,700,700italic";}i:111;a:2:{s:4:"name";s:5:"Cambo";s:7:"variant";s:8:":regular";}i:112;a:2:{s:4:"name";s:6:"Candal";s:7:"variant";s:8:":regular";}i:113;a:2:{s:4:"name";s:9:"Cantarell";s:7:"variant";s:29:":regular,italic,700,700italic";}i:114;a:2:{s:4:"name";s:11:"Cantata One";s:7:"variant";s:8:":regular";}i:115;a:2:{s:4:"name";s:11:"Cantora One";s:7:"variant";s:8:":regular";}i:116;a:2:{s:4:"name";s:8:"Capriola";s:7:"variant";s:8:":regular";}i:117;a:2:{s:4:"name";s:5:"Cardo";s:7:"variant";s:19:":regular,italic,700";}i:118;a:2:{s:4:"name";s:5:"Carme";s:7:"variant";s:8:":regular";}i:119;a:2:{s:4:"name";s:14:"Carrois Gothic";s:7:"variant";s:8:":regular";}i:120;a:2:{s:4:"name";s:17:"Carrois Gothic SC";s:7:"variant";s:8:":regular";}i:121;a:2:{s:4:"name";s:10:"Carter One";s:7:"variant";s:8:":regular";}i:122;a:2:{s:4:"name";s:6:"Caudex";s:7:"variant";s:29:":regular,italic,700,700italic";}i:123;a:2:{s:4:"name";s:18:"Cedarville Cursive";s:7:"variant";s:8:":regular";}i:124;a:2:{s:4:"name";s:11:"Ceviche One";s:7:"variant";s:8:":regular";}i:125;a:2:{s:4:"name";s:10:"Changa One";s:7:"variant";s:15:":regular,italic";}i:126;a:2:{s:4:"name";s:6:"Chango";s:7:"variant";s:8:":regular";}i:127;a:2:{s:4:"name";s:18:"Chau Philomene One";s:7:"variant";s:15:":regular,italic";}i:128;a:2:{s:4:"name";s:9:"Chela One";s:7:"variant";s:8:":regular";}i:129;a:2:{s:4:"name";s:14:"Chelsea Market";s:7:"variant";s:8:":regular";}i:130;a:2:{s:4:"name";s:6:"Chenla";s:7:"variant";s:8:":regular";}i:131;a:2:{s:4:"name";s:17:"Cherry Cream Soda";s:7:"variant";s:8:":regular";}i:132;a:2:{s:4:"name";s:12:"Cherry Swash";s:7:"variant";s:12:":regular,700";}i:133;a:2:{s:4:"name";s:5:"Chewy";s:7:"variant";s:8:":regular";}i:134;a:2:{s:4:"name";s:6:"Chicle";s:7:"variant";s:8:":regular";}i:135;a:2:{s:4:"name";s:5:"Chivo";s:7:"variant";s:29:":regular,italic,900,900italic";}i:136;a:2:{s:4:"name";s:6:"Cinzel";s:7:"variant";s:16:":regular,700,900";}i:137;a:2:{s:4:"name";s:17:"Cinzel Decorative";s:7:"variant";s:16:":regular,700,900";}i:138;a:2:{s:4:"name";s:14:"Clicker Script";s:7:"variant";s:8:":regular";}i:139;a:2:{s:4:"name";s:4:"Coda";s:7:"variant";s:12:":regular,800";}i:140;a:2:{s:4:"name";s:12:"Coda Caption";s:7:"variant";s:4:":800";}i:141;a:2:{s:4:"name";s:8:"Codystar";s:7:"variant";s:12:":300,regular";}i:142;a:2:{s:4:"name";s:5:"Combo";s:7:"variant";s:8:":regular";}i:143;a:2:{s:4:"name";s:9:"Comfortaa";s:7:"variant";s:16:":300,regular,700";}i:144;a:2:{s:4:"name";s:11:"Coming Soon";s:7:"variant";s:8:":regular";}i:145;a:2:{s:4:"name";s:11:"Concert One";s:7:"variant";s:8:":regular";}i:146;a:2:{s:4:"name";s:9:"Condiment";s:7:"variant";s:8:":regular";}i:147;a:2:{s:4:"name";s:7:"Content";s:7:"variant";s:12:":regular,700";}i:148;a:2:{s:4:"name";s:12:"Contrail One";s:7:"variant";s:8:":regular";}i:149;a:2:{s:4:"name";s:11:"Convergence";s:7:"variant";s:8:":regular";}i:150;a:2:{s:4:"name";s:6:"Cookie";s:7:"variant";s:8:":regular";}i:151;a:2:{s:4:"name";s:5:"Copse";s:7:"variant";s:8:":regular";}i:152;a:2:{s:4:"name";s:6:"Corben";s:7:"variant";s:12:":regular,700";}i:153;a:2:{s:4:"name";s:9:"Courgette";s:7:"variant";s:8:":regular";}i:154;a:2:{s:4:"name";s:7:"Cousine";s:7:"variant";s:29:":regular,italic,700,700italic";}i:155;a:2:{s:4:"name";s:8:"Coustard";s:7:"variant";s:12:":regular,900";}i:156;a:2:{s:4:"name";s:21:"Covered By Your Grace";s:7:"variant";s:8:":regular";}i:157;a:2:{s:4:"name";s:12:"Crafty Girls";s:7:"variant";s:8:":regular";}i:158;a:2:{s:4:"name";s:9:"Creepster";s:7:"variant";s:8:":regular";}i:159;a:2:{s:4:"name";s:11:"Crete Round";s:7:"variant";s:15:":regular,italic";}i:160;a:2:{s:4:"name";s:12:"Crimson Text";s:7:"variant";s:43:":regular,italic,600,600italic,700,700italic";}i:161;a:2:{s:4:"name";s:13:"Croissant One";s:7:"variant";s:8:":regular";}i:162;a:2:{s:4:"name";s:7:"Crushed";s:7:"variant";s:8:":regular";}i:163;a:2:{s:4:"name";s:6:"Cuprum";s:7:"variant";s:29:":regular,italic,700,700italic";}i:164;a:2:{s:4:"name";s:6:"Cutive";s:7:"variant";s:8:":regular";}i:165;a:2:{s:4:"name";s:11:"Cutive Mono";s:7:"variant";s:8:":regular";}i:166;a:2:{s:4:"name";s:6:"Damion";s:7:"variant";s:8:":regular";}i:167;a:2:{s:4:"name";s:14:"Dancing Script";s:7:"variant";s:12:":regular,700";}i:168;a:2:{s:4:"name";s:7:"Dangrek";s:7:"variant";s:8:":regular";}i:169;a:2:{s:4:"name";s:20:"Dawning of a New Day";s:7:"variant";s:8:":regular";}i:170;a:2:{s:4:"name";s:8:"Days One";s:7:"variant";s:8:":regular";}i:171;a:2:{s:4:"name";s:5:"Dekko";s:7:"variant";s:8:":regular";}i:172;a:2:{s:4:"name";s:6:"Delius";s:7:"variant";s:8:":regular";}i:173;a:2:{s:4:"name";s:17:"Delius Swash Caps";s:7:"variant";s:8:":regular";}i:174;a:2:{s:4:"name";s:14:"Delius Unicase";s:7:"variant";s:12:":regular,700";}i:175;a:2:{s:4:"name";s:13:"Della Respira";s:7:"variant";s:8:":regular";}i:176;a:2:{s:4:"name";s:8:"Denk One";s:7:"variant";s:8:":regular";}i:177;a:2:{s:4:"name";s:10:"Devonshire";s:7:"variant";s:8:":regular";}i:178;a:2:{s:4:"name";s:8:"Dhurjati";s:7:"variant";s:8:":regular";}i:179;a:2:{s:4:"name";s:13:"Didact Gothic";s:7:"variant";s:8:":regular";}i:180;a:2:{s:4:"name";s:9:"Diplomata";s:7:"variant";s:8:":regular";}i:181;a:2:{s:4:"name";s:12:"Diplomata SC";s:7:"variant";s:8:":regular";}i:182;a:2:{s:4:"name";s:6:"Domine";s:7:"variant";s:12:":regular,700";}i:183;a:2:{s:4:"name";s:11:"Donegal One";s:7:"variant";s:8:":regular";}i:184;a:2:{s:4:"name";s:10:"Doppio One";s:7:"variant";s:8:":regular";}i:185;a:2:{s:4:"name";s:5:"Dorsa";s:7:"variant";s:8:":regular";}i:186;a:2:{s:4:"name";s:5:"Dosis";s:7:"variant";s:32:":200,300,regular,500,600,700,800";}i:187;a:2:{s:4:"name";s:11:"Dr Sugiyama";s:7:"variant";s:8:":regular";}i:188;a:2:{s:4:"name";s:10:"Droid Sans";s:7:"variant";s:12:":regular,700";}i:189;a:2:{s:4:"name";s:15:"Droid Sans Mono";s:7:"variant";s:8:":regular";}i:190;a:2:{s:4:"name";s:11:"Droid Serif";s:7:"variant";s:29:":regular,italic,700,700italic";}i:191;a:2:{s:4:"name";s:9:"Duru Sans";s:7:"variant";s:8:":regular";}i:192;a:2:{s:4:"name";s:9:"Dynalight";s:7:"variant";s:8:":regular";}i:193;a:2:{s:4:"name";s:11:"EB Garamond";s:7:"variant";s:8:":regular";}i:194;a:2:{s:4:"name";s:10:"Eagle Lake";s:7:"variant";s:8:":regular";}i:195;a:2:{s:4:"name";s:5:"Eater";s:7:"variant";s:8:":regular";}i:196;a:2:{s:4:"name";s:9:"Economica";s:7:"variant";s:29:":regular,italic,700,700italic";}i:197;a:2:{s:4:"name";s:8:"Ek Mukta";s:7:"variant";s:32:":200,300,regular,500,600,700,800";}i:198;a:2:{s:4:"name";s:11:"Electrolize";s:7:"variant";s:8:":regular";}i:199;a:2:{s:4:"name";s:5:"Elsie";s:7:"variant";s:12:":regular,900";}i:200;a:2:{s:4:"name";s:16:"Elsie Swash Caps";s:7:"variant";s:12:":regular,900";}i:201;a:2:{s:4:"name";s:11:"Emblema One";s:7:"variant";s:8:":regular";}i:202;a:2:{s:4:"name";s:12:"Emilys Candy";s:7:"variant";s:8:":regular";}i:203;a:2:{s:4:"name";s:10:"Engagement";s:7:"variant";s:8:":regular";}i:204;a:2:{s:4:"name";s:9:"Englebert";s:7:"variant";s:8:":regular";}i:205;a:2:{s:4:"name";s:9:"Enriqueta";s:7:"variant";s:12:":regular,700";}i:206;a:2:{s:4:"name";s:9:"Erica One";s:7:"variant";s:8:":regular";}i:207;a:2:{s:4:"name";s:7:"Esteban";s:7:"variant";s:8:":regular";}i:208;a:2:{s:4:"name";s:15:"Euphoria Script";s:7:"variant";s:8:":regular";}i:209;a:2:{s:4:"name";s:5:"Ewert";s:7:"variant";s:8:":regular";}i:210;a:2:{s:4:"name";s:3:"Exo";s:7:"variant";s:127:":100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic";}i:211;a:2:{s:4:"name";s:5:"Exo 2";s:7:"variant";s:127:":100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic";}i:212;a:2:{s:4:"name";s:13:"Expletus Sans";s:7:"variant";s:57:":regular,italic,500,500italic,600,600italic,700,700italic";}i:213;a:2:{s:4:"name";s:12:"Fanwood Text";s:7:"variant";s:15:":regular,italic";}i:214;a:2:{s:4:"name";s:9:"Fascinate";s:7:"variant";s:8:":regular";}i:215;a:2:{s:4:"name";s:16:"Fascinate Inline";s:7:"variant";s:8:":regular";}i:216;a:2:{s:4:"name";s:10:"Faster One";s:7:"variant";s:8:":regular";}i:217;a:2:{s:4:"name";s:8:"Fasthand";s:7:"variant";s:8:":regular";}i:218;a:2:{s:4:"name";s:9:"Fauna One";s:7:"variant";s:8:":regular";}i:219;a:2:{s:4:"name";s:8:"Federant";s:7:"variant";s:8:":regular";}i:220;a:2:{s:4:"name";s:6:"Federo";s:7:"variant";s:8:":regular";}i:221;a:2:{s:4:"name";s:6:"Felipa";s:7:"variant";s:8:":regular";}i:222;a:2:{s:4:"name";s:5:"Fenix";s:7:"variant";s:8:":regular";}i:223;a:2:{s:4:"name";s:12:"Finger Paint";s:7:"variant";s:8:":regular";}i:224;a:2:{s:4:"name";s:9:"Fira Mono";s:7:"variant";s:12:":regular,700";}i:225;a:2:{s:4:"name";s:9:"Fira Sans";s:7:"variant";s:57:":300,300italic,regular,italic,500,500italic,700,700italic";}i:226;a:2:{s:4:"name";s:10:"Fjalla One";s:7:"variant";s:8:":regular";}i:227;a:2:{s:4:"name";s:9:"Fjord One";s:7:"variant";s:8:":regular";}i:228;a:2:{s:4:"name";s:8:"Flamenco";s:7:"variant";s:12:":300,regular";}i:229;a:2:{s:4:"name";s:7:"Flavors";s:7:"variant";s:8:":regular";}i:230;a:2:{s:4:"name";s:10:"Fondamento";s:7:"variant";s:15:":regular,italic";}i:231;a:2:{s:4:"name";s:16:"Fontdiner Swanky";s:7:"variant";s:8:":regular";}i:232;a:2:{s:4:"name";s:5:"Forum";s:7:"variant";s:8:":regular";}i:233;a:2:{s:4:"name";s:12:"Francois One";s:7:"variant";s:8:":regular";}i:234;a:2:{s:4:"name";s:12:"Freckle Face";s:7:"variant";s:8:":regular";}i:235;a:2:{s:4:"name";s:20:"Fredericka the Great";s:7:"variant";s:8:":regular";}i:236;a:2:{s:4:"name";s:11:"Fredoka One";s:7:"variant";s:8:":regular";}i:237;a:2:{s:4:"name";s:8:"Freehand";s:7:"variant";s:8:":regular";}i:238;a:2:{s:4:"name";s:6:"Fresca";s:7:"variant";s:8:":regular";}i:239;a:2:{s:4:"name";s:7:"Frijole";s:7:"variant";s:8:":regular";}i:240;a:2:{s:4:"name";s:7:"Fruktur";s:7:"variant";s:8:":regular";}i:241;a:2:{s:4:"name";s:9:"Fugaz One";s:7:"variant";s:8:":regular";}i:242;a:2:{s:4:"name";s:9:"GFS Didot";s:7:"variant";s:8:":regular";}i:243;a:2:{s:4:"name";s:15:"GFS Neohellenic";s:7:"variant";s:29:":regular,italic,700,700italic";}i:244;a:2:{s:4:"name";s:8:"Gabriela";s:7:"variant";s:8:":regular";}i:245;a:2:{s:4:"name";s:6:"Gafata";s:7:"variant";s:8:":regular";}i:246;a:2:{s:4:"name";s:8:"Galdeano";s:7:"variant";s:8:":regular";}i:247;a:2:{s:4:"name";s:7:"Galindo";s:7:"variant";s:8:":regular";}i:248;a:2:{s:4:"name";s:13:"Gentium Basic";s:7:"variant";s:29:":regular,italic,700,700italic";}i:249;a:2:{s:4:"name";s:18:"Gentium Book Basic";s:7:"variant";s:29:":regular,italic,700,700italic";}i:250;a:2:{s:4:"name";s:3:"Geo";s:7:"variant";s:15:":regular,italic";}i:251;a:2:{s:4:"name";s:7:"Geostar";s:7:"variant";s:8:":regular";}i:252;a:2:{s:4:"name";s:12:"Geostar Fill";s:7:"variant";s:8:":regular";}i:253;a:2:{s:4:"name";s:12:"Germania One";s:7:"variant";s:8:":regular";}i:254;a:2:{s:4:"name";s:6:"Gidugu";s:7:"variant";s:8:":regular";}i:255;a:2:{s:4:"name";s:13:"Gilda Display";s:7:"variant";s:8:":regular";}i:256;a:2:{s:4:"name";s:14:"Give You Glory";s:7:"variant";s:8:":regular";}i:257;a:2:{s:4:"name";s:13:"Glass Antiqua";s:7:"variant";s:8:":regular";}i:258;a:2:{s:4:"name";s:6:"Glegoo";s:7:"variant";s:12:":regular,700";}i:259;a:2:{s:4:"name";s:17:"Gloria Hallelujah";s:7:"variant";s:8:":regular";}i:260;a:2:{s:4:"name";s:10:"Goblin One";s:7:"variant";s:8:":regular";}i:261;a:2:{s:4:"name";s:10:"Gochi Hand";s:7:"variant";s:8:":regular";}i:262;a:2:{s:4:"name";s:8:"Gorditas";s:7:"variant";s:12:":regular,700";}i:263;a:2:{s:4:"name";s:21:"Goudy Bookletter 1911";s:7:"variant";s:8:":regular";}i:264;a:2:{s:4:"name";s:8:"Graduate";s:7:"variant";s:8:":regular";}i:265;a:2:{s:4:"name";s:11:"Grand Hotel";s:7:"variant";s:8:":regular";}i:266;a:2:{s:4:"name";s:12:"Gravitas One";s:7:"variant";s:8:":regular";}i:267;a:2:{s:4:"name";s:11:"Great Vibes";s:7:"variant";s:8:":regular";}i:268;a:2:{s:4:"name";s:6:"Griffy";s:7:"variant";s:8:":regular";}i:269;a:2:{s:4:"name";s:6:"Gruppo";s:7:"variant";s:8:":regular";}i:270;a:2:{s:4:"name";s:5:"Gudea";s:7:"variant";s:19:":regular,italic,700";}i:271;a:2:{s:4:"name";s:8:"Gurajada";s:7:"variant";s:8:":regular";}i:272;a:2:{s:4:"name";s:6:"Habibi";s:7:"variant";s:8:":regular";}i:273;a:2:{s:4:"name";s:6:"Halant";s:7:"variant";s:24:":300,regular,500,600,700";}i:274;a:2:{s:4:"name";s:15:"Hammersmith One";s:7:"variant";s:8:":regular";}i:275;a:2:{s:4:"name";s:7:"Hanalei";s:7:"variant";s:8:":regular";}i:276;a:2:{s:4:"name";s:12:"Hanalei Fill";s:7:"variant";s:8:":regular";}i:277;a:2:{s:4:"name";s:7:"Handlee";s:7:"variant";s:8:":regular";}i:278;a:2:{s:4:"name";s:7:"Hanuman";s:7:"variant";s:12:":regular,700";}i:279;a:2:{s:4:"name";s:12:"Happy Monkey";s:7:"variant";s:8:":regular";}i:280;a:2:{s:4:"name";s:12:"Headland One";s:7:"variant";s:8:":regular";}i:281;a:2:{s:4:"name";s:11:"Henny Penny";s:7:"variant";s:8:":regular";}i:282;a:2:{s:4:"name";s:20:"Herr Von Muellerhoff";s:7:"variant";s:8:":regular";}i:283;a:2:{s:4:"name";s:4:"Hind";s:7:"variant";s:24:":300,regular,500,600,700";}i:284;a:2:{s:4:"name";s:15:"Holtwood One SC";s:7:"variant";s:8:":regular";}i:285;a:2:{s:4:"name";s:14:"Homemade Apple";s:7:"variant";s:8:":regular";}i:286;a:2:{s:4:"name";s:8:"Homenaje";s:7:"variant";s:8:":regular";}i:287;a:2:{s:4:"name";s:15:"IM Fell DW Pica";s:7:"variant";s:15:":regular,italic";}i:288;a:2:{s:4:"name";s:18:"IM Fell DW Pica SC";s:7:"variant";s:8:":regular";}i:289;a:2:{s:4:"name";s:19:"IM Fell Double Pica";s:7:"variant";s:15:":regular,italic";}i:290;a:2:{s:4:"name";s:22:"IM Fell Double Pica SC";s:7:"variant";s:8:":regular";}i:291;a:2:{s:4:"name";s:15:"IM Fell English";s:7:"variant";s:15:":regular,italic";}i:292;a:2:{s:4:"name";s:18:"IM Fell English SC";s:7:"variant";s:8:":regular";}i:293;a:2:{s:4:"name";s:20:"IM Fell French Canon";s:7:"variant";s:15:":regular,italic";}i:294;a:2:{s:4:"name";s:23:"IM Fell French Canon SC";s:7:"variant";s:8:":regular";}i:295;a:2:{s:4:"name";s:20:"IM Fell Great Primer";s:7:"variant";s:15:":regular,italic";}i:296;a:2:{s:4:"name";s:23:"IM Fell Great Primer SC";s:7:"variant";s:8:":regular";}i:297;a:2:{s:4:"name";s:7:"Iceberg";s:7:"variant";s:8:":regular";}i:298;a:2:{s:4:"name";s:7:"Iceland";s:7:"variant";s:8:":regular";}i:299;a:2:{s:4:"name";s:7:"Imprima";s:7:"variant";s:8:":regular";}i:300;a:2:{s:4:"name";s:11:"Inconsolata";s:7:"variant";s:12:":regular,700";}i:301;a:2:{s:4:"name";s:5:"Inder";s:7:"variant";s:8:":regular";}i:302;a:2:{s:4:"name";s:12:"Indie Flower";s:7:"variant";s:8:":regular";}i:303;a:2:{s:4:"name";s:5:"Inika";s:7:"variant";s:12:":regular,700";}i:304;a:2:{s:4:"name";s:12:"Irish Grover";s:7:"variant";s:8:":regular";}i:305;a:2:{s:4:"name";s:9:"Istok Web";s:7:"variant";s:29:":regular,italic,700,700italic";}i:306;a:2:{s:4:"name";s:8:"Italiana";s:7:"variant";s:8:":regular";}i:307;a:2:{s:4:"name";s:9:"Italianno";s:7:"variant";s:8:":regular";}i:308;a:2:{s:4:"name";s:16:"Jacques Francois";s:7:"variant";s:8:":regular";}i:309;a:2:{s:4:"name";s:23:"Jacques Francois Shadow";s:7:"variant";s:8:":regular";}i:310;a:2:{s:4:"name";s:5:"Jaldi";s:7:"variant";s:12:":regular,700";}i:311;a:2:{s:4:"name";s:14:"Jim Nightshade";s:7:"variant";s:8:":regular";}i:312;a:2:{s:4:"name";s:10:"Jockey One";s:7:"variant";s:8:":regular";}i:313;a:2:{s:4:"name";s:12:"Jolly Lodger";s:7:"variant";s:8:":regular";}i:314;a:2:{s:4:"name";s:12:"Josefin Sans";s:7:"variant";s:71:":100,100italic,300,300italic,regular,italic,600,600italic,700,700italic";}i:315;a:2:{s:4:"name";s:12:"Josefin Slab";s:7:"variant";s:71:":100,100italic,300,300italic,regular,italic,600,600italic,700,700italic";}i:316;a:2:{s:4:"name";s:8:"Joti One";s:7:"variant";s:8:":regular";}i:317;a:2:{s:4:"name";s:6:"Judson";s:7:"variant";s:19:":regular,italic,700";}i:318;a:2:{s:4:"name";s:5:"Julee";s:7:"variant";s:8:":regular";}i:319;a:2:{s:4:"name";s:15:"Julius Sans One";s:7:"variant";s:8:":regular";}i:320;a:2:{s:4:"name";s:5:"Junge";s:7:"variant";s:8:":regular";}i:321;a:2:{s:4:"name";s:4:"Jura";s:7:"variant";s:20:":300,regular,500,600";}i:322;a:2:{s:4:"name";s:17:"Just Another Hand";s:7:"variant";s:8:":regular";}i:323;a:2:{s:4:"name";s:23:"Just Me Again Down Here";s:7:"variant";s:8:":regular";}i:324;a:2:{s:4:"name";s:5:"Kalam";s:7:"variant";s:16:":300,regular,700";}i:325;a:2:{s:4:"name";s:7:"Kameron";s:7:"variant";s:12:":regular,700";}i:326;a:2:{s:4:"name";s:9:"Kantumruy";s:7:"variant";s:16:":300,regular,700";}i:327;a:2:{s:4:"name";s:5:"Karla";s:7:"variant";s:29:":regular,italic,700,700italic";}i:328;a:2:{s:4:"name";s:5:"Karma";s:7:"variant";s:24:":300,regular,500,600,700";}i:329;a:2:{s:4:"name";s:14:"Kaushan Script";s:7:"variant";s:8:":regular";}i:330;a:2:{s:4:"name";s:6:"Kavoon";s:7:"variant";s:8:":regular";}i:331;a:2:{s:4:"name";s:10:"Kdam Thmor";s:7:"variant";s:8:":regular";}i:332;a:2:{s:4:"name";s:10:"Keania One";s:7:"variant";s:8:":regular";}i:333;a:2:{s:4:"name";s:10:"Kelly Slab";s:7:"variant";s:8:":regular";}i:334;a:2:{s:4:"name";s:5:"Kenia";s:7:"variant";s:8:":regular";}i:335;a:2:{s:4:"name";s:5:"Khand";s:7:"variant";s:24:":300,regular,500,600,700";}i:336;a:2:{s:4:"name";s:5:"Khmer";s:7:"variant";s:8:":regular";}i:337;a:2:{s:4:"name";s:5:"Khula";s:7:"variant";s:24:":300,regular,600,700,800";}i:338;a:2:{s:4:"name";s:8:"Kite One";s:7:"variant";s:8:":regular";}i:339;a:2:{s:4:"name";s:7:"Knewave";s:7:"variant";s:8:":regular";}i:340;a:2:{s:4:"name";s:9:"Kotta One";s:7:"variant";s:8:":regular";}i:341;a:2:{s:4:"name";s:6:"Koulen";s:7:"variant";s:8:":regular";}i:342;a:2:{s:4:"name";s:6:"Kranky";s:7:"variant";s:8:":regular";}i:343;a:2:{s:4:"name";s:5:"Kreon";s:7:"variant";s:16:":300,regular,700";}i:344;a:2:{s:4:"name";s:6:"Kristi";s:7:"variant";s:8:":regular";}i:345;a:2:{s:4:"name";s:9:"Krona One";s:7:"variant";s:8:":regular";}i:346;a:2:{s:4:"name";s:6:"Kurale";s:7:"variant";s:8:":regular";}i:347;a:2:{s:4:"name";s:15:"La Belle Aurore";s:7:"variant";s:8:":regular";}i:348;a:2:{s:4:"name";s:5:"Laila";s:7:"variant";s:24:":300,regular,500,600,700";}i:349;a:2:{s:4:"name";s:11:"Lakki Reddy";s:7:"variant";s:8:":regular";}i:350;a:2:{s:4:"name";s:8:"Lancelot";s:7:"variant";s:8:":regular";}i:351;a:2:{s:4:"name";s:6:"Lateef";s:7:"variant";s:8:":regular";}i:352;a:2:{s:4:"name";s:4:"Lato";s:7:"variant";s:71:":100,100italic,300,300italic,regular,italic,700,700italic,900,900italic";}i:353;a:2:{s:4:"name";s:13:"League Script";s:7:"variant";s:8:":regular";}i:354;a:2:{s:4:"name";s:12:"Leckerli One";s:7:"variant";s:8:":regular";}i:355;a:2:{s:4:"name";s:6:"Ledger";s:7:"variant";s:8:":regular";}i:356;a:2:{s:4:"name";s:6:"Lekton";s:7:"variant";s:19:":regular,italic,700";}i:357;a:2:{s:4:"name";s:5:"Lemon";s:7:"variant";s:8:":regular";}i:358;a:2:{s:4:"name";s:17:"Libre Baskerville";s:7:"variant";s:19:":regular,italic,700";}i:359;a:2:{s:4:"name";s:11:"Life Savers";s:7:"variant";s:12:":regular,700";}i:360;a:2:{s:4:"name";s:10:"Lilita One";s:7:"variant";s:8:":regular";}i:361;a:2:{s:4:"name";s:15:"Lily Script One";s:7:"variant";s:8:":regular";}i:362;a:2:{s:4:"name";s:9:"Limelight";s:7:"variant";s:8:":regular";}i:363;a:2:{s:4:"name";s:11:"Linden Hill";s:7:"variant";s:15:":regular,italic";}i:364;a:2:{s:4:"name";s:7:"Lobster";s:7:"variant";s:8:":regular";}i:365;a:2:{s:4:"name";s:11:"Lobster Two";s:7:"variant";s:29:":regular,italic,700,700italic";}i:366;a:2:{s:4:"name";s:16:"Londrina Outline";s:7:"variant";s:8:":regular";}i:367;a:2:{s:4:"name";s:15:"Londrina Shadow";s:7:"variant";s:8:":regular";}i:368;a:2:{s:4:"name";s:15:"Londrina Sketch";s:7:"variant";s:8:":regular";}i:369;a:2:{s:4:"name";s:14:"Londrina Solid";s:7:"variant";s:8:":regular";}i:370;a:2:{s:4:"name";s:4:"Lora";s:7:"variant";s:29:":regular,italic,700,700italic";}i:371;a:2:{s:4:"name";s:21:"Love Ya Like A Sister";s:7:"variant";s:8:":regular";}i:372;a:2:{s:4:"name";s:17:"Loved by the King";s:7:"variant";s:8:":regular";}i:373;a:2:{s:4:"name";s:14:"Lovers Quarrel";s:7:"variant";s:8:":regular";}i:374;a:2:{s:4:"name";s:12:"Luckiest Guy";s:7:"variant";s:8:":regular";}i:375;a:2:{s:4:"name";s:8:"Lusitana";s:7:"variant";s:12:":regular,700";}i:376;a:2:{s:4:"name";s:7:"Lustria";s:7:"variant";s:8:":regular";}i:377;a:2:{s:4:"name";s:7:"Macondo";s:7:"variant";s:8:":regular";}i:378;a:2:{s:4:"name";s:18:"Macondo Swash Caps";s:7:"variant";s:8:":regular";}i:379;a:2:{s:4:"name";s:5:"Magra";s:7:"variant";s:12:":regular,700";}i:380;a:2:{s:4:"name";s:13:"Maiden Orange";s:7:"variant";s:8:":regular";}i:381;a:2:{s:4:"name";s:4:"Mako";s:7:"variant";s:8:":regular";}i:382;a:2:{s:4:"name";s:8:"Mallanna";s:7:"variant";s:8:":regular";}i:383;a:2:{s:4:"name";s:7:"Mandali";s:7:"variant";s:8:":regular";}i:384;a:2:{s:4:"name";s:9:"Marcellus";s:7:"variant";s:8:":regular";}i:385;a:2:{s:4:"name";s:12:"Marcellus SC";s:7:"variant";s:8:":regular";}i:386;a:2:{s:4:"name";s:12:"Marck Script";s:7:"variant";s:8:":regular";}i:387;a:2:{s:4:"name";s:9:"Margarine";s:7:"variant";s:8:":regular";}i:388;a:2:{s:4:"name";s:9:"Marko One";s:7:"variant";s:8:":regular";}i:389;a:2:{s:4:"name";s:8:"Marmelad";s:7:"variant";s:8:":regular";}i:390;a:2:{s:4:"name";s:6:"Martel";s:7:"variant";s:32:":200,300,regular,600,700,800,900";}i:391;a:2:{s:4:"name";s:11:"Martel Sans";s:7:"variant";s:32:":200,300,regular,600,700,800,900";}i:392;a:2:{s:4:"name";s:6:"Marvel";s:7:"variant";s:29:":regular,italic,700,700italic";}i:393;a:2:{s:4:"name";s:4:"Mate";s:7:"variant";s:15:":regular,italic";}i:394;a:2:{s:4:"name";s:7:"Mate SC";s:7:"variant";s:8:":regular";}i:395;a:2:{s:4:"name";s:9:"Maven Pro";s:7:"variant";s:20:":regular,500,700,900";}i:396;a:2:{s:4:"name";s:7:"McLaren";s:7:"variant";s:8:":regular";}i:397;a:2:{s:4:"name";s:6:"Meddon";s:7:"variant";s:8:":regular";}i:398;a:2:{s:4:"name";s:13:"MedievalSharp";s:7:"variant";s:8:":regular";}i:399;a:2:{s:4:"name";s:10:"Medula One";s:7:"variant";s:8:":regular";}i:400;a:2:{s:4:"name";s:6:"Megrim";s:7:"variant";s:8:":regular";}i:401;a:2:{s:4:"name";s:11:"Meie Script";s:7:"variant";s:8:":regular";}i:402;a:2:{s:4:"name";s:8:"Merienda";s:7:"variant";s:12:":regular,700";}i:403;a:2:{s:4:"name";s:12:"Merienda One";s:7:"variant";s:8:":regular";}i:404;a:2:{s:4:"name";s:12:"Merriweather";s:7:"variant";s:57:":300,300italic,regular,italic,700,700italic,900,900italic";}i:405;a:2:{s:4:"name";s:17:"Merriweather Sans";s:7:"variant";s:57:":300,300italic,regular,italic,700,700italic,800,800italic";}i:406;a:2:{s:4:"name";s:5:"Metal";s:7:"variant";s:8:":regular";}i:407;a:2:{s:4:"name";s:11:"Metal Mania";s:7:"variant";s:8:":regular";}i:408;a:2:{s:4:"name";s:12:"Metamorphous";s:7:"variant";s:8:":regular";}i:409;a:2:{s:4:"name";s:11:"Metrophobic";s:7:"variant";s:8:":regular";}i:410;a:2:{s:4:"name";s:8:"Michroma";s:7:"variant";s:8:":regular";}i:411;a:2:{s:4:"name";s:7:"Milonga";s:7:"variant";s:8:":regular";}i:412;a:2:{s:4:"name";s:9:"Miltonian";s:7:"variant";s:8:":regular";}i:413;a:2:{s:4:"name";s:16:"Miltonian Tattoo";s:7:"variant";s:8:":regular";}i:414;a:2:{s:4:"name";s:7:"Miniver";s:7:"variant";s:8:":regular";}i:415;a:2:{s:4:"name";s:14:"Miss Fajardose";s:7:"variant";s:8:":regular";}i:416;a:2:{s:4:"name";s:5:"Modak";s:7:"variant";s:8:":regular";}i:417;a:2:{s:4:"name";s:14:"Modern Antiqua";s:7:"variant";s:8:":regular";}i:418;a:2:{s:4:"name";s:7:"Molengo";s:7:"variant";s:8:":regular";}i:419;a:2:{s:4:"name";s:5:"Molle";s:7:"variant";s:7:":italic";}i:420;a:2:{s:4:"name";s:5:"Monda";s:7:"variant";s:12:":regular,700";}i:421;a:2:{s:4:"name";s:8:"Monofett";s:7:"variant";s:8:":regular";}i:422;a:2:{s:4:"name";s:7:"Monoton";s:7:"variant";s:8:":regular";}i:423;a:2:{s:4:"name";s:20:"Monsieur La Doulaise";s:7:"variant";s:8:":regular";}i:424;a:2:{s:4:"name";s:7:"Montaga";s:7:"variant";s:8:":regular";}i:425;a:2:{s:4:"name";s:6:"Montez";s:7:"variant";s:8:":regular";}i:426;a:2:{s:4:"name";s:10:"Montserrat";s:7:"variant";s:12:":regular,700";}i:427;a:2:{s:4:"name";s:21:"Montserrat Alternates";s:7:"variant";s:12:":regular,700";}i:428;a:2:{s:4:"name";s:20:"Montserrat Subrayada";s:7:"variant";s:12:":regular,700";}i:429;a:2:{s:4:"name";s:4:"Moul";s:7:"variant";s:8:":regular";}i:430;a:2:{s:4:"name";s:8:"Moulpali";s:7:"variant";s:8:":regular";}i:431;a:2:{s:4:"name";s:22:"Mountains of Christmas";s:7:"variant";s:12:":regular,700";}i:432;a:2:{s:4:"name";s:13:"Mouse Memoirs";s:7:"variant";s:8:":regular";}i:433;a:2:{s:4:"name";s:10:"Mr Bedfort";s:7:"variant";s:8:":regular";}i:434;a:2:{s:4:"name";s:8:"Mr Dafoe";s:7:"variant";s:8:":regular";}i:435;a:2:{s:4:"name";s:14:"Mr De Haviland";s:7:"variant";s:8:":regular";}i:436;a:2:{s:4:"name";s:19:"Mrs Saint Delafield";s:7:"variant";s:8:":regular";}i:437;a:2:{s:4:"name";s:13:"Mrs Sheppards";s:7:"variant";s:8:":regular";}i:438;a:2:{s:4:"name";s:4:"Muli";s:7:"variant";s:29:":300,300italic,regular,italic";}i:439;a:2:{s:4:"name";s:13:"Mystery Quest";s:7:"variant";s:8:":regular";}i:440;a:2:{s:4:"name";s:3:"NTR";s:7:"variant";s:8:":regular";}i:441;a:2:{s:4:"name";s:6:"Neucha";s:7:"variant";s:8:":regular";}i:442;a:2:{s:4:"name";s:6:"Neuton";s:7:"variant";s:31:":200,300,regular,italic,700,800";}i:443;a:2:{s:4:"name";s:10:"New Rocker";s:7:"variant";s:8:":regular";}i:444;a:2:{s:4:"name";s:10:"News Cycle";s:7:"variant";s:12:":regular,700";}i:445;a:2:{s:4:"name";s:7:"Niconne";s:7:"variant";s:8:":regular";}i:446;a:2:{s:4:"name";s:9:"Nixie One";s:7:"variant";s:8:":regular";}i:447;a:2:{s:4:"name";s:6:"Nobile";s:7:"variant";s:29:":regular,italic,700,700italic";}i:448;a:2:{s:4:"name";s:6:"Nokora";s:7:"variant";s:12:":regular,700";}i:449;a:2:{s:4:"name";s:7:"Norican";s:7:"variant";s:8:":regular";}i:450;a:2:{s:4:"name";s:7:"Nosifer";s:7:"variant";s:8:":regular";}i:451;a:2:{s:4:"name";s:20:"Nothing You Could Do";s:7:"variant";s:8:":regular";}i:452;a:2:{s:4:"name";s:12:"Noticia Text";s:7:"variant";s:29:":regular,italic,700,700italic";}i:453;a:2:{s:4:"name";s:9:"Noto Sans";s:7:"variant";s:29:":regular,italic,700,700italic";}i:454;a:2:{s:4:"name";s:10:"Noto Serif";s:7:"variant";s:29:":regular,italic,700,700italic";}i:455;a:2:{s:4:"name";s:8:"Nova Cut";s:7:"variant";s:8:":regular";}i:456;a:2:{s:4:"name";s:9:"Nova Flat";s:7:"variant";s:8:":regular";}i:457;a:2:{s:4:"name";s:9:"Nova Mono";s:7:"variant";s:8:":regular";}i:458;a:2:{s:4:"name";s:9:"Nova Oval";s:7:"variant";s:8:":regular";}i:459;a:2:{s:4:"name";s:10:"Nova Round";s:7:"variant";s:8:":regular";}i:460;a:2:{s:4:"name";s:11:"Nova Script";s:7:"variant";s:8:":regular";}i:461;a:2:{s:4:"name";s:9:"Nova Slim";s:7:"variant";s:8:":regular";}i:462;a:2:{s:4:"name";s:11:"Nova Square";s:7:"variant";s:8:":regular";}i:463;a:2:{s:4:"name";s:6:"Numans";s:7:"variant";s:8:":regular";}i:464;a:2:{s:4:"name";s:6:"Nunito";s:7:"variant";s:16:":300,regular,700";}i:465;a:2:{s:4:"name";s:14:"Odor Mean Chey";s:7:"variant";s:8:":regular";}i:466;a:2:{s:4:"name";s:7:"Offside";s:7:"variant";s:8:":regular";}i:467;a:2:{s:4:"name";s:15:"Old Standard TT";s:7:"variant";s:19:":regular,italic,700";}i:468;a:2:{s:4:"name";s:9:"Oldenburg";s:7:"variant";s:8:":regular";}i:469;a:2:{s:4:"name";s:11:"Oleo Script";s:7:"variant";s:12:":regular,700";}i:470;a:2:{s:4:"name";s:22:"Oleo Script Swash Caps";s:7:"variant";s:12:":regular,700";}i:471;a:2:{s:4:"name";s:9:"Open Sans";s:7:"variant";s:71:":300,300italic,regular,italic,600,600italic,700,700italic,800,800italic";}i:472;a:2:{s:4:"name";s:19:"Open Sans Condensed";s:7:"variant";s:18:":300,300italic,700";}i:473;a:2:{s:4:"name";s:11:"Oranienbaum";s:7:"variant";s:8:":regular";}i:474;a:2:{s:4:"name";s:8:"Orbitron";s:7:"variant";s:20:":regular,500,700,900";}i:475;a:2:{s:4:"name";s:7:"Oregano";s:7:"variant";s:15:":regular,italic";}i:476;a:2:{s:4:"name";s:7:"Orienta";s:7:"variant";s:8:":regular";}i:477;a:2:{s:4:"name";s:15:"Original Surfer";s:7:"variant";s:8:":regular";}i:478;a:2:{s:4:"name";s:6:"Oswald";s:7:"variant";s:16:":300,regular,700";}i:479;a:2:{s:4:"name";s:16:"Over the Rainbow";s:7:"variant";s:8:":regular";}i:480;a:2:{s:4:"name";s:8:"Overlock";s:7:"variant";s:43:":regular,italic,700,700italic,900,900italic";}i:481;a:2:{s:4:"name";s:11:"Overlock SC";s:7:"variant";s:8:":regular";}i:482;a:2:{s:4:"name";s:3:"Ovo";s:7:"variant";s:8:":regular";}i:483;a:2:{s:4:"name";s:6:"Oxygen";s:7:"variant";s:16:":300,regular,700";}i:484;a:2:{s:4:"name";s:11:"Oxygen Mono";s:7:"variant";s:8:":regular";}i:485;a:2:{s:4:"name";s:7:"PT Mono";s:7:"variant";s:8:":regular";}i:486;a:2:{s:4:"name";s:7:"PT Sans";s:7:"variant";s:29:":regular,italic,700,700italic";}i:487;a:2:{s:4:"name";s:15:"PT Sans Caption";s:7:"variant";s:12:":regular,700";}i:488;a:2:{s:4:"name";s:14:"PT Sans Narrow";s:7:"variant";s:12:":regular,700";}i:489;a:2:{s:4:"name";s:8:"PT Serif";s:7:"variant";s:29:":regular,italic,700,700italic";}i:490;a:2:{s:4:"name";s:16:"PT Serif Caption";s:7:"variant";s:15:":regular,italic";}i:491;a:2:{s:4:"name";s:8:"Pacifico";s:7:"variant";s:8:":regular";}i:492;a:2:{s:4:"name";s:9:"Palanquin";s:7:"variant";s:32:":100,200,300,regular,500,600,700";}i:493;a:2:{s:4:"name";s:14:"Palanquin Dark";s:7:"variant";s:20:":regular,500,600,700";}i:494;a:2:{s:4:"name";s:7:"Paprika";s:7:"variant";s:8:":regular";}i:495;a:2:{s:4:"name";s:10:"Parisienne";s:7:"variant";s:8:":regular";}i:496;a:2:{s:4:"name";s:11:"Passero One";s:7:"variant";s:8:":regular";}i:497;a:2:{s:4:"name";s:11:"Passion One";s:7:"variant";s:16:":regular,700,900";}i:498;a:2:{s:4:"name";s:18:"Pathway Gothic One";s:7:"variant";s:8:":regular";}i:499;a:2:{s:4:"name";s:12:"Patrick Hand";s:7:"variant";s:8:":regular";}i:500;a:2:{s:4:"name";s:15:"Patrick Hand SC";s:7:"variant";s:8:":regular";}i:501;a:2:{s:4:"name";s:9:"Patua One";s:7:"variant";s:8:":regular";}i:502;a:2:{s:4:"name";s:11:"Paytone One";s:7:"variant";s:8:":regular";}i:503;a:2:{s:4:"name";s:7:"Peddana";s:7:"variant";s:8:":regular";}i:504;a:2:{s:4:"name";s:7:"Peralta";s:7:"variant";s:8:":regular";}i:505;a:2:{s:4:"name";s:16:"Permanent Marker";s:7:"variant";s:8:":regular";}i:506;a:2:{s:4:"name";s:19:"Petit Formal Script";s:7:"variant";s:8:":regular";}i:507;a:2:{s:4:"name";s:7:"Petrona";s:7:"variant";s:8:":regular";}i:508;a:2:{s:4:"name";s:11:"Philosopher";s:7:"variant";s:29:":regular,italic,700,700italic";}i:509;a:2:{s:4:"name";s:6:"Piedra";s:7:"variant";s:8:":regular";}i:510;a:2:{s:4:"name";s:13:"Pinyon Script";s:7:"variant";s:8:":regular";}i:511;a:2:{s:4:"name";s:10:"Pirata One";s:7:"variant";s:8:":regular";}i:512;a:2:{s:4:"name";s:7:"Plaster";s:7:"variant";s:8:":regular";}i:513;a:2:{s:4:"name";s:4:"Play";s:7:"variant";s:12:":regular,700";}i:514;a:2:{s:4:"name";s:8:"Playball";s:7:"variant";s:8:":regular";}i:515;a:2:{s:4:"name";s:16:"Playfair Display";s:7:"variant";s:43:":regular,italic,700,700italic,900,900italic";}i:516;a:2:{s:4:"name";s:19:"Playfair Display SC";s:7:"variant";s:43:":regular,italic,700,700italic,900,900italic";}i:517;a:2:{s:4:"name";s:7:"Podkova";s:7:"variant";s:12:":regular,700";}i:518;a:2:{s:4:"name";s:10:"Poiret One";s:7:"variant";s:8:":regular";}i:519;a:2:{s:4:"name";s:10:"Poller One";s:7:"variant";s:8:":regular";}i:520;a:2:{s:4:"name";s:4:"Poly";s:7:"variant";s:15:":regular,italic";}i:521;a:2:{s:4:"name";s:8:"Pompiere";s:7:"variant";s:8:":regular";}i:522;a:2:{s:4:"name";s:12:"Pontano Sans";s:7:"variant";s:8:":regular";}i:523;a:2:{s:4:"name";s:16:"Port Lligat Sans";s:7:"variant";s:8:":regular";}i:524;a:2:{s:4:"name";s:16:"Port Lligat Slab";s:7:"variant";s:8:":regular";}i:525;a:2:{s:4:"name";s:14:"Pragati Narrow";s:7:"variant";s:12:":regular,700";}i:526;a:2:{s:4:"name";s:5:"Prata";s:7:"variant";s:8:":regular";}i:527;a:2:{s:4:"name";s:11:"Preahvihear";s:7:"variant";s:8:":regular";}i:528;a:2:{s:4:"name";s:14:"Press Start 2P";s:7:"variant";s:8:":regular";}i:529;a:2:{s:4:"name";s:14:"Princess Sofia";s:7:"variant";s:8:":regular";}i:530;a:2:{s:4:"name";s:8:"Prociono";s:7:"variant";s:8:":regular";}i:531;a:2:{s:4:"name";s:10:"Prosto One";s:7:"variant";s:8:":regular";}i:532;a:2:{s:4:"name";s:7:"Puritan";s:7:"variant";s:29:":regular,italic,700,700italic";}i:533;a:2:{s:4:"name";s:12:"Purple Purse";s:7:"variant";s:8:":regular";}i:534;a:2:{s:4:"name";s:6:"Quando";s:7:"variant";s:8:":regular";}i:535;a:2:{s:4:"name";s:8:"Quantico";s:7:"variant";s:29:":regular,italic,700,700italic";}i:536;a:2:{s:4:"name";s:12:"Quattrocento";s:7:"variant";s:12:":regular,700";}i:537;a:2:{s:4:"name";s:17:"Quattrocento Sans";s:7:"variant";s:29:":regular,italic,700,700italic";}i:538;a:2:{s:4:"name";s:9:"Questrial";s:7:"variant";s:8:":regular";}i:539;a:2:{s:4:"name";s:9:"Quicksand";s:7:"variant";s:16:":300,regular,700";}i:540;a:2:{s:4:"name";s:14:"Quintessential";s:7:"variant";s:8:":regular";}i:541;a:2:{s:4:"name";s:7:"Qwigley";s:7:"variant";s:8:":regular";}i:542;a:2:{s:4:"name";s:15:"Racing Sans One";s:7:"variant";s:8:":regular";}i:543;a:2:{s:4:"name";s:6:"Radley";s:7:"variant";s:15:":regular,italic";}i:544;a:2:{s:4:"name";s:8:"Rajdhani";s:7:"variant";s:24:":300,regular,500,600,700";}i:545;a:2:{s:4:"name";s:7:"Raleway";s:7:"variant";s:40:":100,200,300,regular,500,600,700,800,900";}i:546;a:2:{s:4:"name";s:12:"Raleway Dots";s:7:"variant";s:8:":regular";}i:547;a:2:{s:4:"name";s:10:"Ramabhadra";s:7:"variant";s:8:":regular";}i:548;a:2:{s:4:"name";s:8:"Ramaraja";s:7:"variant";s:8:":regular";}i:549;a:2:{s:4:"name";s:6:"Rambla";s:7:"variant";s:29:":regular,italic,700,700italic";}i:550;a:2:{s:4:"name";s:12:"Rammetto One";s:7:"variant";s:8:":regular";}i:551;a:2:{s:4:"name";s:8:"Ranchers";s:7:"variant";s:8:":regular";}i:552;a:2:{s:4:"name";s:6:"Rancho";s:7:"variant";s:8:":regular";}i:553;a:2:{s:4:"name";s:5:"Ranga";s:7:"variant";s:12:":regular,700";}i:554;a:2:{s:4:"name";s:9:"Rationale";s:7:"variant";s:8:":regular";}i:555;a:2:{s:4:"name";s:12:"Ravi Prakash";s:7:"variant";s:8:":regular";}i:556;a:2:{s:4:"name";s:9:"Redressed";s:7:"variant";s:8:":regular";}i:557;a:2:{s:4:"name";s:13:"Reenie Beanie";s:7:"variant";s:8:":regular";}i:558;a:2:{s:4:"name";s:7:"Revalia";s:7:"variant";s:8:":regular";}i:559;a:2:{s:4:"name";s:6:"Ribeye";s:7:"variant";s:8:":regular";}i:560;a:2:{s:4:"name";s:13:"Ribeye Marrow";s:7:"variant";s:8:":regular";}i:561;a:2:{s:4:"name";s:9:"Righteous";s:7:"variant";s:8:":regular";}i:562;a:2:{s:4:"name";s:6:"Risque";s:7:"variant";s:8:":regular";}i:563;a:2:{s:4:"name";s:6:"Roboto";s:7:"variant";s:85:":100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic";}i:564;a:2:{s:4:"name";s:16:"Roboto Condensed";s:7:"variant";s:43:":300,300italic,regular,italic,700,700italic";}i:565;a:2:{s:4:"name";s:11:"Roboto Mono";s:7:"variant";s:71:":100,100italic,300,300italic,regular,italic,500,500italic,700,700italic";}i:566;a:2:{s:4:"name";s:11:"Roboto Slab";s:7:"variant";s:20:":100,300,regular,700";}i:567;a:2:{s:4:"name";s:9:"Rochester";s:7:"variant";s:8:":regular";}i:568;a:2:{s:4:"name";s:9:"Rock Salt";s:7:"variant";s:8:":regular";}i:569;a:2:{s:4:"name";s:7:"Rokkitt";s:7:"variant";s:12:":regular,700";}i:570;a:2:{s:4:"name";s:9:"Romanesco";s:7:"variant";s:8:":regular";}i:571;a:2:{s:4:"name";s:9:"Ropa Sans";s:7:"variant";s:15:":regular,italic";}i:572;a:2:{s:4:"name";s:7:"Rosario";s:7:"variant";s:29:":regular,italic,700,700italic";}i:573;a:2:{s:4:"name";s:8:"Rosarivo";s:7:"variant";s:15:":regular,italic";}i:574;a:2:{s:4:"name";s:12:"Rouge Script";s:7:"variant";s:8:":regular";}i:575;a:2:{s:4:"name";s:9:"Rozha One";s:7:"variant";s:8:":regular";}i:576;a:2:{s:4:"name";s:14:"Rubik Mono One";s:7:"variant";s:8:":regular";}i:577;a:2:{s:4:"name";s:9:"Rubik One";s:7:"variant";s:8:":regular";}i:578;a:2:{s:4:"name";s:4:"Ruda";s:7:"variant";s:16:":regular,700,900";}i:579;a:2:{s:4:"name";s:6:"Rufina";s:7:"variant";s:12:":regular,700";}i:580;a:2:{s:4:"name";s:11:"Ruge Boogie";s:7:"variant";s:8:":regular";}i:581;a:2:{s:4:"name";s:6:"Ruluko";s:7:"variant";s:8:":regular";}i:582;a:2:{s:4:"name";s:10:"Rum Raisin";s:7:"variant";s:8:":regular";}i:583;a:2:{s:4:"name";s:14:"Ruslan Display";s:7:"variant";s:8:":regular";}i:584;a:2:{s:4:"name";s:9:"Russo One";s:7:"variant";s:8:":regular";}i:585;a:2:{s:4:"name";s:6:"Ruthie";s:7:"variant";s:8:":regular";}i:586;a:2:{s:4:"name";s:3:"Rye";s:7:"variant";s:8:":regular";}i:587;a:2:{s:4:"name";s:10:"Sacramento";s:7:"variant";s:8:":regular";}i:588;a:2:{s:4:"name";s:4:"Sail";s:7:"variant";s:8:":regular";}i:589;a:2:{s:4:"name";s:5:"Salsa";s:7:"variant";s:8:":regular";}i:590;a:2:{s:4:"name";s:7:"Sanchez";s:7:"variant";s:15:":regular,italic";}i:591;a:2:{s:4:"name";s:8:"Sancreek";s:7:"variant";s:8:":regular";}i:592;a:2:{s:4:"name";s:11:"Sansita One";s:7:"variant";s:8:":regular";}i:593;a:2:{s:4:"name";s:6:"Sarina";s:7:"variant";s:8:":regular";}i:594;a:2:{s:4:"name";s:8:"Sarpanch";s:7:"variant";s:28:":regular,500,600,700,800,900";}i:595;a:2:{s:4:"name";s:7:"Satisfy";s:7:"variant";s:8:":regular";}i:596;a:2:{s:4:"name";s:5:"Scada";s:7:"variant";s:29:":regular,italic,700,700italic";}i:597;a:2:{s:4:"name";s:12:"Scheherazade";s:7:"variant";s:8:":regular";}i:598;a:2:{s:4:"name";s:10:"Schoolbell";s:7:"variant";s:8:":regular";}i:599;a:2:{s:4:"name";s:14:"Seaweed Script";s:7:"variant";s:8:":regular";}i:600;a:2:{s:4:"name";s:9:"Sevillana";s:7:"variant";s:8:":regular";}i:601;a:2:{s:4:"name";s:11:"Seymour One";s:7:"variant";s:8:":regular";}i:602;a:2:{s:4:"name";s:18:"Shadows Into Light";s:7:"variant";s:8:":regular";}i:603;a:2:{s:4:"name";s:22:"Shadows Into Light Two";s:7:"variant";s:8:":regular";}i:604;a:2:{s:4:"name";s:6:"Shanti";s:7:"variant";s:8:":regular";}i:605;a:2:{s:4:"name";s:5:"Share";s:7:"variant";s:29:":regular,italic,700,700italic";}i:606;a:2:{s:4:"name";s:10:"Share Tech";s:7:"variant";s:8:":regular";}i:607;a:2:{s:4:"name";s:15:"Share Tech Mono";s:7:"variant";s:8:":regular";}i:608;a:2:{s:4:"name";s:9:"Shojumaru";s:7:"variant";s:8:":regular";}i:609;a:2:{s:4:"name";s:11:"Short Stack";s:7:"variant";s:8:":regular";}i:610;a:2:{s:4:"name";s:8:"Siemreap";s:7:"variant";s:8:":regular";}i:611;a:2:{s:4:"name";s:10:"Sigmar One";s:7:"variant";s:8:":regular";}i:612;a:2:{s:4:"name";s:7:"Signika";s:7:"variant";s:20:":300,regular,600,700";}i:613;a:2:{s:4:"name";s:16:"Signika Negative";s:7:"variant";s:20:":300,regular,600,700";}i:614;a:2:{s:4:"name";s:9:"Simonetta";s:7:"variant";s:29:":regular,italic,900,900italic";}i:615;a:2:{s:4:"name";s:7:"Sintony";s:7:"variant";s:12:":regular,700";}i:616;a:2:{s:4:"name";s:13:"Sirin Stencil";s:7:"variant";s:8:":regular";}i:617;a:2:{s:4:"name";s:8:"Six Caps";s:7:"variant";s:8:":regular";}i:618;a:2:{s:4:"name";s:7:"Skranji";s:7:"variant";s:12:":regular,700";}i:619;a:2:{s:4:"name";s:10:"Slabo 13px";s:7:"variant";s:8:":regular";}i:620;a:2:{s:4:"name";s:10:"Slabo 27px";s:7:"variant";s:8:":regular";}i:621;a:2:{s:4:"name";s:7:"Slackey";s:7:"variant";s:8:":regular";}i:622;a:2:{s:4:"name";s:6:"Smokum";s:7:"variant";s:8:":regular";}i:623;a:2:{s:4:"name";s:6:"Smythe";s:7:"variant";s:8:":regular";}i:624;a:2:{s:4:"name";s:7:"Sniglet";s:7:"variant";s:12:":regular,800";}i:625;a:2:{s:4:"name";s:7:"Snippet";s:7:"variant";s:8:":regular";}i:626;a:2:{s:4:"name";s:13:"Snowburst One";s:7:"variant";s:8:":regular";}i:627;a:2:{s:4:"name";s:10:"Sofadi One";s:7:"variant";s:8:":regular";}i:628;a:2:{s:4:"name";s:5:"Sofia";s:7:"variant";s:8:":regular";}i:629;a:2:{s:4:"name";s:10:"Sonsie One";s:7:"variant";s:8:":regular";}i:630;a:2:{s:4:"name";s:16:"Sorts Mill Goudy";s:7:"variant";s:15:":regular,italic";}i:631;a:2:{s:4:"name";s:15:"Source Code Pro";s:7:"variant";s:32:":200,300,regular,500,600,700,900";}i:632;a:2:{s:4:"name";s:15:"Source Sans Pro";s:7:"variant";s:85:":200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,900,900italic";}i:633;a:2:{s:4:"name";s:16:"Source Serif Pro";s:7:"variant";s:16:":regular,600,700";}i:634;a:2:{s:4:"name";s:13:"Special Elite";s:7:"variant";s:8:":regular";}i:635;a:2:{s:4:"name";s:10:"Spicy Rice";s:7:"variant";s:8:":regular";}i:636;a:2:{s:4:"name";s:9:"Spinnaker";s:7:"variant";s:8:":regular";}i:637;a:2:{s:4:"name";s:6:"Spirax";s:7:"variant";s:8:":regular";}i:638;a:2:{s:4:"name";s:10:"Squada One";s:7:"variant";s:8:":regular";}i:639;a:2:{s:4:"name";s:20:"Sree Krushnadevaraya";s:7:"variant";s:8:":regular";}i:640;a:2:{s:4:"name";s:9:"Stalemate";s:7:"variant";s:8:":regular";}i:641;a:2:{s:4:"name";s:13:"Stalinist One";s:7:"variant";s:8:":regular";}i:642;a:2:{s:4:"name";s:15:"Stardos Stencil";s:7:"variant";s:12:":regular,700";}i:643;a:2:{s:4:"name";s:21:"Stint Ultra Condensed";s:7:"variant";s:8:":regular";}i:644;a:2:{s:4:"name";s:20:"Stint Ultra Expanded";s:7:"variant";s:8:":regular";}i:645;a:2:{s:4:"name";s:5:"Stoke";s:7:"variant";s:12:":300,regular";}i:646;a:2:{s:4:"name";s:6:"Strait";s:7:"variant";s:8:":regular";}i:647;a:2:{s:4:"name";s:19:"Sue Ellen Francisco";s:7:"variant";s:8:":regular";}i:648;a:2:{s:4:"name";s:6:"Sumana";s:7:"variant";s:12:":regular,700";}i:649;a:2:{s:4:"name";s:9:"Sunshiney";s:7:"variant";s:8:":regular";}i:650;a:2:{s:4:"name";s:16:"Supermercado One";s:7:"variant";s:8:":regular";}i:651;a:2:{s:4:"name";s:7:"Suranna";s:7:"variant";s:8:":regular";}i:652;a:2:{s:4:"name";s:9:"Suravaram";s:7:"variant";s:8:":regular";}i:653;a:2:{s:4:"name";s:11:"Suwannaphum";s:7:"variant";s:8:":regular";}i:654;a:2:{s:4:"name";s:18:"Swanky and Moo Moo";s:7:"variant";s:8:":regular";}i:655;a:2:{s:4:"name";s:9:"Syncopate";s:7:"variant";s:12:":regular,700";}i:656;a:2:{s:4:"name";s:9:"Tangerine";s:7:"variant";s:12:":regular,700";}i:657;a:2:{s:4:"name";s:6:"Taprom";s:7:"variant";s:8:":regular";}i:658;a:2:{s:4:"name";s:5:"Tauri";s:7:"variant";s:8:":regular";}i:659;a:2:{s:4:"name";s:4:"Teko";s:7:"variant";s:24:":300,regular,500,600,700";}i:660;a:2:{s:4:"name";s:5:"Telex";s:7:"variant";s:8:":regular";}i:661;a:2:{s:4:"name";s:18:"Tenali Ramakrishna";s:7:"variant";s:8:":regular";}i:662;a:2:{s:4:"name";s:10:"Tenor Sans";s:7:"variant";s:8:":regular";}i:663;a:2:{s:4:"name";s:11:"Text Me One";s:7:"variant";s:8:":regular";}i:664;a:2:{s:4:"name";s:18:"The Girl Next Door";s:7:"variant";s:8:":regular";}i:665;a:2:{s:4:"name";s:6:"Tienne";s:7:"variant";s:16:":regular,700,900";}i:666;a:2:{s:4:"name";s:7:"Timmana";s:7:"variant";s:8:":regular";}i:667;a:2:{s:4:"name";s:5:"Tinos";s:7:"variant";s:29:":regular,italic,700,700italic";}i:668;a:2:{s:4:"name";s:9:"Titan One";s:7:"variant";s:8:":regular";}i:669;a:2:{s:4:"name";s:13:"Titillium Web";s:7:"variant";s:75:":200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,900";}i:670;a:2:{s:4:"name";s:11:"Trade Winds";s:7:"variant";s:8:":regular";}i:671;a:2:{s:4:"name";s:7:"Trocchi";s:7:"variant";s:8:":regular";}i:672;a:2:{s:4:"name";s:7:"Trochut";s:7:"variant";s:19:":regular,italic,700";}i:673;a:2:{s:4:"name";s:7:"Trykker";s:7:"variant";s:8:":regular";}i:674;a:2:{s:4:"name";s:10:"Tulpen One";s:7:"variant";s:8:":regular";}i:675;a:2:{s:4:"name";s:6:"Ubuntu";s:7:"variant";s:57:":300,300italic,regular,italic,500,500italic,700,700italic";}i:676;a:2:{s:4:"name";s:16:"Ubuntu Condensed";s:7:"variant";s:8:":regular";}i:677;a:2:{s:4:"name";s:11:"Ubuntu Mono";s:7:"variant";s:29:":regular,italic,700,700italic";}i:678;a:2:{s:4:"name";s:5:"Ultra";s:7:"variant";s:8:":regular";}i:679;a:2:{s:4:"name";s:14:"Uncial Antiqua";s:7:"variant";s:8:":regular";}i:680;a:2:{s:4:"name";s:8:"Underdog";s:7:"variant";s:8:":regular";}i:681;a:2:{s:4:"name";s:9:"Unica One";s:7:"variant";s:8:":regular";}i:682;a:2:{s:4:"name";s:14:"UnifrakturCook";s:7:"variant";s:4:":700";}i:683;a:2:{s:4:"name";s:18:"UnifrakturMaguntia";s:7:"variant";s:8:":regular";}i:684;a:2:{s:4:"name";s:7:"Unkempt";s:7:"variant";s:12:":regular,700";}i:685;a:2:{s:4:"name";s:6:"Unlock";s:7:"variant";s:8:":regular";}i:686;a:2:{s:4:"name";s:4:"Unna";s:7:"variant";s:8:":regular";}i:687;a:2:{s:4:"name";s:5:"VT323";s:7:"variant";s:8:":regular";}i:688;a:2:{s:4:"name";s:11:"Vampiro One";s:7:"variant";s:8:":regular";}i:689;a:2:{s:4:"name";s:6:"Varela";s:7:"variant";s:8:":regular";}i:690;a:2:{s:4:"name";s:12:"Varela Round";s:7:"variant";s:8:":regular";}i:691;a:2:{s:4:"name";s:11:"Vast Shadow";s:7:"variant";s:8:":regular";}i:692;a:2:{s:4:"name";s:12:"Vesper Libre";s:7:"variant";s:20:":regular,500,700,900";}i:693;a:2:{s:4:"name";s:5:"Vibur";s:7:"variant";s:8:":regular";}i:694;a:2:{s:4:"name";s:8:"Vidaloka";s:7:"variant";s:8:":regular";}i:695;a:2:{s:4:"name";s:4:"Viga";s:7:"variant";s:8:":regular";}i:696;a:2:{s:4:"name";s:5:"Voces";s:7:"variant";s:8:":regular";}i:697;a:2:{s:4:"name";s:7:"Volkhov";s:7:"variant";s:29:":regular,italic,700,700italic";}i:698;a:2:{s:4:"name";s:8:"Vollkorn";s:7:"variant";s:29:":regular,italic,700,700italic";}i:699;a:2:{s:4:"name";s:8:"Voltaire";s:7:"variant";s:8:":regular";}i:700;a:2:{s:4:"name";s:23:"Waiting for the Sunrise";s:7:"variant";s:8:":regular";}i:701;a:2:{s:4:"name";s:8:"Wallpoet";s:7:"variant";s:8:":regular";}i:702;a:2:{s:4:"name";s:15:"Walter Turncoat";s:7:"variant";s:8:":regular";}i:703;a:2:{s:4:"name";s:6:"Warnes";s:7:"variant";s:8:":regular";}i:704;a:2:{s:4:"name";s:9:"Wellfleet";s:7:"variant";s:8:":regular";}i:705;a:2:{s:4:"name";s:9:"Wendy One";s:7:"variant";s:8:":regular";}i:706;a:2:{s:4:"name";s:8:"Wire One";s:7:"variant";s:8:":regular";}i:707;a:2:{s:4:"name";s:17:"Yanone Kaffeesatz";s:7:"variant";s:20:":200,300,regular,700";}i:708;a:2:{s:4:"name";s:10:"Yellowtail";s:7:"variant";s:8:":regular";}i:709;a:2:{s:4:"name";s:10:"Yeseva One";s:7:"variant";s:8:":regular";}i:710;a:2:{s:4:"name";s:10:"Yesteryear";s:7:"variant";s:8:":regular";}i:711;a:2:{s:4:"name";s:6:"Zeyada";s:7:"variant";s:8:":regular";}}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(936, 'a3rev_woo_dgallery_lite_version', '2.2.2', 'yes'),
(937, 'a3_dynamic_gallery_db_version', '2.2.2', 'yes'),
(938, 'wc_dgallery_lite_clean_on_deletion', 'no', 'yes'),
(939, 'wc_dgallery_activate', 'yes', 'yes'),
(940, 'wc_dgallery_reset_galleries_activate', 'no', 'yes'),
(941, 'wc_dgallery_show_variation', 'no', 'yes'),
(942, 'wc_dgallery_reset_variation_activate', 'no', 'yes'),
(943, 'wc_dgallery_popup_gallery', 'fb', 'yes'),
(944, 'wc_dgallery_width_type', 'px', 'yes'),
(945, 'wc_dgallery_product_gallery_width_responsive', '100', 'yes'),
(946, 'wc_dgallery_product_gallery_width_fixed', '400', 'yes'),
(947, 'wc_dgallery_gallery_height_type', 'fixed', 'yes'),
(948, 'wc_dgallery_product_gallery_height', '300', 'yes'),
(949, 'wc_dgallery_product_gallery_auto_start', 'true', 'yes'),
(950, 'wc_dgallery_product_gallery_effect', 'slide-vert', 'yes'),
(951, 'wc_dgallery_product_gallery_speed', '4', 'yes'),
(952, 'wc_dgallery_product_gallery_animation_speed', '2', 'yes'),
(953, 'wc_dgallery_stop_scroll_1image', 'no', 'yes'),
(954, 'wc_dgallery_main_bg_color', 'a:2:{s:6:"enable";i:1;s:5:"color";s:7:"#FFFFFF";}', 'yes'),
(955, 'wc_dgallery_main_border', 'a:8:{s:5:"width";s:3:"1px";s:5:"style";s:5:"solid";s:5:"color";s:4:"#666";s:6:"corner";s:6:"square";s:15:"top_left_corner";i:3;s:16:"top_right_corner";i:3;s:18:"bottom_left_corner";i:3;s:19:"bottom_right_corner";i:3;}', 'yes'),
(956, 'wc_dgallery_main_shadow', 'a:7:{s:6:"enable";i:0;s:8:"h_shadow";s:3:"0px";s:8:"v_shadow";s:3:"0px";s:4:"blur";s:3:"0px";s:6:"spread";s:3:"0px";s:5:"color";s:7:"#DBDBDB";s:5:"inset";s:0:"";}', 'yes'),
(957, 'wc_dgallery_main_margin_top', '0', 'yes'),
(958, 'wc_dgallery_main_margin_bottom', '0', 'yes'),
(959, 'wc_dgallery_main_margin_left', '0', 'yes'),
(960, 'wc_dgallery_main_margin_right', '0', 'yes'),
(961, 'wc_dgallery_main_padding_top', '0', 'yes'),
(962, 'wc_dgallery_main_padding_bottom', '0', 'yes'),
(963, 'wc_dgallery_main_padding_left', '0', 'yes'),
(964, 'wc_dgallery_main_padding_right', '0', 'yes'),
(965, 'wc_dgallery_product_gallery_nav', 'yes', 'yes'),
(966, 'wc_dgallery_navbar_font', 'a:4:{s:4:"size";s:4:"12px";s:4:"face";s:17:"Arial, sans-serif";s:5:"style";s:6:"normal";s:5:"color";s:7:"#000000";}', 'yes'),
(967, 'wc_dgallery_navbar_bg_color', 'a:2:{s:6:"enable";i:1;s:5:"color";s:7:"#FFFFFF";}', 'yes'),
(968, 'wc_dgallery_navbar_border', 'a:8:{s:5:"width";s:3:"1px";s:5:"style";s:5:"solid";s:5:"color";s:4:"#666";s:6:"corner";s:6:"square";s:15:"top_left_corner";i:3;s:16:"top_right_corner";i:3;s:18:"bottom_left_corner";i:3;s:19:"bottom_right_corner";i:3;}', 'yes'),
(969, 'wc_dgallery_navbar_shadow', 'a:7:{s:6:"enable";i:0;s:8:"h_shadow";s:3:"0px";s:8:"v_shadow";s:3:"0px";s:4:"blur";s:3:"0px";s:6:"spread";s:3:"0px";s:5:"color";s:7:"#DBDBDB";s:5:"inset";s:0:"";}', 'yes'),
(970, 'wc_dgallery_navbar_margin_top', '0', 'yes'),
(971, 'wc_dgallery_navbar_margin_bottom', '0', 'yes'),
(972, 'wc_dgallery_navbar_margin_left', '0', 'yes'),
(973, 'wc_dgallery_navbar_margin_right', '0', 'yes'),
(974, 'wc_dgallery_navbar_padding_top', '5', 'yes'),
(975, 'wc_dgallery_navbar_padding_bottom', '5', 'yes'),
(976, 'wc_dgallery_navbar_padding_left', '5', 'yes'),
(977, 'wc_dgallery_navbar_padding_right', '5', 'yes'),
(978, 'wc_dgallery_navbar_separator', 'a:3:{s:5:"width";s:3:"1px";s:5:"style";s:5:"solid";s:5:"color";s:4:"#666";}', 'yes'),
(979, 'wc_dgallery_caption_font', 'a:4:{s:4:"size";s:4:"12px";s:4:"face";s:17:"Arial, sans-serif";s:5:"style";s:6:"normal";s:5:"color";s:7:"#FFFFFF";}', 'yes'),
(980, 'wc_dgallery_caption_bg_color', 'a:2:{s:6:"enable";i:1;s:5:"color";s:7:"#000000";}', 'yes'),
(981, 'wc_dgallery_caption_bg_transparent', '50', 'yes'),
(982, 'wc_dgallery_lazy_load_scroll', 'yes', 'yes'),
(983, 'wc_dgallery_transition_scroll_bar', '#000000', 'yes'),
(984, 'wc_dgallery_variation_gallery_effect', 'fade', 'yes'),
(985, 'wc_dgallery_variation_gallery_effect_speed', '2', 'yes'),
(986, 'wc_dgallery_enable_gallery_thumb', 'yes', 'yes'),
(987, 'wc_dgallery_hide_thumb_1image', 'yes', 'yes'),
(988, 'wc_dgallery_thumb_show_type', 'slider', 'yes'),
(989, 'wc_dgallery_thumb_spacing', '8', 'yes'),
(990, 'wc_dgallery_thumb_columns', '2', 'yes'),
(991, 'wc_dgallery_thumb_border_color', 'transparent', 'yes'),
(992, 'wc_dgallery_thumb_current_border_color', '#dbdbdb', 'yes'),
(993, 'woo_dynamic_gallery_style_version', '1462854993', 'yes'),
(997, 'woo_dynamic_gallery_google_api_key_enable', '0', 'yes'),
(998, 'woo_dynamic_gallery_google_api_key', '', 'yes'),
(999, 'woo_dynamic_gallery_toggle_box_open', '0', 'yes'),
(1023, '_transient_timeout_wc_cbp_b9fafa2636ab2b548f85c5a807dc73c0', '1465330759', 'no'),
(1024, '_transient_wc_cbp_b9fafa2636ab2b548f85c5a807dc73c0', 'a:1:{i:0;i:31;}', 'no'),
(1031, '_transient_timeout_wc_cbp_578abef1ec93d0aecbcd1d57e4c70774', '1465338246', 'no'),
(1032, '_transient_wc_cbp_578abef1ec93d0aecbcd1d57e4c70774', 'a:0:{}', 'no'),
(1047, '_transient_timeout_settings_errors', '1464847914', 'no'),
(1048, '_transient_settings_errors', 'a:1:{i:0;a:4:{s:7:"setting";s:7:"general";s:4:"code";s:16:"settings_updated";s:7:"message";s:28:"Đã lưu mọi cài đặt.";s:4:"type";s:7:"updated";}}', 'no'),
(1056, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.5.2.zip";s:6:"locale";s:2:"vi";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.5.2.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.5.2-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.5.2-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.5.2";s:7:"version";s:5:"4.5.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.4";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1465529181;s:15:"version_checked";s:5:"4.5.2";s:12:"translations";a:0:{}}', 'yes'),
(1057, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1465529198;s:7:"checked";a:2:{s:6:"mypham";s:3:"1.0";s:16:"myphamduongtrang";s:3:"2.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(1080, '_transient_woocommerce_cache_excluded_uris', 'a:6:{i:0;s:4:"p=19";i:1;s:10:"/gio-hang/";i:2;s:4:"p=20";i:3;s:12:"/thanh-toan/";i:4;s:4:"p=21";i:5;s:11:"/tai-khoan/";}', 'yes'),
(1131, '_transient_timeout_wc_related_23', '1462946697', 'no'),
(1132, '_transient_wc_related_23', 'a:8:{i:0;s:2:"27";i:1;s:2:"28";i:2;s:2:"29";i:3;s:2:"31";i:4;s:3:"156";i:5;s:3:"158";i:6;s:3:"161";i:7;s:3:"168";}', 'no'),
(1172, '_transient_timeout_wc_related_156', '1462950552', 'no'),
(1173, '_transient_wc_related_156', 'a:8:{i:0;s:2:"23";i:1;s:2:"27";i:2;s:2:"28";i:3;s:2:"29";i:4;s:2:"31";i:5;s:3:"158";i:6;s:3:"161";i:7;s:3:"168";}', 'no'),
(1195, '_transient_timeout_wc_uf_pid_fa37e9706ca4ce5ab92f9ead448c84af_s', '1465457041', 'no'),
(1196, '_transient_wc_uf_pid_fa37e9706ca4ce5ab92f9ead448c84af_s', 'a:9:{i:0;i:168;i:1;i:161;i:2;i:158;i:3;i:156;i:4;i:31;i:5;i:29;i:6;i:28;i:7;i:27;i:8;i:23;}', 'no'),
(1251, 'hotline_option2', '0979 286 079', 'yes'),
(1273, '_transient_timeout_wc_uf_pid_f40749e0a356355cfcfefac6dbc3f5f5', '1465859229', 'no'),
(1274, '_transient_wc_uf_pid_f40749e0a356355cfcfefac6dbc3f5f5', 'a:9:{i:0;i:168;i:1;i:161;i:2;i:158;i:3;i:156;i:4;i:31;i:5;i:29;i:6;i:28;i:7;i:27;i:8;i:23;}', 'no'),
(1275, '_transient_timeout_wc_uf_pid_b6e16deb1a59f66f58c5e1e3172886f7', '1465859267', 'no'),
(1276, '_transient_wc_uf_pid_b6e16deb1a59f66f58c5e1e3172886f7', 'a:8:{i:0;i:168;i:1;i:158;i:2;i:156;i:3;i:31;i:4;i:29;i:5;i:28;i:6;i:27;i:7;i:23;}', 'no'),
(1281, 'mail_from', '', 'yes'),
(1282, 'mail_from_name', '', 'yes'),
(1283, 'mailer', 'smtp', 'yes'),
(1284, 'mail_set_return_path', 'false', 'yes'),
(1285, 'smtp_host', 'localhost', 'yes'),
(1286, 'smtp_port', '25', 'yes'),
(1287, 'smtp_ssl', 'none', 'yes'),
(1288, 'smtp_auth', '', 'yes'),
(1289, 'smtp_user', '', 'yes'),
(1290, 'smtp_pass', '', 'yes'),
(1343, '_site_transient_timeout_wporg_theme_feature_list', '1463816035', 'yes'),
(1344, '_site_transient_wporg_theme_feature_list', 'a:4:{s:6:"Colors";a:15:{i:0;s:5:"black";i:1;s:4:"blue";i:2;s:5:"brown";i:3;s:4:"gray";i:4;s:5:"green";i:5;s:6:"orange";i:6;s:4:"pink";i:7;s:6:"purple";i:8;s:3:"red";i:9;s:6:"silver";i:10;s:3:"tan";i:11;s:5:"white";i:12;s:6:"yellow";i:13;s:4:"dark";i:14;s:5:"light";}s:6:"Layout";a:9:{i:0;s:12:"fixed-layout";i:1;s:12:"fluid-layout";i:2;s:17:"responsive-layout";i:3;s:10:"one-column";i:4;s:11:"two-columns";i:5;s:13:"three-columns";i:6;s:12:"four-columns";i:7;s:12:"left-sidebar";i:8;s:13:"right-sidebar";}s:8:"Features";a:20:{i:0;s:19:"accessibility-ready";i:1;s:8:"blavatar";i:2;s:10:"buddypress";i:3;s:17:"custom-background";i:4;s:13:"custom-colors";i:5;s:13:"custom-header";i:6;s:11:"custom-menu";i:7;s:12:"editor-style";i:8;s:21:"featured-image-header";i:9;s:15:"featured-images";i:10;s:15:"flexible-header";i:11;s:20:"front-page-post-form";i:12;s:19:"full-width-template";i:13;s:12:"microformats";i:14;s:12:"post-formats";i:15;s:20:"rtl-language-support";i:16;s:11:"sticky-post";i:17;s:13:"theme-options";i:18;s:17:"threaded-comments";i:19;s:17:"translation-ready";}s:7:"Subject";a:3:{i:0;s:7:"holiday";i:1;s:13:"photoblogging";i:2;s:8:"seasonal";}}', 'yes'),
(1346, 'theme_mods_myphamduongtrang', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}}', 'yes'),
(1385, '_transient_timeout_wc_uf_pid_41420511c78013c40fb901dc88ca6321_s', '1466573522', 'no'),
(1386, '_transient_wc_uf_pid_41420511c78013c40fb901dc88ca6321_s', 'a:0:{}', 'no'),
(1414, '_transient_timeout_external_ip_address_127.0.0.1', '1464760473', 'no'),
(1415, '_transient_external_ip_address_127.0.0.1', '1.52.39.174', 'no'),
(1439, '_transient_timeout_wc_uf_pid_0d30cd58814670aa5ea99ebe23e5c30d', '1466917303', 'no'),
(1440, '_transient_wc_uf_pid_0d30cd58814670aa5ea99ebe23e5c30d', 'a:8:{i:0;i:168;i:1;i:161;i:2;i:156;i:3;i:31;i:4;i:29;i:5;i:28;i:6;i:27;i:7;i:23;}', 'no'),
(1441, '_transient_timeout_wc_related_158', '1464411951', 'no'),
(1442, '_transient_wc_related_158', 'a:8:{i:0;s:2:"23";i:1;s:2:"27";i:2;s:2:"28";i:3;s:2:"29";i:4;s:2:"31";i:5;s:3:"156";i:6;s:3:"161";i:7;s:3:"168";}', 'no'),
(1455, '_transient_timeout_wc_related_27', '1464416328', 'no'),
(1456, '_transient_wc_related_27', 'a:8:{i:0;s:2:"23";i:1;s:2:"28";i:2;s:2:"29";i:3;s:2:"31";i:4;s:3:"156";i:5;s:3:"158";i:6;s:3:"161";i:7;s:3:"168";}', 'no'),
(1476, '_site_transient_timeout_browser_deff56fee9cf7386a4a6a51073ca5621', '1465104627', 'yes'),
(1477, '_site_transient_browser_deff56fee9cf7386a4a6a51073ca5621', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"50.0.2661.102";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(1480, 'category_children', 'a:1:{i:13;a:2:{i:0;i:11;i:1;i:12;}}', 'yes'),
(1505, '_transient_timeout_wc_related_31', '1464929339', 'no'),
(1506, '_transient_wc_related_31', 'a:8:{i:0;s:2:"23";i:1;s:2:"27";i:2;s:2:"28";i:3;s:2:"29";i:4;s:3:"156";i:5;s:3:"158";i:6;s:3:"161";i:7;s:3:"168";}', 'no'),
(1507, '_transient_timeout_wc_related_29', '1464929352', 'no'),
(1508, '_transient_wc_related_29', 'a:8:{i:0;s:2:"23";i:1;s:2:"27";i:2;s:2:"28";i:3;s:2:"31";i:4;s:3:"156";i:5;s:3:"158";i:6;s:3:"161";i:7;s:3:"168";}', 'no'),
(1514, '_transient_timeout_wc_uf_pid_f1efc35d241b72a37924f1bd966409a2', '1467438046', 'no'),
(1515, '_transient_wc_uf_pid_f1efc35d241b72a37924f1bd966409a2', 'a:9:{i:0;i:168;i:1;i:161;i:2;i:158;i:3;i:156;i:4;i:31;i:5;i:29;i:6;i:28;i:7;i:27;i:8;i:23;}', 'no'),
(1516, '_transient_timeout_wc_related_28', '1464934123', 'no'),
(1517, '_transient_wc_related_28', 'a:8:{i:0;s:2:"23";i:1;s:2:"27";i:2;s:2:"29";i:3;s:2:"31";i:4;s:3:"156";i:5;s:3:"158";i:6;s:3:"161";i:7;s:3:"168";}', 'no'),
(1521, '_transient_timeout_wc_low_stock_count', '1467439812', 'no'),
(1522, '_transient_wc_low_stock_count', '0', 'no'),
(1523, '_transient_timeout_wc_outofstock_count', '1467439812', 'no'),
(1524, '_transient_wc_outofstock_count', '1', 'no'),
(1527, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1464858629', 'yes'),
(1528, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:100:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"5899";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3655";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3598";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"3121";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2789";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"2370";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"2216";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"2097";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"2046";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"2025";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1988";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1934";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1868";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:4:"1705";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1689";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1583";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1536";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1393";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1316";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1289";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:4:"1220";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:4:"1109";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:4:"1083";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:4:"1008";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"986";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:3:"974";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"922";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"915";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"911";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"906";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"905";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"840";}s:10:"responsive";a:3:{s:4:"name";s:10:"responsive";s:4:"slug";s:10:"responsive";s:5:"count";s:3:"830";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"794";}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";s:3:"776";}s:10:"e-commerce";a:3:{s:4:"name";s:10:"e-commerce";s:4:"slug";s:10:"e-commerce";s:5:"count";s:3:"767";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"761";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"758";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"752";}s:5:"share";a:3:{s:4:"name";s:5:"Share";s:4:"slug";s:5:"share";s:5:"count";s:3:"750";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"747";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"743";}s:8:"category";a:3:{s:4:"name";s:8:"category";s:4:"slug";s:8:"category";s:5:"count";s:3:"707";}s:9:"analytics";a:3:{s:4:"name";s:9:"analytics";s:4:"slug";s:9:"analytics";s:5:"count";s:3:"699";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"692";}s:3:"css";a:3:{s:4:"name";s:3:"CSS";s:4:"slug";s:3:"css";s:5:"count";s:3:"691";}s:5:"embed";a:3:{s:4:"name";s:5:"embed";s:4:"slug";s:5:"embed";s:5:"count";s:3:"688";}s:4:"form";a:3:{s:4:"name";s:4:"form";s:4:"slug";s:4:"form";s:5:"count";s:3:"687";}s:6:"search";a:3:{s:4:"name";s:6:"search";s:4:"slug";s:6:"search";s:5:"count";s:3:"667";}s:6:"slider";a:3:{s:4:"name";s:6:"slider";s:4:"slug";s:6:"slider";s:5:"count";s:3:"661";}s:6:"custom";a:3:{s:4:"name";s:6:"custom";s:4:"slug";s:6:"custom";s:5:"count";s:3:"653";}s:9:"slideshow";a:3:{s:4:"name";s:9:"slideshow";s:4:"slug";s:9:"slideshow";s:5:"count";s:3:"645";}s:5:"stats";a:3:{s:4:"name";s:5:"stats";s:4:"slug";s:5:"stats";s:5:"count";s:3:"614";}s:6:"button";a:3:{s:4:"name";s:6:"button";s:4:"slug";s:6:"button";s:5:"count";s:3:"612";}s:4:"menu";a:3:{s:4:"name";s:4:"menu";s:4:"slug";s:4:"menu";s:5:"count";s:3:"602";}s:5:"theme";a:3:{s:4:"name";s:5:"theme";s:4:"slug";s:5:"theme";s:5:"count";s:3:"599";}s:7:"comment";a:3:{s:4:"name";s:7:"comment";s:4:"slug";s:7:"comment";s:5:"count";s:3:"598";}s:9:"dashboard";a:3:{s:4:"name";s:9:"dashboard";s:4:"slug";s:9:"dashboard";s:5:"count";s:3:"596";}s:4:"tags";a:3:{s:4:"name";s:4:"tags";s:4:"slug";s:4:"tags";s:5:"count";s:3:"586";}s:10:"categories";a:3:{s:4:"name";s:10:"categories";s:4:"slug";s:10:"categories";s:5:"count";s:3:"579";}s:6:"mobile";a:3:{s:4:"name";s:6:"mobile";s:4:"slug";s:6:"mobile";s:5:"count";s:3:"573";}s:10:"statistics";a:3:{s:4:"name";s:10:"statistics";s:4:"slug";s:10:"statistics";s:5:"count";s:3:"568";}s:3:"ads";a:3:{s:4:"name";s:3:"ads";s:4:"slug";s:3:"ads";s:5:"count";s:3:"562";}s:6:"editor";a:3:{s:4:"name";s:6:"editor";s:4:"slug";s:6:"editor";s:5:"count";s:3:"553";}s:4:"user";a:3:{s:4:"name";s:4:"user";s:4:"slug";s:4:"user";s:5:"count";s:3:"553";}s:4:"list";a:3:{s:4:"name";s:4:"list";s:4:"slug";s:4:"list";s:5:"count";s:3:"543";}s:5:"users";a:3:{s:4:"name";s:5:"users";s:4:"slug";s:5:"users";s:5:"count";s:3:"534";}s:7:"plugins";a:3:{s:4:"name";s:7:"plugins";s:4:"slug";s:7:"plugins";s:5:"count";s:3:"523";}s:12:"social-media";a:3:{s:4:"name";s:12:"social media";s:4:"slug";s:12:"social-media";s:5:"count";s:3:"521";}s:9:"affiliate";a:3:{s:4:"name";s:9:"affiliate";s:4:"slug";s:9:"affiliate";s:5:"count";s:3:"516";}s:7:"picture";a:3:{s:4:"name";s:7:"picture";s:4:"slug";s:7:"picture";s:5:"count";s:3:"514";}s:12:"contact-form";a:3:{s:4:"name";s:12:"contact form";s:4:"slug";s:12:"contact-form";s:5:"count";s:3:"512";}s:6:"simple";a:3:{s:4:"name";s:6:"simple";s:4:"slug";s:6:"simple";s:5:"count";s:3:"509";}s:9:"multisite";a:3:{s:4:"name";s:9:"multisite";s:4:"slug";s:9:"multisite";s:5:"count";s:3:"506";}s:7:"contact";a:3:{s:4:"name";s:7:"contact";s:4:"slug";s:7:"contact";s:5:"count";s:3:"482";}s:8:"pictures";a:3:{s:4:"name";s:8:"pictures";s:4:"slug";s:8:"pictures";s:5:"count";s:3:"462";}s:9:"marketing";a:3:{s:4:"name";s:9:"marketing";s:4:"slug";s:9:"marketing";s:5:"count";s:3:"461";}s:4:"shop";a:3:{s:4:"name";s:4:"shop";s:4:"slug";s:4:"shop";s:5:"count";s:3:"457";}s:3:"api";a:3:{s:4:"name";s:3:"api";s:4:"slug";s:3:"api";s:5:"count";s:3:"456";}s:3:"url";a:3:{s:4:"name";s:3:"url";s:4:"slug";s:3:"url";s:5:"count";s:3:"452";}s:4:"html";a:3:{s:4:"name";s:4:"html";s:4:"slug";s:4:"html";s:5:"count";s:3:"443";}s:10:"navigation";a:3:{s:4:"name";s:10:"navigation";s:4:"slug";s:10:"navigation";s:5:"count";s:3:"443";}s:10:"newsletter";a:3:{s:4:"name";s:10:"newsletter";s:4:"slug";s:10:"newsletter";s:5:"count";s:3:"428";}s:6:"events";a:3:{s:4:"name";s:6:"events";s:4:"slug";s:6:"events";s:5:"count";s:3:"423";}s:4:"meta";a:3:{s:4:"name";s:4:"meta";s:4:"slug";s:4:"meta";s:5:"count";s:3:"423";}s:8:"calendar";a:3:{s:4:"name";s:8:"calendar";s:4:"slug";s:8:"calendar";s:5:"count";s:3:"421";}s:5:"flash";a:3:{s:4:"name";s:5:"flash";s:4:"slug";s:5:"flash";s:5:"count";s:3:"421";}s:8:"tracking";a:3:{s:4:"name";s:8:"tracking";s:4:"slug";s:8:"tracking";s:5:"count";s:3:"419";}s:10:"shortcodes";a:3:{s:4:"name";s:10:"shortcodes";s:4:"slug";s:10:"shortcodes";s:5:"count";s:3:"414";}s:4:"news";a:3:{s:4:"name";s:4:"News";s:4:"slug";s:4:"news";s:5:"count";s:3:"412";}s:3:"tag";a:3:{s:4:"name";s:3:"tag";s:4:"slug";s:3:"tag";s:5:"count";s:3:"406";}s:11:"advertising";a:3:{s:4:"name";s:11:"advertising";s:4:"slug";s:11:"advertising";s:5:"count";s:3:"404";}s:6:"upload";a:3:{s:4:"name";s:6:"upload";s:4:"slug";s:6:"upload";s:5:"count";s:3:"404";}s:9:"thumbnail";a:3:{s:4:"name";s:9:"thumbnail";s:4:"slug";s:9:"thumbnail";s:5:"count";s:3:"402";}s:7:"sharing";a:3:{s:4:"name";s:7:"sharing";s:4:"slug";s:7:"sharing";s:5:"count";s:3:"400";}s:6:"paypal";a:3:{s:4:"name";s:6:"paypal";s:4:"slug";s:6:"paypal";s:5:"count";s:3:"400";}s:12:"notification";a:3:{s:4:"name";s:12:"notification";s:4:"slug";s:12:"notification";s:5:"count";s:3:"398";}s:8:"linkedin";a:3:{s:4:"name";s:8:"linkedin";s:4:"slug";s:8:"linkedin";s:5:"count";s:3:"393";}s:8:"lightbox";a:3:{s:4:"name";s:8:"lightbox";s:4:"slug";s:8:"lightbox";s:5:"count";s:3:"393";}s:7:"profile";a:3:{s:4:"name";s:7:"profile";s:4:"slug";s:7:"profile";s:5:"count";s:3:"392";}}', 'yes'),
(1547, '_transient_timeout_wc_related_168', '1465172968', 'no'),
(1548, '_transient_wc_related_168', 'a:8:{i:0;s:2:"23";i:1;s:2:"27";i:2;s:2:"28";i:3;s:2:"29";i:4;s:2:"31";i:5;s:3:"156";i:6;s:3:"158";i:7;s:3:"161";}', 'no'),
(1549, '_transient_timeout_external_ip_address_::1', '1465704468', 'no'),
(1550, '_transient_external_ip_address_::1', '183.81.9.89', 'no'),
(1574, '_transient_timeout_wc_related_161', '1465446730', 'no'),
(1575, '_transient_wc_related_161', 'a:8:{i:0;s:2:"23";i:1;s:2:"27";i:2;s:2:"28";i:3;s:2:"29";i:4;s:2:"31";i:5;s:3:"156";i:6;s:3:"158";i:7;s:3:"168";}', 'no'),
(1576, '_transient_timeout_woo_dynamic_gallery_google_api_key_status', '1465615575', 'no'),
(1577, '_transient_woo_dynamic_gallery_google_api_key_status', 'invalid', 'no'),
(1579, '_transient_timeout_wc_term_counts', '1468121179', 'no'),
(1580, '_transient_wc_term_counts', 'a:8:{i:19;s:0:"";i:9;s:1:"8";i:8;s:1:"5";i:6;s:1:"9";i:18;s:0:"";i:17;s:0:"";i:7;s:1:"8";i:10;s:1:"5";}', 'no'),
(1582, '_site_transient_timeout_theme_roots', '1465530985', 'yes'),
(1583, '_site_transient_theme_roots', 'a:2:{s:6:"mypham";s:7:"/themes";s:16:"myphamduongtrang";s:7:"/themes";}', 'yes'),
(1584, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1465529194;s:8:"response";a:1:{s:62:"woocommerce-dynamic-gallery/wc_dynamic_gallery_woocommerce.php";O:8:"stdClass":9:{s:2:"id";s:5:"33076";s:4:"slug";s:27:"woocommerce-dynamic-gallery";s:6:"plugin";s:62:"woocommerce-dynamic-gallery/wc_dynamic_gallery_woocommerce.php";s:11:"new_version";s:5:"2.2.5";s:3:"url";s:58:"https://wordpress.org/plugins/woocommerce-dynamic-gallery/";s:7:"package";s:70:"https://downloads.wordpress.org/plugin/woocommerce-dynamic-gallery.zip";s:14:"upgrade_notice";s:127:"Maintenance Update. 4 Tweaks to convert plugin to use the WooCommerce Single Product Image settings including Hard Crop Option.";s:6:"tested";s:5:"4.5.2";s:13:"compatibility";O:8:"stdClass":1:{s:6:"scalar";O:8:"stdClass":1:{s:6:"scalar";b:0;}}}}s:12:"translations";a:0:{}s:9:"no_update";a:8:{s:30:"advanced-custom-fields/acf.php";O:8:"stdClass":6:{s:2:"id";s:5:"21367";s:4:"slug";s:22:"advanced-custom-fields";s:6:"plugin";s:30:"advanced-custom-fields/acf.php";s:11:"new_version";s:5:"4.4.7";s:3:"url";s:53:"https://wordpress.org/plugins/advanced-custom-fields/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.7.zip";}s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":6:{s:2:"id";s:3:"790";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"4.4.2";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.4.4.2.zip";}s:45:"meteor-slides - Copy/meteor-slides-plugin.php";O:8:"stdClass":7:{s:2:"id";s:5:"15291";s:4:"slug";s:13:"meteor-slides";s:6:"plugin";s:45:"meteor-slides - Copy/meteor-slides-plugin.php";s:11:"new_version";s:5:"1.5.6";s:3:"url";s:44:"https://wordpress.org/plugins/meteor-slides/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/meteor-slides.1.5.6.zip";s:14:"upgrade_notice";s:108:"Meteor Slides 1.5.6 includes updated support and functionality for the latest version of the Members plugin.";}s:38:"meteor-slides/meteor-slides-plugin.php";O:8:"stdClass":7:{s:2:"id";s:5:"15291";s:4:"slug";s:13:"meteor-slides";s:6:"plugin";s:38:"meteor-slides/meteor-slides-plugin.php";s:11:"new_version";s:5:"1.5.6";s:3:"url";s:44:"https://wordpress.org/plugins/meteor-slides/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/meteor-slides.1.5.6.zip";s:14:"upgrade_notice";s:108:"Meteor Slides 1.5.6 includes updated support and functionality for the latest version of the Members plugin.";}s:54:"responsive-contact-form/ai-responsive-contact-form.php";O:8:"stdClass":6:{s:2:"id";s:5:"42370";s:4:"slug";s:23:"responsive-contact-form";s:6:"plugin";s:54:"responsive-contact-form/ai-responsive-contact-form.php";s:11:"new_version";s:5:"2.7.2";s:3:"url";s:54:"https://wordpress.org/plugins/responsive-contact-form/";s:7:"package";s:66:"https://downloads.wordpress.org/plugin/responsive-contact-form.zip";}s:37:"tinymce-advanced/tinymce-advanced.php";O:8:"stdClass":6:{s:2:"id";s:3:"731";s:4:"slug";s:16:"tinymce-advanced";s:6:"plugin";s:37:"tinymce-advanced/tinymce-advanced.php";s:11:"new_version";s:8:"4.3.10.1";s:3:"url";s:47:"https://wordpress.org/plugins/tinymce-advanced/";s:7:"package";s:68:"https://downloads.wordpress.org/plugin/tinymce-advanced.4.3.10.1.zip";}s:27:"woocommerce/woocommerce.php";O:8:"stdClass":6:{s:2:"id";s:5:"25331";s:4:"slug";s:11:"woocommerce";s:6:"plugin";s:27:"woocommerce/woocommerce.php";s:11:"new_version";s:5:"2.5.5";s:3:"url";s:42:"https://wordpress.org/plugins/woocommerce/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/woocommerce.2.5.5.zip";}s:29:"wp-mail-smtp/wp_mail_smtp.php";O:8:"stdClass":7:{s:2:"id";s:3:"951";s:4:"slug";s:12:"wp-mail-smtp";s:6:"plugin";s:29:"wp-mail-smtp/wp_mail_smtp.php";s:11:"new_version";s:5:"0.9.5";s:3:"url";s:43:"https://wordpress.org/plugins/wp-mail-smtp/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/wp-mail-smtp.0.9.6.zip";s:14:"upgrade_notice";s:38:"Minor security fix, hat tip JD Grimes.";}}}', 'yes'),
(1585, '_site_transient_timeout_browser_564205166f5dd7218c74f327f1c4d138', '1466135983', 'yes'),
(1586, '_site_transient_browser_564205166f5dd7218c74f327f1c4d138', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:7:"Firefox";s:7:"version";s:4:"46.0";s:10:"update_url";s:23:"http://www.firefox.com/";s:7:"img_src";s:50:"http://s.wordpress.org/images/browsers/firefox.png";s:11:"img_src_ssl";s:49:"https://wordpress.org/images/browsers/firefox.png";s:15:"current_version";s:2:"16";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(1587, '_transient_timeout_wc_admin_report', '1465617584', 'no'),
(1588, '_transient_wc_admin_report', 'a:2:{s:32:"032f9bde654dfc02d6114a461d70645f";a:1:{i:0;O:8:"stdClass":2:{s:15:"sparkline_value";s:3:"300";s:9:"post_date";s:19:"2016-06-02 12:31:56";}}s:32:"fb4677a7fe31a93b310dd91589cd8827";a:1:{i:0;O:8:"stdClass":3:{s:10:"product_id";s:2:"29";s:15:"sparkline_value";s:1:"1";s:9:"post_date";s:19:"2016-06-02 12:31:56";}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1461045955:1'),
(4, 6, '_edit_last', '1'),
(5, 6, '_edit_lock', '1460698186:1'),
(6, 8, '_edit_last', '1'),
(7, 8, '_edit_lock', '1460698218:1'),
(8, 10, '_edit_last', '1'),
(9, 10, '_edit_lock', '1460698275:1'),
(10, 12, '_edit_last', '1'),
(11, 12, '_edit_lock', '1460698299:1'),
(12, 14, '_edit_last', '1'),
(13, 14, '_edit_lock', '1460698355:1'),
(14, 16, '_edit_last', '1'),
(15, 16, '_edit_lock', '1461017891:1'),
(16, 23, '_edit_last', '1'),
(17, 23, '_edit_lock', '1462864593:1'),
(18, 24, '_wp_attached_file', '2016/04/img08.jpg'),
(19, 24, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:153;s:6:"height";i:150;s:4:"file";s:17:"2016/04/img08.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img08-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(20, 23, '_thumbnail_id', '24'),
(21, 25, '_wp_attached_file', '2016/04/img06.png'),
(22, 25, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:153;s:6:"height";i:150;s:4:"file";s:17:"2016/04/img06.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img06-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(23, 26, '_wp_attached_file', '2016/04/img07.png'),
(24, 26, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:153;s:6:"height";i:150;s:4:"file";s:17:"2016/04/img07.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img07-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(25, 23, '_visibility', 'visible'),
(26, 23, '_stock_status', 'instock'),
(27, 23, 'total_sales', '0'),
(28, 23, '_downloadable', 'no'),
(29, 23, '_virtual', 'no'),
(30, 23, '_purchase_note', ''),
(31, 23, '_featured', 'no'),
(32, 23, '_weight', ''),
(33, 23, '_length', ''),
(34, 23, '_width', ''),
(35, 23, '_height', ''),
(36, 23, '_sku', ''),
(37, 23, '_product_attributes', 'a:0:{}'),
(38, 23, '_regular_price', '160.000'),
(39, 23, '_sale_price', ''),
(40, 23, '_sale_price_dates_from', ''),
(41, 23, '_sale_price_dates_to', ''),
(42, 23, '_price', '160.000'),
(43, 23, '_sold_individually', ''),
(44, 23, '_manage_stock', 'no'),
(45, 23, '_backorders', 'no'),
(46, 23, '_stock', ''),
(47, 23, '_upsell_ids', 'a:0:{}'),
(48, 23, '_crosssell_ids', 'a:0:{}'),
(49, 23, '_product_version', '2.5.5'),
(50, 23, '_product_image_gallery', '25,26'),
(51, 27, '_edit_last', '1'),
(52, 27, '_edit_lock', '1462864590:1'),
(54, 27, '_visibility', 'visible'),
(55, 27, '_stock_status', 'instock'),
(56, 27, '_downloadable', 'no'),
(57, 27, '_virtual', 'no'),
(58, 27, '_purchase_note', ''),
(59, 27, '_featured', 'no'),
(60, 27, '_weight', ''),
(61, 27, '_length', ''),
(62, 27, '_width', ''),
(63, 27, '_height', ''),
(64, 27, '_sku', ''),
(65, 27, '_product_attributes', 'a:0:{}'),
(66, 27, '_regular_price', '190.000'),
(67, 27, '_sale_price', '100.000'),
(68, 27, '_sale_price_dates_from', ''),
(69, 27, '_sale_price_dates_to', ''),
(70, 27, '_price', '100.000'),
(71, 27, '_sold_individually', ''),
(72, 27, '_manage_stock', 'no'),
(73, 27, '_backorders', 'no'),
(74, 27, '_stock', ''),
(75, 27, '_upsell_ids', 'a:0:{}'),
(76, 27, '_crosssell_ids', 'a:0:{}'),
(77, 27, '_product_version', '2.5.5'),
(78, 27, '_product_image_gallery', '198,146'),
(82, 27, '_thumbnail_id', '25'),
(83, 27, 'total_sales', '0'),
(84, 28, '_edit_last', '1'),
(85, 28, '_edit_lock', '1462864592:1'),
(87, 28, '_visibility', 'visible'),
(88, 28, '_stock_status', 'instock'),
(89, 28, '_downloadable', 'no'),
(90, 28, '_virtual', 'no'),
(91, 28, '_purchase_note', ''),
(92, 28, '_featured', 'no'),
(93, 28, '_weight', ''),
(94, 28, '_length', ''),
(95, 28, '_width', ''),
(96, 28, '_height', ''),
(97, 28, '_sku', ''),
(98, 28, '_product_attributes', 'a:0:{}'),
(99, 28, '_regular_price', '160.000'),
(100, 28, '_sale_price', ''),
(101, 28, '_sale_price_dates_from', ''),
(102, 28, '_sale_price_dates_to', ''),
(103, 28, '_price', '160.000'),
(104, 28, '_sold_individually', ''),
(105, 28, '_manage_stock', 'no'),
(106, 28, '_backorders', 'no'),
(107, 28, '_stock', ''),
(108, 28, '_upsell_ids', 'a:0:{}'),
(109, 28, '_crosssell_ids', 'a:0:{}'),
(110, 28, '_product_version', '2.5.5'),
(111, 28, '_product_image_gallery', '198,192'),
(115, 28, '_thumbnail_id', '26'),
(116, 28, 'total_sales', '0'),
(117, 29, '_edit_last', '1'),
(118, 29, '_edit_lock', '1462864208:1'),
(119, 29, '_visibility', 'visible'),
(120, 29, '_stock_status', 'instock'),
(121, 29, '_downloadable', 'no'),
(122, 29, '_virtual', 'no'),
(123, 29, '_purchase_note', ''),
(124, 29, '_featured', 'no'),
(125, 29, '_weight', ''),
(126, 29, '_length', ''),
(127, 29, '_width', ''),
(128, 29, '_height', ''),
(129, 29, '_sku', ''),
(130, 29, '_product_attributes', 'a:0:{}'),
(131, 29, '_regular_price', '320.000'),
(132, 29, '_sale_price', '300.000'),
(133, 29, '_sale_price_dates_from', ''),
(134, 29, '_sale_price_dates_to', ''),
(135, 29, '_price', '300.000'),
(136, 29, '_sold_individually', ''),
(137, 29, '_manage_stock', 'yes'),
(138, 29, '_backorders', 'no'),
(139, 29, '_stock', '19'),
(140, 29, '_upsell_ids', 'a:0:{}'),
(141, 29, '_crosssell_ids', 'a:0:{}'),
(142, 29, '_product_version', '2.5.5'),
(143, 29, '_product_image_gallery', '198,194'),
(151, 29, 'total_sales', '1'),
(152, 31, '_edit_last', '1'),
(153, 31, '_edit_lock', '1462864072:1'),
(154, 31, '_visibility', 'visible'),
(155, 31, '_stock_status', 'outofstock'),
(156, 31, '_downloadable', 'no'),
(157, 31, '_virtual', 'no'),
(158, 31, '_purchase_note', ''),
(159, 31, '_featured', 'no'),
(160, 31, '_weight', ''),
(161, 31, '_length', ''),
(162, 31, '_width', ''),
(163, 31, '_height', ''),
(164, 31, '_sku', ''),
(165, 31, '_product_attributes', 'a:0:{}'),
(166, 31, '_regular_price', '320.000'),
(167, 31, '_sale_price', '300.000'),
(168, 31, '_sale_price_dates_from', ''),
(169, 31, '_sale_price_dates_to', ''),
(170, 31, '_price', '300.000'),
(171, 31, '_sold_individually', ''),
(172, 31, '_manage_stock', 'yes'),
(173, 31, '_backorders', 'no'),
(174, 31, '_stock', '0'),
(175, 31, '_upsell_ids', 'a:0:{}'),
(176, 31, '_crosssell_ids', 'a:0:{}'),
(177, 31, '_product_version', '2.5.5'),
(178, 31, '_product_image_gallery', '190,192'),
(183, 31, '_thumbnail_id', '25'),
(184, 31, 'total_sales', '2'),
(187, 28, '_wc_rating_count', 'a:0:{}'),
(188, 28, '_wc_average_rating', '0'),
(189, 31, '_wc_rating_count', 'a:0:{}'),
(190, 31, '_wc_average_rating', '0'),
(191, 23, '_wc_rating_count', 'a:0:{}'),
(192, 23, '_wc_average_rating', '0'),
(195, 28, '_wc_review_count', '0'),
(196, 31, '_wc_review_count', '0'),
(198, 23, '_wc_review_count', '0'),
(203, 29, '_wc_rating_count', 'a:1:{i:4;s:1:"1";}'),
(204, 29, '_wc_review_count', '2'),
(205, 29, '_wc_average_rating', '4.00'),
(206, 25, '_wp_attachment_image_alt', 'KEM DƯỠNG TRẮNG DA TRỊ MỤN CAO CẤP KBONE'),
(208, 26, '_wp_attachment_image_alt', 'Nước tẩy trang zoley toner 160ml'),
(209, 24, '_wp_attachment_image_alt', 'Kem dưỡng chất chống lão hóa 10gr'),
(210, 1, '_edit_lock', '1461536754:1'),
(215, 1, '_edit_last', '1'),
(216, 36, '_edit_last', '1'),
(217, 36, '_edit_lock', '1461536547:1'),
(223, 41, '_edit_last', '1'),
(224, 41, '_edit_lock', '1461536928:1'),
(230, 40, '_edit_last', '1'),
(231, 40, '_edit_lock', '1461536863:1'),
(234, 44, '_edit_last', '1'),
(235, 44, '_edit_lock', '1461536795:1'),
(248, 48, '_edit_last', '1'),
(249, 48, '_edit_lock', '1461535540:1'),
(253, 52, '_menu_item_type', 'post_type'),
(254, 52, '_menu_item_menu_item_parent', '0'),
(255, 52, '_menu_item_object_id', '14'),
(256, 52, '_menu_item_object', 'page'),
(257, 52, '_menu_item_target', ''),
(258, 52, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(259, 52, '_menu_item_xfn', ''),
(260, 52, '_menu_item_url', ''),
(262, 53, '_menu_item_type', 'post_type'),
(263, 53, '_menu_item_menu_item_parent', '0'),
(264, 53, '_menu_item_object_id', '12'),
(265, 53, '_menu_item_object', 'page'),
(266, 53, '_menu_item_target', ''),
(267, 53, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(268, 53, '_menu_item_xfn', ''),
(269, 53, '_menu_item_url', ''),
(271, 54, '_menu_item_type', 'post_type'),
(272, 54, '_menu_item_menu_item_parent', '0'),
(273, 54, '_menu_item_object_id', '10'),
(274, 54, '_menu_item_object', 'page'),
(275, 54, '_menu_item_target', ''),
(276, 54, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(277, 54, '_menu_item_xfn', ''),
(278, 54, '_menu_item_url', ''),
(280, 55, '_menu_item_type', 'post_type'),
(281, 55, '_menu_item_menu_item_parent', '0'),
(282, 55, '_menu_item_object_id', '8'),
(283, 55, '_menu_item_object', 'page'),
(284, 55, '_menu_item_target', ''),
(285, 55, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(286, 55, '_menu_item_xfn', ''),
(287, 55, '_menu_item_url', ''),
(289, 56, '_menu_item_type', 'post_type'),
(290, 56, '_menu_item_menu_item_parent', '0'),
(291, 56, '_menu_item_object_id', '6'),
(292, 56, '_menu_item_object', 'page'),
(293, 56, '_menu_item_target', ''),
(294, 56, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(295, 56, '_menu_item_xfn', ''),
(296, 56, '_menu_item_url', ''),
(298, 57, '_form', '<p>Họ và Tên:*<br />\n    [text* your-name] </p>\n\n<p>Email:*<br />\n    [email* your-email] </p>\n\n<p>Số điện thoại:*<br />\n    [tel* tel-774]\n </p>\n\n<p>Thông điệp:*<br />\n    [textarea* your-message] </p>\n\n<p>[submit "Gửi đi"]</p>'),
(299, 57, '_mail', 'a:8:{s:7:"subject";s:24:"My pham "[your-subject]"";s:6:"sender";s:36:"[your-name] <hongdiepbach@gmail.com>";s:4:"body";s:176:"Gửi đến từ: [your-name] <[your-email]>\nTiêu đề: [your-subject]\n\nNội dung thông điệp:\n[your-message]\n\n--\nEmail này gửi từ My pham (http://localhost/mypham)";s:9:"recipient";s:22:"hongdiepbach@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(300, 57, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:24:"My pham "[your-subject]"";s:6:"sender";s:32:"My pham <hongdiepbach@gmail.com>";s:4:"body";s:123:"Nội dung thông điệp:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on My pham (http://localhost/mypham)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:32:"Reply-To: hongdiepbach@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(301, 57, '_messages', 'a:23:{s:12:"mail_sent_ok";s:42:"Cám ơn bạn đã gửi thông điệp !";s:12:"mail_sent_ng";s:80:"Gửi email thất bại. Vui lòng kiểm tra thông tin nhập và gửi lại";s:16:"validation_error";s:61:"Một vài trường chưa hợp lệ, vui lòng nhập lại";s:4:"spam";s:80:"Gửi email thất bại. Vui lòng kiểm tra thông tin nhập và gửi lại";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:16:"Vui lòng nhập";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";s:12:"invalid_date";s:29:"The date format is incorrect.";s:14:"date_too_early";s:44:"The date is before the earliest one allowed.";s:13:"date_too_late";s:41:"The date is after the latest one allowed.";s:13:"upload_failed";s:46:"There was an unknown error uploading the file.";s:24:"upload_file_type_invalid";s:49:"You are not allowed to upload files of this type.";s:21:"upload_file_too_large";s:20:"The file is too big.";s:23:"upload_failed_php_error";s:38:"There was an error uploading the file.";s:14:"invalid_number";s:21:"Số chưa hợp lệ";s:16:"number_too_small";s:47:"The number is smaller than the minimum allowed.";s:16:"number_too_large";s:46:"The number is larger than the maximum allowed.";s:23:"quiz_answer_not_correct";s:36:"The answer to the quiz is incorrect.";s:17:"captcha_not_match";s:34:"Bạn đã nhập sai mã CAPTCHA.";s:13:"invalid_email";s:22:"Email chưa hợp lệ";s:11:"invalid_url";s:19:"The URL is invalid.";s:11:"invalid_tel";s:37:"Số điện thoại chưa hợp lệ";}'),
(302, 57, '_additional_settings', ''),
(303, 57, '_locale', 'vi'),
(319, 63, '_wp_attached_file', '2016/04/baner-1600x400.jpg'),
(320, 63, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:400;s:4:"file";s:26:"2016/04/baner-1600x400.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"baner-1600x400-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"baner-1600x400-300x75.jpg";s:5:"width";i:300;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:26:"baner-1600x400-768x192.jpg";s:5:"width";i:768;s:6:"height";i:192;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:27:"baner-1600x400-1024x256.jpg";s:5:"width";i:1024;s:6:"height";i:256;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:25:"baner-1600x400-250x63.jpg";s:5:"width";i:250;s:6:"height";i:63;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"baner-1600x400-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"baner-1600x400-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:26:"baner-1600x400-600x400.jpg";s:5:"width";i:600;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(321, 63, '_wp_attachment_image_alt', 'Quang cao kem trang da'),
(323, 62, '_edit_last', '1'),
(324, 62, '_edit_lock', '1462865397:1'),
(328, 67, '_edit_last', '1'),
(329, 67, '_edit_lock', '1464504095:1'),
(334, 72, '_edit_last', '1'),
(335, 72, '_edit_lock', '1464500986:1'),
(338, 74, '_edit_last', '1'),
(341, 74, '_edit_lock', '1463031554:1'),
(344, 78, '_wp_attached_file', '2016/04/thuong.jpg'),
(345, 78, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:625;s:6:"height";i:480;s:4:"file";s:18:"2016/04/thuong.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"thuong-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"thuong-300x230.jpg";s:5:"width";i:300;s:6:"height";i:230;s:9:"mime-type";s:10:"image/jpeg";}s:14:"featured-slide";a:4:{s:4:"file";s:18:"thuong-625x400.jpg";s:5:"width";i:625;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:18:"thuong-250x192.jpg";s:5:"width";i:250;s:6:"height";i:192;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"thuong-180x138.jpg";s:5:"width";i:180;s:6:"height";i:138;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:18:"thuong-300x230.jpg";s:5:"width";i:300;s:6:"height";i:230;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:18:"thuong-600x461.jpg";s:5:"width";i:600;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.6";s:6:"credit";s:0:"";s:6:"camera";s:16:"FE4000,X920,X925";s:7:"caption";s:22:"OLYMPUS DIGITAL CAMERA";s:17:"created_timestamp";s:10:"1374169454";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.65";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:4:"0.04";s:5:"title";s:22:"OLYMPUS DIGITAL CAMERA";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(346, 78, '_wp_attachment_image_alt', 'Ngô Thị Xuân Thủy'),
(347, 79, '_wp_attached_file', '2016/04/image.jpg'),
(348, 79, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:432;s:4:"file";s:17:"2016/04/image.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"image-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"image-278x300.jpg";s:5:"width";i:278;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"featured-slide";a:4:{s:4:"file";s:17:"image-400x400.jpg";s:5:"width";i:400;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:17:"image-250x270.jpg";s:5:"width";i:250;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"image-167x180.jpg";s:5:"width";i:167;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:17:"image-278x300.jpg";s:5:"width";i:278;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(349, 79, '_wp_attachment_image_alt', 'Ngô Thị Xuân Thủy'),
(360, 85, '_wp_attached_file', '2016/04/ming01.jpg'),
(361, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:120;s:4:"file";s:18:"2016/04/ming01.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(362, 85, '_wp_attachment_image_alt', 'Kem dưỡng trắng da se khít lỗ chân lông 35gr'),
(363, 40, '_thumbnail_id', '85'),
(366, 88, '_wp_attached_file', '2016/04/mig02.jpg'),
(367, 88, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:120;s:4:"file";s:17:"2016/04/mig02.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(368, 88, '_wp_attachment_image_alt', 'Nước hoa nữ Jolie Dion Emotion 50ml'),
(369, 48, '_thumbnail_id', '88'),
(378, 92, '_wp_attached_file', '2016/04/img03.jpg'),
(379, 92, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:160;s:6:"height";i:160;s:4:"file";s:17:"2016/04/img03.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img03-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(380, 92, '_wp_attachment_image_alt', 'Kem dưỡng trắng da se khít lỗ chân lông Kem dưỡng trắng da se khít lỗ chân lông'),
(381, 44, '_thumbnail_id', '92'),
(384, 94, '_wp_attached_file', '2016/04/img05.jpg'),
(385, 94, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:160;s:6:"height";i:160;s:4:"file";s:17:"2016/04/img05.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img05-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(386, 94, '_wp_attachment_image_alt', 'Kem dưỡng trắng toàn thân zoley 150gr'),
(387, 36, '_thumbnail_id', '94'),
(390, 97, '_wp_attached_file', '2016/04/img06.jpg'),
(391, 97, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:160;s:6:"height";i:160;s:4:"file";s:17:"2016/04/img06.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img06-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(392, 97, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban…'),
(393, 41, '_thumbnail_id', '97'),
(396, 99, '_wp_attached_file', '2016/04/img04.jpg'),
(397, 99, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:120;s:4:"file";s:17:"2016/04/img04.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(398, 99, '_wp_attachment_image_alt', 'Kem dưỡng trắng toàn thân zoley 150gr'),
(399, 1, '_thumbnail_id', '99'),
(402, 101, '_edit_last', '1'),
(403, 101, '_edit_lock', '1461536865:1'),
(404, 101, '_thumbnail_id', '94'),
(407, 104, '_wp_attached_file', '2016/04/img07.jpg'),
(408, 104, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:160;s:6:"height";i:160;s:4:"file";s:17:"2016/04/img07.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img07-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(409, 103, '_edit_last', '1'),
(410, 103, '_edit_lock', '1461538668:1'),
(411, 104, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr'),
(412, 103, '_thumbnail_id', '104'),
(445, 116, '_edit_last', '1'),
(446, 116, '_edit_lock', '1461546020:1'),
(454, 121, '_wp_attached_file', '2016/04/img12-1.jpg'),
(455, 121, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:19:"2016/04/img12-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img12-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img12-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img12-1-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img12-1-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img12-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(457, 121, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr'),
(458, 122, '_wp_attached_file', '2016/04/img14.jpg'),
(459, 122, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:17:"2016/04/img14.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img14-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"img14-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:17:"img14-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"img14-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:17:"img14-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(460, 122, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr'),
(462, 123, '_wp_attached_file', '2016/04/img15.jpg'),
(463, 123, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:17:"2016/04/img15.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img15-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"img15-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:17:"img15-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"img15-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:17:"img15-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(464, 123, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr'),
(474, 125, '_edit_last', '1'),
(475, 125, '_edit_lock', '1461546003:1'),
(476, 125, '_thumbnail_id', '121'),
(479, 116, '_thumbnail_id', '122'),
(482, 127, '_edit_last', '1'),
(483, 127, '_edit_lock', '1461545988:1'),
(484, 128, '_wp_attached_file', '2016/04/img13.jpg'),
(485, 128, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:17:"2016/04/img13.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img13-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"img13-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:17:"img13-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"img13-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:17:"img13-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(486, 128, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr'),
(487, 127, '_thumbnail_id', '128'),
(490, 130, '_edit_last', '1'),
(491, 130, '_edit_lock', '1461545802:1'),
(492, 131, '_wp_attached_file', '2016/04/img15-1.jpg'),
(493, 131, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:19:"2016/04/img15-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img15-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img15-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img15-1-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img15-1-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img15-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(494, 131, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr'),
(502, 133, '_edit_last', '1'),
(505, 133, '_edit_lock', '1461546231:1'),
(510, 136, '_edit_last', '1'),
(513, 136, '_edit_lock', '1461545852:1'),
(518, 139, '_edit_last', '1'),
(521, 139, '_edit_lock', '1461495206:1'),
(522, 142, '_thumbnail_id', '122'),
(523, 142, '_edit_last', '1'),
(526, 142, '_edit_lock', '1461495135:1'),
(527, 144, '_wp_attached_file', '2016/04/img01.jpg'),
(528, 144, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:17:"2016/04/img01.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img01-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"img01-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:17:"img01-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"img01-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:17:"img01-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(529, 144, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr totop'),
(530, 139, '_thumbnail_id', '144'),
(533, 145, '_wp_attached_file', '2016/04/img02.jpg'),
(534, 145, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:17:"2016/04/img02.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"img02-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"img02-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:17:"img02-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"img02-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:17:"img02-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(535, 145, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr totop'),
(536, 130, '_thumbnail_id', '145'),
(539, 146, '_wp_attached_file', '2016/04/img04-1.jpg'),
(540, 146, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:19:"2016/04/img04-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img04-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img04-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img04-1-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img04-1-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img04-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(541, 146, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr totop'),
(542, 136, '_thumbnail_id', '146'),
(545, 147, '_edit_last', '1'),
(546, 147, '_edit_lock', '1461545929:1'),
(547, 148, '_wp_attached_file', '2016/04/img03-1.jpg'),
(548, 148, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:19:"2016/04/img03-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img03-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img03-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img03-1-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img03-1-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img03-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:13:"lev dolgachov";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(549, 148, '_wp_attachment_image_alt', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr'),
(550, 147, '_thumbnail_id', '148'),
(553, 150, '_edit_last', '1'),
(554, 150, '_edit_lock', '1461642683:1'),
(555, 151, '_wp_attached_file', '2016/04/img05-1.jpg'),
(556, 151, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:200;s:4:"file";s:19:"2016/04/img05-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img05-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img05-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img05-1-250x167.jpg";s:5:"width";i:250;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img05-1-180x120.jpg";s:5:"width";i:180;s:6:"height";i:120;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img05-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(557, 150, '_thumbnail_id', '151'),
(562, 133, '_thumbnail_id', '145'),
(565, 156, '_edit_last', '1'),
(566, 156, '_edit_lock', '1462864020:1'),
(567, 157, '_wp_attached_file', '2016/04/img01-1.jpg'),
(568, 157, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:220;s:6:"height";i:220;s:4:"file";s:19:"2016/04/img01-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img01-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img01-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(569, 157, '_wp_attachment_image_alt', 'kem dưỡng trắng da trị mụn cao cấp 50gr'),
(570, 156, '_visibility', 'visible'),
(571, 156, '_stock_status', 'instock'),
(572, 156, 'total_sales', '0'),
(573, 156, '_downloadable', 'no'),
(574, 156, '_virtual', 'no'),
(575, 156, '_purchase_note', ''),
(576, 156, '_featured', 'no'),
(577, 156, '_weight', ''),
(578, 156, '_length', ''),
(579, 156, '_width', ''),
(580, 156, '_height', ''),
(581, 156, '_sku', ''),
(582, 156, '_product_attributes', 'a:0:{}'),
(583, 156, '_regular_price', ''),
(584, 156, '_sale_price', ''),
(585, 156, '_sale_price_dates_from', ''),
(586, 156, '_sale_price_dates_to', ''),
(587, 156, '_price', ''),
(588, 156, '_sold_individually', ''),
(589, 156, '_manage_stock', 'no'),
(590, 156, '_backorders', 'no'),
(591, 156, '_stock', ''),
(592, 156, '_upsell_ids', 'a:0:{}'),
(593, 156, '_crosssell_ids', 'a:0:{}'),
(594, 156, '_product_version', '2.5.5'),
(595, 156, '_product_image_gallery', '194,192'),
(596, 158, '_edit_last', '1'),
(597, 158, '_edit_lock', '1462863957:1'),
(601, 158, '_visibility', 'visible'),
(602, 158, '_stock_status', 'instock'),
(603, 158, 'total_sales', '0'),
(604, 158, '_downloadable', 'no'),
(605, 158, '_virtual', 'no'),
(606, 158, '_purchase_note', ''),
(607, 158, '_featured', 'no'),
(608, 158, '_weight', ''),
(609, 158, '_length', ''),
(610, 158, '_width', ''),
(611, 158, '_height', ''),
(612, 158, '_sku', 'MY0222'),
(613, 158, '_product_attributes', 'a:0:{}'),
(614, 158, '_regular_price', '5000'),
(615, 158, '_sale_price', ''),
(616, 158, '_sale_price_dates_from', ''),
(617, 158, '_sale_price_dates_to', ''),
(618, 158, '_price', '5000'),
(619, 158, '_sold_individually', ''),
(620, 158, '_manage_stock', 'no'),
(621, 158, '_backorders', 'no'),
(622, 158, '_stock', ''),
(623, 158, '_upsell_ids', 'a:0:{}'),
(624, 158, '_crosssell_ids', 'a:0:{}'),
(625, 158, '_product_version', '2.5.5'),
(626, 158, '_product_image_gallery', '194'),
(629, 156, '_wc_rating_count', 'a:0:{}'),
(630, 156, '_wc_average_rating', '0'),
(632, 160, '_wp_attached_file', '2016/04/k8imsubu.jpg'),
(633, 160, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:200;s:6:"height";i:200;s:4:"file";s:20:"2016/04/k8imsubu.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"k8imsubu-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"k8imsubu-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(634, 160, '_wp_attachment_image_alt', 'kem dưỡng trắng da trị mụn cao cấp 50gr'),
(635, 158, '_thumbnail_id', '160'),
(636, 161, '_edit_last', '1'),
(637, 161, '_edit_lock', '1462863946:1'),
(640, 163, '_wp_attached_file', '2016/04/img06-1.jpg'),
(641, 163, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:200;s:6:"height";i:200;s:4:"file";s:19:"2016/04/img06-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img06-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img06-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(642, 163, '_wp_attachment_image_alt', 'Kem dưỡng trắng da trị mụn cao cấp 50gr'),
(644, 161, '_visibility', 'visible'),
(645, 161, '_stock_status', 'instock'),
(646, 161, 'total_sales', '0'),
(647, 161, '_downloadable', 'no'),
(648, 161, '_virtual', 'no'),
(649, 161, '_purchase_note', ''),
(650, 161, '_featured', 'no'),
(651, 161, '_weight', ''),
(652, 161, '_length', ''),
(653, 161, '_width', ''),
(654, 161, '_height', ''),
(655, 161, '_sku', ''),
(656, 161, '_product_attributes', 'a:0:{}'),
(657, 161, '_regular_price', '600'),
(658, 161, '_sale_price', ''),
(659, 161, '_sale_price_dates_from', ''),
(660, 161, '_sale_price_dates_to', ''),
(661, 161, '_price', '600'),
(662, 161, '_sold_individually', ''),
(663, 161, '_manage_stock', 'no'),
(664, 161, '_backorders', 'no'),
(665, 161, '_stock', ''),
(666, 161, '_upsell_ids', 'a:0:{}'),
(667, 161, '_crosssell_ids', 'a:0:{}'),
(668, 161, '_product_version', '2.5.5'),
(669, 161, '_product_image_gallery', '194,192,191'),
(670, 161, '_wc_rating_count', 'a:0:{}'),
(671, 161, '_wc_average_rating', '0'),
(672, 156, '_thumbnail_id', '157'),
(673, 165, '_edit_last', '1'),
(674, 165, 'field_571dac72b6ceb', 'a:14:{s:3:"key";s:19:"field_571dac72b6ceb";s:5:"label";s:36:"Nhập id cho bài giới thiệu sp";s:4:"name";s:7:"nhap_id";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"html";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(677, 165, 'position', 'normal'),
(678, 165, 'layout', 'no_box'),
(679, 165, 'hide_on_screen', ''),
(680, 165, '_edit_lock', '1461648704:1'),
(688, 167, '_wp_attached_file', '2016/04/img07-1.jpg'),
(689, 167, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:200;s:6:"height";i:200;s:4:"file";s:19:"2016/04/img07-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img07-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img07-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(690, 167, '_wp_attachment_image_alt', 'Kem dưỡng trắng da trị mụn cao cấp 50gr'),
(691, 161, 'img_2', '167'),
(692, 161, '_img_2', 'field_571dad49a1eaa'),
(695, 168, '_edit_last', '1'),
(696, 168, '_visibility', 'visible'),
(697, 168, '_stock_status', 'instock'),
(698, 168, 'total_sales', '0'),
(699, 168, '_downloadable', 'no'),
(700, 168, '_virtual', 'no'),
(701, 168, '_purchase_note', ''),
(702, 168, '_featured', 'no'),
(703, 168, '_weight', ''),
(704, 168, '_length', ''),
(705, 168, '_width', ''),
(706, 168, '_height', ''),
(707, 168, '_sku', 'KB019191'),
(708, 168, '_product_attributes', 'a:0:{}'),
(709, 168, '_regular_price', '500'),
(710, 168, '_sale_price', ''),
(711, 168, '_sale_price_dates_from', ''),
(712, 168, '_sale_price_dates_to', ''),
(713, 168, '_price', '500'),
(714, 168, '_sold_individually', ''),
(715, 168, '_manage_stock', 'no'),
(716, 168, '_backorders', 'no'),
(717, 168, '_stock', ''),
(718, 168, '_upsell_ids', 'a:0:{}'),
(719, 168, '_crosssell_ids', 'a:0:{}'),
(720, 168, '_product_version', '2.5.5'),
(721, 168, '_product_image_gallery', '186,190,191,192'),
(722, 168, 'img_2', ''),
(723, 168, '_img_2', 'field_571dad49a1eaa'),
(724, 168, '_edit_lock', '1462857842:1'),
(728, 161, '_wc_review_count', '0'),
(734, 165, 'rule', 'a:5:{s:5:"param";s:13:"post_category";s:8:"operator";s:2:"==";s:5:"value";s:2:"22";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(735, 170, '_edit_last', '1'),
(736, 170, '_edit_lock', '1461647922:1'),
(737, 171, '_wp_attached_file', '2016/04/imgzoley.jpg'),
(738, 171, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:950;s:6:"height";i:230;s:4:"file";s:20:"2016/04/imgzoley.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"imgzoley-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"imgzoley-300x73.jpg";s:5:"width";i:300;s:6:"height";i:73;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"imgzoley-768x186.jpg";s:5:"width";i:768;s:6:"height";i:186;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"imgzoley-250x61.jpg";s:5:"width";i:250;s:6:"height";i:61;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"imgzoley-180x44.jpg";s:5:"width";i:180;s:6:"height";i:44;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"imgzoley-300x73.jpg";s:5:"width";i:300;s:6:"height";i:73;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:20:"imgzoley-600x145.jpg";s:5:"width";i:600;s:6:"height";i:145;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(739, 171, '_wp_attachment_image_alt', 'Zoley'),
(740, 170, '_thumbnail_id', '171'),
(743, 172, 'nhap_id', '9'),
(744, 172, '_nhap_id', 'field_571dac72b6ceb'),
(745, 170, 'nhap_id', '9'),
(746, 170, '_nhap_id', 'field_571dac72b6ceb'),
(747, 175, '_order_key', 'wc_order_5722f7f70dce1'),
(748, 175, '_order_currency', 'VND'),
(749, 175, '_prices_include_tax', 'no'),
(750, 175, '_customer_ip_address', '::1'),
(751, 175, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.3; rv:46.0) Gecko/20100101 Firefox/46.0'),
(752, 175, '_customer_user', '0'),
(753, 175, '_created_via', 'checkout'),
(754, 175, '_order_version', '2.5.5'),
(755, 175, '_billing_first_name', 'hong diep'),
(756, 175, '_billing_last_name', 'Bach ngoc'),
(757, 175, '_billing_company', 'Freesalse'),
(758, 175, '_billing_email', 'hongdiepbach@gmail.com'),
(759, 175, '_billing_phone', '01224047894'),
(760, 175, '_billing_country', 'VN'),
(761, 175, '_billing_address_1', '165b, Nguyen duy duong'),
(762, 175, '_billing_address_2', ''),
(763, 175, '_billing_city', 'HCM'),
(764, 175, '_billing_state', ''),
(765, 175, '_shipping_first_name', 'hong diep'),
(766, 175, '_shipping_last_name', 'Bach ngoc'),
(767, 175, '_shipping_company', 'Freesalse'),
(768, 175, '_shipping_country', 'VN'),
(769, 175, '_shipping_address_1', '165b, Nguyen duy duong'),
(770, 175, '_shipping_address_2', ''),
(771, 175, '_shipping_city', 'HCM'),
(772, 175, '_shipping_state', ''),
(773, 175, '_shipping_postcode', ''),
(774, 175, '_payment_method', 'cod'),
(775, 175, '_payment_method_title', 'Trả tiền mặt khi nhận hàng'),
(776, 175, '_order_shipping', ''),
(777, 175, '_cart_discount', '0'),
(778, 175, '_cart_discount_tax', '0'),
(779, 175, '_order_tax', '0'),
(780, 175, '_order_shipping_tax', '0'),
(781, 175, '_order_total', '600.000'),
(782, 175, '_download_permissions_granted', '1'),
(783, 175, '_recorded_sales', 'yes'),
(784, 175, '_order_stock_reduced', '1'),
(799, 180, '_edit_last', '1'),
(800, 180, '_edit_lock', '1462864869:1'),
(801, 156, '_wc_review_count', '0'),
(802, 182, '_wp_attached_file', '2016/04/hinh01.jpg'),
(803, 182, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:300;s:4:"file";s:18:"2016/04/hinh01.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"hinh01-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"hinh01-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:18:"hinh01-250x188.jpg";s:5:"width";i:250;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"hinh01-180x135.jpg";s:5:"width";i:180;s:6:"height";i:135;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:18:"hinh01-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(804, 182, '_wp_attachment_image_alt', 'anh 02'),
(805, 168, '_actived_d_gallery', '1'),
(806, 168, '_wc_rating_count', 'a:1:{i:3;s:1:"1";}'),
(807, 168, '_wc_review_count', '1'),
(808, 168, '_wc_average_rating', '3.00'),
(809, 184, '_edit_last', '1'),
(810, 184, '_edit_lock', '1462752706:1'),
(811, 186, '_wp_attached_file', '2016/04/9cycxykk.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(812, 186, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:400;s:4:"file";s:20:"2016/04/9cycxykk.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"9cycxykk-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"9cycxykk-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:20:"9cycxykk-250x200.jpg";s:5:"width";i:250;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"9cycxykk-180x144.jpg";s:5:"width";i:180;s:6:"height";i:144;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"9cycxykk-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(825, 190, '_wp_attached_file', '2016/04/img01-2.jpg'),
(826, 190, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:400;s:4:"file";s:19:"2016/04/img01-2.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img01-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img01-2-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:14:"featured-slide";a:4:{s:4:"file";s:19:"img01-2-500x386.jpg";s:5:"width";i:500;s:6:"height";i:386;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img01-2-250x200.jpg";s:5:"width";i:250;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img01-2-180x144.jpg";s:5:"width";i:180;s:6:"height";i:144;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img01-2-400x320.jpg";s:5:"width";i:400;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(827, 190, '_wp_attachment_image_alt', 'kem trang'),
(828, 191, '_wp_attached_file', '2016/04/img02-1.jpg'),
(829, 191, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:400;s:4:"file";s:19:"2016/04/img02-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img02-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img02-1-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:14:"featured-slide";a:4:{s:4:"file";s:19:"img02-1-500x386.jpg";s:5:"width";i:500;s:6:"height";i:386;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img02-1-250x200.jpg";s:5:"width";i:250;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img02-1-180x144.jpg";s:5:"width";i:180;s:6:"height";i:144;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img02-1-400x320.jpg";s:5:"width";i:400;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(830, 191, '_wp_attachment_image_alt', 'kem trang'),
(831, 192, '_wp_attached_file', '2016/04/img03-2.jpg'),
(832, 192, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:400;s:4:"file";s:19:"2016/04/img03-2.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img03-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img03-2-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:14:"featured-slide";a:4:{s:4:"file";s:19:"img03-2-500x386.jpg";s:5:"width";i:500;s:6:"height";i:386;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img03-2-250x200.jpg";s:5:"width";i:250;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img03-2-180x144.jpg";s:5:"width";i:180;s:6:"height";i:144;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img03-2-400x320.jpg";s:5:"width";i:400;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(833, 192, '_wp_attachment_image_alt', 'kem trang'),
(834, 168, '_thumbnail_id', '192'),
(835, 194, '_wp_attached_file', '2016/04/img06-2.jpg'),
(836, 194, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:400;s:4:"file";s:19:"2016/04/img06-2.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img06-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img06-2-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:14:"featured-slide";a:4:{s:4:"file";s:19:"img06-2-500x386.jpg";s:5:"width";i:500;s:6:"height";i:386;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img06-2-250x200.jpg";s:5:"width";i:250;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img06-2-180x144.jpg";s:5:"width";i:180;s:6:"height";i:144;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img06-2-400x320.jpg";s:5:"width";i:400;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(837, 161, '_actived_d_gallery', '1'),
(851, 62, '_wp_old_slug', '62__trashed'),
(853, 27, '_wc_rating_count', 'a:1:{i:2;s:1:"1";}'),
(854, 27, '_wc_review_count', '1'),
(855, 27, '_wc_average_rating', '2.00'),
(857, 158, '_actived_d_gallery', '1'),
(858, 158, '_wc_rating_count', 'a:1:{i:1;s:1:"1";}'),
(859, 158, '_wc_review_count', '1'),
(860, 158, '_wc_average_rating', '1.00'),
(861, 156, '_actived_d_gallery', '1'),
(862, 198, '_wp_attached_file', '2016/04/img07-2.jpg'),
(863, 198, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:400;s:4:"file";s:19:"2016/04/img07-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img07-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img07-2-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img07-2-250x200.jpg";s:5:"width";i:250;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img07-2-180x144.jpg";s:5:"width";i:180;s:6:"height";i:144;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img07-2-400x320.jpg";s:5:"width";i:400;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(864, 198, '_wp_attachment_image_alt', 'My pham'),
(865, 161, '_thumbnail_id', '198'),
(866, 31, '_actived_d_gallery', '1'),
(867, 29, '_actived_d_gallery', '1'),
(868, 200, '_wp_attached_file', '2016/04/img05-2.jpg'),
(869, 200, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:400;s:4:"file";s:19:"2016/04/img05-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"img05-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"img05-2-300x240.jpg";s:5:"width";i:300;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:19:"img05-2-250x200.jpg";s:5:"width";i:250;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"img05-2-180x144.jpg";s:5:"width";i:180;s:6:"height";i:144;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"img05-2-400x320.jpg";s:5:"width";i:400;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(870, 29, '_thumbnail_id', '200'),
(871, 200, '_wp_attachment_image_alt', 'Kem'),
(872, 28, '_actived_d_gallery', '1'),
(873, 27, '_actived_d_gallery', '1'),
(874, 202, '_wp_attached_file', '2016/05/baner_MY-PHAM-DUONG-TRANG-1.jpg'),
(875, 202, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1366;s:6:"height";i:440;s:4:"file";s:39:"2016/05/baner_MY-PHAM-DUONG-TRANG-1.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"baner_MY-PHAM-DUONG-TRANG-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:38:"baner_MY-PHAM-DUONG-TRANG-1-300x97.jpg";s:5:"width";i:300;s:6:"height";i:97;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:39:"baner_MY-PHAM-DUONG-TRANG-1-768x247.jpg";s:5:"width";i:768;s:6:"height";i:247;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:40:"baner_MY-PHAM-DUONG-TRANG-1-1024x330.jpg";s:5:"width";i:1024;s:6:"height";i:330;s:9:"mime-type";s:10:"image/jpeg";}s:14:"featured-slide";a:4:{s:4:"file";s:40:"baner_MY-PHAM-DUONG-TRANG-1-1366x440.jpg";s:5:"width";i:1366;s:6:"height";i:440;s:9:"mime-type";s:10:"image/jpeg";}s:20:"featured-slide-thumb";a:4:{s:4:"file";s:38:"baner_MY-PHAM-DUONG-TRANG-1-250x81.jpg";s:5:"width";i:250;s:6:"height";i:81;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"baner_MY-PHAM-DUONG-TRANG-1-180x58.jpg";s:5:"width";i:180;s:6:"height";i:58;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"baner_MY-PHAM-DUONG-TRANG-1-400x129.jpg";s:5:"width";i:400;s:6:"height";i:129;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:39:"baner_MY-PHAM-DUONG-TRANG-1-600x193.jpg";s:5:"width";i:600;s:6:"height";i:193;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(876, 202, '_wp_attachment_image_alt', 'Duong trang da'),
(877, 180, '_thumbnail_id', '202'),
(882, 62, '_thumbnail_id', '202'),
(883, 210, '_order_key', 'wc_order_574fc4cc7a832'),
(884, 210, '_order_currency', 'VND'),
(885, 210, '_prices_include_tax', 'no'),
(886, 210, '_customer_ip_address', '::1'),
(887, 210, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.3; rv:46.0) Gecko/20100101 Firefox/46.0'),
(888, 210, '_customer_user', '0'),
(889, 210, '_created_via', 'checkout'),
(890, 210, '_order_version', '2.5.5'),
(891, 210, '_billing_first_name', 'hong'),
(892, 210, '_billing_last_name', 'diep'),
(893, 210, '_billing_company', 'sss'),
(894, 210, '_billing_email', 'hoadiepdo@gmail.com'),
(895, 210, '_billing_phone', '01224444'),
(896, 210, '_billing_country', 'VN'),
(897, 210, '_billing_address_1', 'zzzz'),
(898, 210, '_billing_address_2', ''),
(899, 210, '_billing_city', 'zzz'),
(900, 210, '_billing_state', ''),
(901, 210, '_shipping_first_name', 'hong'),
(902, 210, '_shipping_last_name', 'diep'),
(903, 210, '_shipping_company', 'sss'),
(904, 210, '_shipping_country', 'VN'),
(905, 210, '_shipping_address_1', 'zzzz'),
(906, 210, '_shipping_address_2', ''),
(907, 210, '_shipping_city', 'zzz'),
(908, 210, '_shipping_state', ''),
(909, 210, '_shipping_postcode', ''),
(910, 210, '_payment_method', 'cod'),
(911, 210, '_payment_method_title', 'Trả tiền mặt khi nhận hàng'),
(912, 210, '_order_shipping', ''),
(913, 210, '_cart_discount', '0'),
(914, 210, '_cart_discount_tax', '0'),
(915, 210, '_order_tax', '0'),
(916, 210, '_order_shipping_tax', '0'),
(917, 210, '_order_total', '300.000'),
(918, 210, '_download_permissions_granted', '1'),
(919, 210, '_recorded_sales', 'yes'),
(920, 210, '_order_stock_reduced', '1'),
(921, 212, '_edit_last', '1'),
(922, 212, '_edit_lock', '1465531137:1'),
(923, 214, '_menu_item_type', 'post_type'),
(924, 214, '_menu_item_menu_item_parent', '0'),
(925, 214, '_menu_item_object_id', '212'),
(926, 214, '_menu_item_object', 'page'),
(927, 214, '_menu_item_target', ''),
(928, 214, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(929, 214, '_menu_item_xfn', ''),
(930, 214, '_menu_item_url', ''),
(932, 215, '_menu_item_type', 'post_type'),
(933, 215, '_menu_item_menu_item_parent', '0'),
(934, 215, '_menu_item_object_id', '184'),
(935, 215, '_menu_item_object', 'page'),
(936, 215, '_menu_item_target', ''),
(937, 215, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(938, 215, '_menu_item_xfn', ''),
(939, 215, '_menu_item_url', ''),
(941, 216, '_menu_item_type', 'post_type'),
(942, 216, '_menu_item_menu_item_parent', '0'),
(943, 216, '_menu_item_object_id', '21'),
(944, 216, '_menu_item_object', 'page'),
(945, 216, '_menu_item_target', ''),
(946, 216, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(947, 216, '_menu_item_xfn', ''),
(948, 216, '_menu_item_url', ''),
(950, 217, '_menu_item_type', 'post_type'),
(951, 217, '_menu_item_menu_item_parent', '0'),
(952, 217, '_menu_item_object_id', '20'),
(953, 217, '_menu_item_object', 'page'),
(954, 217, '_menu_item_target', ''),
(955, 217, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(956, 217, '_menu_item_xfn', ''),
(957, 217, '_menu_item_url', ''),
(959, 218, '_menu_item_type', 'post_type'),
(960, 218, '_menu_item_menu_item_parent', '0'),
(961, 218, '_menu_item_object_id', '19'),
(962, 218, '_menu_item_object', 'page'),
(963, 218, '_menu_item_target', ''),
(964, 218, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(965, 218, '_menu_item_xfn', ''),
(966, 218, '_menu_item_url', ''),
(968, 219, '_menu_item_type', 'post_type'),
(969, 219, '_menu_item_menu_item_parent', '0'),
(970, 219, '_menu_item_object_id', '16'),
(971, 219, '_menu_item_object', 'page'),
(972, 219, '_menu_item_target', ''),
(973, 219, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(974, 219, '_menu_item_xfn', ''),
(975, 219, '_menu_item_url', ''),
(977, 220, '_menu_item_type', 'post_type'),
(978, 220, '_menu_item_menu_item_parent', '0'),
(979, 220, '_menu_item_object_id', '14'),
(980, 220, '_menu_item_object', 'page'),
(981, 220, '_menu_item_target', ''),
(982, 220, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(983, 220, '_menu_item_xfn', ''),
(984, 220, '_menu_item_url', '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2016-04-13 06:11:29', '2016-04-13 05:11:29', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng toàn thân zoley 150gr', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2016-04-25 05:27:34', '2016-04-24 22:27:34', '', 0, 'http://localhost/mypham/?p=1', 0, 'post', '', 1),
(2, 1, '2016-04-13 06:11:29', '2016-04-13 05:11:29', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/mypham/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2016-04-13 06:11:29', '2016-04-13 05:11:29', '', 0, 'http://localhost/mypham/?page_id=2', 0, 'page', '', 0),
(4, 1, '2016-04-15 12:29:19', '2016-04-15 05:29:19', 'Mọi thắc mắc Quý khách sẽ được giải đáp trong vòng 1-2 ngày', 'Liên hệ', '', 'publish', 'closed', 'closed', '', 'lien-he', '', '', '2016-04-19 13:04:16', '2016-04-19 06:04:16', '', 0, 'http://localhost/mypham/?page_id=4', 0, 'page', '', 0),
(5, 1, '2016-04-15 12:29:19', '2016-04-15 05:29:19', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2016-04-15 12:29:19', '2016-04-15 05:29:19', '', 4, 'http://localhost/mypham/2016/04/15/4-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2016-04-15 12:30:41', '2016-04-15 05:30:41', '', 'Chính  sách đại lý', '', 'publish', 'closed', 'closed', '', 'chinh-sach-dai-ly', '', '', '2016-04-15 12:30:41', '2016-04-15 05:30:41', '', 0, 'http://localhost/mypham/?page_id=6', 0, 'page', '', 0),
(7, 1, '2016-04-15 12:30:41', '2016-04-15 05:30:41', '', 'Chính  sách đại lý', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2016-04-15 12:30:41', '2016-04-15 05:30:41', '', 6, 'http://localhost/mypham/2016/04/15/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2016-04-15 12:32:36', '2016-04-15 05:32:36', '', 'Chính sách bán hàng', '', 'publish', 'closed', 'closed', '', 'chinh-sach-ban-hang', '', '', '2016-04-15 12:32:36', '2016-04-15 05:32:36', '', 0, 'http://localhost/mypham/?page_id=8', 0, 'page', '', 0),
(9, 1, '2016-04-15 12:32:36', '2016-04-15 05:32:36', '', 'Chính sách bán hàng', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2016-04-15 12:32:36', '2016-04-15 05:32:36', '', 8, 'http://localhost/mypham/2016/04/15/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2016-04-15 12:33:32', '2016-04-15 05:33:32', '', 'Chính sách giao nhận, vận chuyển', '', 'publish', 'closed', 'closed', '', 'chinh-sach-giao-nhan-van-chuyen', '', '', '2016-04-15 12:33:32', '2016-04-15 05:33:32', '', 0, 'http://localhost/mypham/?page_id=10', 0, 'page', '', 0),
(11, 1, '2016-04-15 12:33:32', '2016-04-15 05:33:32', '', 'Chính sách giao nhận, vận chuyển', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2016-04-15 12:33:32', '2016-04-15 05:33:32', '', 10, 'http://localhost/mypham/2016/04/15/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2016-04-15 12:33:51', '2016-04-15 05:33:51', '', 'Hướng dẫn tư vấn', '', 'publish', 'closed', 'closed', '', 'huong-dan-tu-van', '', '', '2016-04-15 12:33:51', '2016-04-15 05:33:51', '', 0, 'http://localhost/mypham/?page_id=12', 0, 'page', '', 0),
(13, 1, '2016-04-15 12:33:51', '2016-04-15 05:33:51', '', 'Hướng dẫn tư vấn', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2016-04-15 12:33:51', '2016-04-15 05:33:51', '', 12, 'http://localhost/mypham/2016/04/15/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2016-04-15 12:34:35', '2016-04-15 05:34:35', '', 'Nhận trao gởi, ký gửi mua mỹ phẩm', '', 'publish', 'closed', 'closed', '', 'nhan-trao-goi-ky-gui-mua-my-pham', '', '', '2016-04-15 12:34:35', '2016-04-15 05:34:35', '', 0, 'http://localhost/mypham/?page_id=14', 0, 'page', '', 0),
(15, 1, '2016-04-15 12:34:35', '2016-04-15 05:34:35', '', 'Nhận trao gởi, ký gửi mua mỹ phẩm', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2016-04-15 12:34:35', '2016-04-15 05:34:35', '', 14, 'http://localhost/mypham/2016/04/15/14-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2016-04-15 12:35:12', '2016-04-15 05:35:12', 'Mỹ Phẩm Hoàng Dung\r\n<strong>Địa chỉ</strong>: 1063 Huỳnh Tấn Phát, Phường Phú Thuận, Quận 7, HCM\r\n<strong>Giờ làm việc</strong>: 9h-12h, T2-CN, trừ ngày lễ\r\n<strong>Điện thoại</strong>: 0903 665 795 - 0912 883137\r\n<strong>Email</strong>: infor@myphamhoangdung.com', 'Liên hệ với chúng tôi', '', 'publish', 'closed', 'closed', '', 'lien-he-voi-chung-toi', '', '', '2016-04-19 05:20:02', '2016-04-18 22:20:02', '', 0, 'http://localhost/mypham/?page_id=16', 0, 'page', '', 0),
(17, 1, '2016-04-15 12:35:12', '2016-04-15 05:35:12', '', 'Liên hệ với chúng tôi', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2016-04-15 12:35:12', '2016-04-15 05:35:12', '', 16, 'http://localhost/mypham/2016/04/15/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2016-04-15 13:37:36', '2016-04-15 06:37:36', '', 'Cửa hàng', '', 'publish', 'closed', 'closed', '', 'mua', '', '', '2016-04-15 13:37:36', '2016-04-15 06:37:36', '', 0, 'http://localhost/mypham/mua/', 0, 'page', '', 0),
(19, 1, '2016-04-15 13:37:36', '2016-04-15 06:37:36', '[woocommerce_cart]', 'Giỏ hàng', '', 'publish', 'closed', 'closed', '', 'gio-hang', '', '', '2016-04-15 13:37:36', '2016-04-15 06:37:36', '', 0, 'http://localhost/mypham/gio-hang/', 0, 'page', '', 0),
(20, 1, '2016-04-15 13:37:36', '2016-04-15 06:37:36', '[woocommerce_checkout]', 'Thanh toán', '', 'publish', 'closed', 'closed', '', 'thanh-toan', '', '', '2016-04-15 13:37:36', '2016-04-15 06:37:36', '', 0, 'http://localhost/mypham/thanh-toan/', 0, 'page', '', 0),
(21, 1, '2016-04-15 13:37:37', '2016-04-15 06:37:37', '[woocommerce_my_account]', 'Tài khoản của tôi', '', 'publish', 'closed', 'closed', '', 'tai-khoan', '', '', '2016-04-15 13:37:37', '2016-04-15 06:37:37', '', 0, 'http://localhost/mypham/tai-khoan/', 0, 'page', '', 0),
(23, 1, '2016-04-18 05:57:01', '2016-04-17 22:57:01', 'KEM DƯỠNG CHẤT COLAGEN CHỐNG LÃO HÓA DA\r\n\r\n– Công dụng : Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng. Kem giúp cân bằng độ ẩm cho da và sắc tố da. Không bắt nắng và hiệu quả sau 7 ngày sử dụng.\r\n– Thành phần : Do được chiết xuất từ Vitamin B3, C, E và dưỡng chất Collagen giúp nuôi dưỡng da tốt nhất, chống lão hóa da, làm tan nếp nhăn, se khít lỗ chân lông và duy trì làn da mịn màng tươi trẻ.\r\n\r\n– Cách sử dụng : Rửa da sạch bằng nước ấm, lau khô . Thoa một lượng kem vừa đủ toàn da mặt rồi nhẹ nhàng massage toàn mặt cho kem thấm vào da. Nên thoa kem ngày 1 lấn đến 2 lần để đạt kết quả tốt nhất.\r\n\r\n– Chỉ tiêu kích ứng : Đặc biệt không kích ứng da, thích hợp với mọi loại da.\r\n\r\n**Lưu ý khi mua\r\n\r\n– Chúng tôi giao sản phẩm tận nơi đến khách hàng\r\n\r\n– Vui lòng kiểm tra kỹ trước khi nhận\r\n\r\n– Trong 3 ngày sử dụng nếu bị dị ứng có quyền trả lại và được hoàn tiền\r\n\r\n&gt;&gt; Sản phẩm độc quyền của Cty được Sở Y Tế cấp phép an toàn và được phép lưu hành ra thị trường với số CÔNG BỐ SẢN PHẨM: 003829/14/CBMP-HCM.', 'Kem dưỡng chất chống lão hóa 10gr', 'Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng', 'publish', 'open', 'closed', '', 'kem-duong-chat-chong-lao-hoa-10gr', '', '', '2016-04-18 12:12:21', '2016-04-18 05:12:21', '', 0, 'http://localhost/mypham/?post_type=product&#038;p=23', 0, 'product', '', 0),
(24, 1, '2016-04-18 05:55:23', '2016-04-17 22:55:23', '', 'img08', '', 'inherit', 'open', 'closed', '', 'img08', '', '', '2016-04-18 12:12:17', '2016-04-18 05:12:17', '', 23, 'http://localhost/mypham/wp-content/uploads/2016/04/img08.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2016-04-18 05:56:53', '2016-04-17 22:56:53', '', 'KEM DƯỠNG TRẮNG DA TRỊ MỤN CAO CẤP KBONE', '', 'inherit', 'open', 'closed', '', 'img06', '', '', '2016-04-18 12:10:33', '2016-04-18 05:10:33', '', 23, 'http://localhost/mypham/wp-content/uploads/2016/04/img06.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2016-04-18 05:56:55', '2016-04-17 22:56:55', '', 'img07', '', 'inherit', 'open', 'closed', '', 'img07', '', '', '2016-04-18 12:11:49', '2016-04-18 05:11:49', '', 23, 'http://localhost/mypham/wp-content/uploads/2016/04/img07.png', 0, 'attachment', 'image/png', 0),
(27, 1, '2016-04-18 05:57:22', '2016-04-17 22:57:22', '<p>KEM DƯỠNG CHẤT COLAGEN CHỐNG LÃO HÓA DA</p><p>– Công dụng : Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng. Kem giúp cân bằng độ ẩm cho da và sắc tố da. Không bắt nắng và hiệu quả sau 7 ngày sử dụng.<br /> – Thành phần : Do được chiết xuất từ Vitamin B3, C, E và dưỡng chất Collagen giúp nuôi dưỡng da tốt nhất, chống lão hóa da, làm tan nếp nhăn, se khít lỗ chân lông và duy trì làn da mịn màng tươi trẻ.</p><p>– Cách sử dụng : Rửa da sạch bằng nước ấm, lau khô . Thoa một lượng kem vừa đủ toàn da mặt rồi nhẹ nhàng massage toàn mặt cho kem thấm vào da. Nên thoa kem ngày 1 lấn đến 2 lần để đạt kết quả tốt nhất.</p><p>– Chỉ tiêu kích ứng : Đặc biệt không kích ứng da, thích hợp với mọi loại da.</p><p>**Lưu ý khi mua</p><p>– Chúng tôi giao sản phẩm tận nơi đến khách hàng</p><p>– Vui lòng kiểm tra kỹ trước khi nhận</p><p>– Trong 3 ngày sử dụng nếu bị dị ứng có quyền trả lại và được hoàn tiền</p><p>&gt;&gt; Sản phẩm độc quyền của Cty được Sở Y Tế cấp phép an toàn và được phép lưu hành ra thị trường với số CÔNG BỐ SẢN PHẨM: 003829/14/CBMP-HCM.</p>', 'Kem dưỡng chất chống lão hóa', '', 'publish', 'open', 'closed', '', 'kem-duong-chat-chong-lao-hoa', '', '', '2016-05-10 14:14:51', '2016-05-10 07:14:51', '', 0, 'http://localhost/mypham/san-pham/kem-duong-chat-chong-lao-hoa/', 0, 'product', '', 1),
(28, 1, '2016-04-18 05:58:28', '2016-04-17 22:58:28', '<p>KEM DƯỠNG CHẤT COLAGEN CHỐNG LÃO HÓA DA – Công dụng : Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng. Kem giúp cân bằng độ ẩm cho da và sắc tố da. Không bắt nắng và hiệu quả sau 7 ngày sử dụng. – Thành phần : Do được chiết xuất từ Vitamin B3, C, E và dưỡng chất Collagen giúp nuôi dưỡng da tốt nhất, chống lão hóa da, làm tan nếp nhăn, se khít lỗ chân lông và duy trì làn da mịn màng tươi trẻ. – Cách sử dụng : Rửa da sạch bằng nước ấm, lau khô . Thoa một lượng kem vừa đủ toàn da mặt rồi nhẹ nhàng massage toàn mặt cho kem thấm vào da. Nên thoa kem ngày 1 lấn đến 2 lần để đạt kết quả tốt nhất. – Chỉ tiêu kích ứng : Đặc biệt không kích ứng da, thích hợp với mọi loại da. **Lưu ý khi mua – Chúng tôi giao sản phẩm tận nơi đến khách hàng – Vui lòng kiểm tra kỹ trước khi nhận – Trong 3 ngày sử dụng nếu bị dị ứng có quyền trả lại và được hoàn tiền &gt;&gt; Sản phẩm độc quyền của Cty được Sở Y Tế cấp phép an toàn và được phép lưu hành ra thị trường với số CÔNG BỐ SẢN PHẨM: 003829/14/CBMP-HCM.</p>', 'Nước tẩy trang zoley toner 160ml', '<p>Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh</p>', 'publish', 'open', 'closed', '', 'nuoc-tay-trang-zoley-toner-160ml', '', '', '2016-05-10 14:13:15', '2016-05-10 07:13:15', '', 0, 'http://localhost/mypham/san-pham/nuoc-tay-trang-zoley-toner-160ml/', 0, 'product', '', 0),
(29, 1, '2016-04-18 06:00:28', '2016-04-17 23:00:28', '<p>KEM DƯỠNG CHẤT COLAGEN CHỐNG LÃO HÓA DA</p><p>– Công dụng : Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng. Kem giúp cân bằng độ ẩm cho da và sắc tố da. Không bắt nắng và hiệu quả sau 7 ngày sử dụng.<br /> – Thành phần : Do được chiết xuất từ Vitamin B3, C, E và dưỡng chất Collagen giúp nuôi dưỡng da tốt nhất, chống lão hóa da, làm tan nếp nhăn, se khít lỗ chân lông và duy trì làn da mịn màng tươi trẻ.</p><p>– Cách sử dụng : Rửa da sạch bằng nước ấm, lau khô . Thoa một lượng kem vừa đủ toàn da mặt rồi nhẹ nhàng massage toàn mặt cho kem thấm vào da. Nên thoa kem ngày 1 lấn đến 2 lần để đạt kết quả tốt nhất.</p><p>– Chỉ tiêu kích ứng : Đặc biệt không kích ứng da, thích hợp với mọi loại da.</p><p>**Lưu ý khi mua</p><p>– Chúng tôi giao sản phẩm tận nơi đến khách hàng</p><p>– Vui lòng kiểm tra kỹ trước khi nhận</p><p>– Trong 3 ngày sử dụng nếu bị dị ứng có quyền trả lại và được hoàn tiền</p><p>&gt;&gt; Sản phẩm độc quyền của Cty được Sở Y Tế cấp phép an toàn và được phép lưu hành ra thị trường với số CÔNG BỐ SẢN PHẨM: 003829/14/CBMP-HCM.</p>', 'kem dưỡng trắng da trị mụn cao cấp 50gr', '<p>Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh</p>', 'publish', 'open', 'closed', '', 'kem-duong-trang-da-tri-mun-cao-cap-50gr', '', '', '2016-05-10 14:12:23', '2016-05-10 07:12:23', '', 0, 'http://localhost/mypham/san-pham/kem-duong-trang-da-tri-mun-cao-cap-50gr/', 0, 'product', '', 2),
(31, 1, '2016-04-18 06:01:47', '2016-04-17 23:01:47', '<p>KEM DƯỠNG CHẤT COLAGEN CHỐNG LÃO HÓA DA – Công dụng : Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng. Kem giúp cân bằng độ ẩm cho da và sắc tố da. Không bắt nắng và hiệu quả sau 7 ngày sử dụng. – Thành phần : Do được chiết xuất từ Vitamin B3, C, E và dưỡng chất Collagen giúp nuôi dưỡng da tốt nhất, chống lão hóa da, làm tan nếp nhăn, se khít lỗ chân lông và duy trì làn da mịn màng tươi trẻ. – Cách sử dụng : Rửa da sạch bằng nước ấm, lau khô . Thoa một lượng kem vừa đủ toàn da mặt rồi nhẹ nhàng massage toàn mặt cho kem thấm vào da. Nên thoa kem ngày 1 lấn đến 2 lần để đạt kết quả tốt nhất. – Chỉ tiêu kích ứng : Đặc biệt không kích ứng da, thích hợp với mọi loại da. **Lưu ý khi mua – Chúng tôi giao sản phẩm tận nơi đến khách hàng – Vui lòng kiểm tra kỹ trước khi nhận – Trong 3 ngày sử dụng nếu bị dị ứng có quyền trả lại và được hoàn tiền &gt;&gt; Sản phẩm độc quyền của Cty được Sở Y Tế cấp phép an toàn và được phép lưu hành ra thị trường với số CÔNG BỐ SẢN PHẨM: 003829/14/CBMP-HCM.</p>', 'KEM DƯỠNG TRẮNG DA TRỊ MỤN CAO CẤP KBONE', '<p>Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng</p>', 'publish', 'open', 'closed', '', 'kem-duong-trang-da-tri-mun-cao-cap-kbone', '', '', '2016-05-10 14:09:46', '2016-05-10 07:09:46', '', 0, 'http://localhost/mypham/san-pham/kem-duong-trang-da-tri-mun-cao-cap-kbone/', 0, 'product', '', 0),
(33, 1, '2016-04-18 09:40:28', '2016-04-18 02:40:28', 'KEM DƯỠNG CHẤT COLAGEN CHỐNG LÃO HÓA DA\n\n– Công dụng : Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng. Kem giúp cân bằng độ ẩm cho da và sắc tố da. Không bắt nắng và hiệu quả sau 7 ngày sử dụng.\n– Thành phần : Do được chiết xuất từ Vitamin B3, C, E và dưỡng chất Collagen giúp nuôi dưỡng da tốt nhất, chống lão hóa da, làm tan nếp nhăn, se khít lỗ chân lông và duy trì làn da mịn màng tươi trẻ.\n\n– Cách sử dụng : Rửa da sạch bằng nước ấm, lau khô . Thoa một lượng kem vừa đủ toàn da mặt rồi nhẹ nhàng massage toàn mặt cho kem thấm vào da. Nên thoa kem ngày 1 lấn đến 2 lần để đạt kết quả tốt nhất.\n\n– Chỉ tiêu kích ứng : Đặc biệt không kích ứng da, thích hợp với mọi loại da.\n\n**Lưu ý khi mua\n\n– Chúng tôi giao sản phẩm tận nơi đến khách hàng\n\n– Vui lòng kiểm tra kỹ trước khi nhận\n\n– Trong 3 ngày sử dụng nếu bị dị ứng có quyền trả lại và được hoàn tiền\n\n&gt;&gt; Sản phẩm độc quyền của Cty được Sở Y Tế cấp phép an toàn và được phép lưu hành ra thị trường với số CÔNG BỐ SẢN PHẨM: 003829/14/CBMP-HCM.', 'KEM DƯỠNG TRẮNG DA TRỊ MỤN CAO CẤP KBONE', 'Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng', 'inherit', 'closed', 'closed', '', '31-autosave-v1', '', '', '2016-04-18 09:40:28', '2016-04-18 02:40:28', '', 31, 'http://localhost/mypham/?p=33', 0, 'revision', '', 0),
(35, 1, '2016-04-18 13:33:21', '2016-04-18 06:33:21', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n\r\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\r\n\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n280k/150gr', 'Kem dưỡng trắng toàn thân zoley 150gr Hướng dẫn 3 bước làm đẹp', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2016-04-18 13:33:21', '2016-04-18 06:33:21', '', 1, 'http://localhost/mypham/?p=35', 0, 'revision', '', 0),
(36, 1, '2016-04-19 03:15:09', '2016-04-18 20:15:09', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone –</p><p>Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄</p><p>Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet –</p><p>Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016 –</p><p>Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone –</p><p>Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄</p><p>Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet –</p><p>Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016 – Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng toàn thân zoley 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-toan-than-zoley-150gr', '', '', '2016-04-25 05:21:48', '2016-04-24 22:21:48', '', 0, 'http://localhost/mypham/?p=36', 0, 'post', '', 0),
(38, 1, '2016-04-19 03:15:01', '2016-04-18 20:15:01', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\n\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n280k/150gr', 'Kem dưỡng trắng toàn thân zoley 150gr', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2016-04-19 03:15:01', '2016-04-18 20:15:01', '', 1, 'http://localhost/mypham/?p=38', 0, 'revision', '', 0),
(39, 1, '2016-04-19 03:15:06', '2016-04-18 20:15:06', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n\r\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\r\n\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n280k/150gr', 'Kem dưỡng trắng toàn thân zoley 150gr', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2016-04-19 03:15:06', '2016-04-18 20:15:06', '', 36, 'http://localhost/mypham/?p=39', 0, 'revision', '', 0),
(40, 1, '2016-04-19 05:12:43', '2016-04-18 22:12:43', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Kem dưỡng trắng da se khít lỗ chân lông 35gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-se-khit-lo-chan-long-35gr', '', '', '2016-04-25 04:59:24', '2016-04-24 21:59:24', '', 0, 'http://localhost/mypham/?p=40', 0, 'post', '', 0),
(41, 1, '2016-04-19 03:23:21', '2016-04-18 20:23:21', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr', '', '', '2016-04-25 05:26:27', '2016-04-24 22:26:27', '', 0, 'http://localhost/mypham/?p=41', 0, 'post', '', 0),
(43, 1, '2016-04-19 03:23:21', '2016-04-18 20:23:21', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\nSản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n\r\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\nSản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.\r\n\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n280k/150gr', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2016-04-19 03:23:21', '2016-04-18 20:23:21', '', 41, 'http://localhost/mypham/?p=43', 0, 'revision', '', 0),
(44, 1, '2016-04-19 03:25:37', '2016-04-18 20:25:37', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Kem dưỡng trắng da se khít lỗ chân lông', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-se-khit-lo-chan-long', '', '', '2016-04-25 05:07:52', '2016-04-24 22:07:52', '', 0, 'http://localhost/mypham/?p=44', 0, 'post', '', 0),
(46, 1, '2016-04-19 03:25:37', '2016-04-18 20:25:37', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n\r\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\r\n\r\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\r\nCực chất dạng tinh chất trà xanh..\r\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\r\n\r\n130k/330ml\r\nzoley-gel-tay-te-bao-chet\r\n\r\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\r\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\r\n\r\n190k/900ml\r\nsua-tam-gold-plus-2016\r\n\r\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\r\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\r\n280k/150gr', 'Kem dưỡng trắng da se khít lỗ chân lông', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2016-04-19 03:25:37', '2016-04-18 20:25:37', '', 44, 'http://localhost/mypham/?p=46', 0, 'revision', '', 0),
(47, 1, '2016-04-19 05:12:43', '2016-04-18 22:12:43', '', 'Kem dưỡng trắng da se khít lỗ chân lông 35gr', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2016-04-19 05:12:43', '2016-04-18 22:12:43', '', 40, 'http://localhost/mypham/?p=47', 0, 'revision', '', 0),
(48, 1, '2016-04-19 05:13:47', '2016-04-18 22:13:47', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'publish', 'open', 'open', '', 'nuoc-hoa-nu-jolie-dion-emotion-50ml', '', '', '2016-04-25 05:00:47', '2016-04-24 22:00:47', '', 0, 'http://localhost/mypham/?p=48', 0, 'post', '', 0),
(49, 1, '2016-04-19 05:13:47', '2016-04-18 22:13:47', '', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-04-19 05:13:47', '2016-04-18 22:13:47', '', 48, 'http://localhost/mypham/?p=49', 0, 'revision', '', 0),
(50, 1, '2016-04-19 05:18:17', '2016-04-18 22:18:17', 'Mỹ Phẩm Hoàng Dung\nĐịa chỉ: 1063 Huỳnh Tấn Phát, Phường Phú Thuận, Quận 7, HCM\nGiờ làm việc: 9h-12h, T2-CN, trừ ngày lễ\nĐiện t\n\n&nbsp;', 'Liên hệ với chúng tôi', '', 'inherit', 'closed', 'closed', '', '16-autosave-v1', '', '', '2016-04-19 05:18:17', '2016-04-18 22:18:17', '', 16, 'http://localhost/mypham/?p=50', 0, 'revision', '', 0),
(51, 1, '2016-04-19 05:20:02', '2016-04-18 22:20:02', 'Mỹ Phẩm Hoàng Dung\r\n<strong>Địa chỉ</strong>: 1063 Huỳnh Tấn Phát, Phường Phú Thuận, Quận 7, HCM\r\n<strong>Giờ làm việc</strong>: 9h-12h, T2-CN, trừ ngày lễ\r\n<strong>Điện thoại</strong>: 0903 665 795 - 0912 883137\r\n<strong>Email</strong>: infor@myphamhoangdung.com', 'Liên hệ với chúng tôi', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2016-04-19 05:20:02', '2016-04-18 22:20:02', '', 16, 'http://localhost/mypham/?p=51', 0, 'revision', '', 0),
(52, 1, '2016-04-19 05:22:20', '2016-04-18 22:22:20', ' ', '', '', 'publish', 'closed', 'closed', '', '52', '', '', '2016-06-10 11:01:35', '2016-06-10 04:01:35', '', 0, 'http://localhost/mypham/?p=52', 5, 'nav_menu_item', '', 0),
(53, 1, '2016-04-19 05:22:20', '2016-04-18 22:22:20', ' ', '', '', 'publish', 'closed', 'closed', '', '53', '', '', '2016-06-10 11:01:35', '2016-06-10 04:01:35', '', 0, 'http://localhost/mypham/?p=53', 4, 'nav_menu_item', '', 0),
(54, 1, '2016-04-19 05:22:20', '2016-04-18 22:22:20', ' ', '', '', 'publish', 'closed', 'closed', '', '54', '', '', '2016-06-10 11:01:35', '2016-06-10 04:01:35', '', 0, 'http://localhost/mypham/?p=54', 3, 'nav_menu_item', '', 0),
(55, 1, '2016-04-19 05:22:19', '2016-04-18 22:22:19', ' ', '', '', 'publish', 'closed', 'closed', '', '55', '', '', '2016-06-10 11:01:35', '2016-06-10 04:01:35', '', 0, 'http://localhost/mypham/?p=55', 2, 'nav_menu_item', '', 0),
(56, 1, '2016-04-19 05:22:19', '2016-04-18 22:22:19', ' ', '', '', 'publish', 'closed', 'closed', '', '56', '', '', '2016-06-10 11:01:35', '2016-06-10 04:01:35', '', 0, 'http://localhost/mypham/?p=56', 1, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(57, 1, '2016-04-19 05:57:08', '2016-04-18 22:57:08', '<p>Họ và Tên:*<br />\r\n    [text* your-name] </p>\r\n\r\n<p>Email:*<br />\r\n    [email* your-email] </p>\r\n\r\n<p>Số điện thoại:*<br />\r\n    [tel* tel-774]\r\n </p>\r\n\r\n<p>Thông điệp:*<br />\r\n    [textarea* your-message] </p>\r\n\r\n<p>[submit "Gửi đi"]</p>\nMy pham "[your-subject]"\n[your-name] <hongdiepbach@gmail.com>\nGửi đến từ: [your-name] <[your-email]>\r\nTiêu đề: [your-subject]\r\n\r\nNội dung thông điệp:\r\n[your-message]\r\n\r\n--\r\nEmail này gửi từ My pham (http://localhost/mypham)\nhongdiepbach@gmail.com\nReply-To: [your-email]\n\n\n\n\nMy pham "[your-subject]"\nMy pham <hongdiepbach@gmail.com>\nNội dung thông điệp:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on My pham (http://localhost/mypham)\n[your-email]\nReply-To: hongdiepbach@gmail.com\n\n\n\nCám ơn bạn đã gửi thông điệp !\nGửi email thất bại. Vui lòng kiểm tra thông tin nhập và gửi lại\nMột vài trường chưa hợp lệ, vui lòng nhập lại\nGửi email thất bại. Vui lòng kiểm tra thông tin nhập và gửi lại\nYou must accept the terms and conditions before sending your message.\nVui lòng nhập\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nSố chưa hợp lệ\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nBạn đã nhập sai mã CAPTCHA.\nEmail chưa hợp lệ\nThe URL is invalid.\nSố điện thoại chưa hợp lệ', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2016-04-21 13:32:28', '2016-04-21 06:32:28', '', 0, 'http://localhost/mypham/?post_type=wpcf7_contact_form&#038;p=57', 0, 'wpcf7_contact_form', '', 0),
(62, 1, '2016-04-19 10:05:46', '2016-04-19 03:05:46', '', 'anh 1', '', 'publish', 'closed', 'closed', '', '62', '', '', '2016-05-10 14:30:12', '2016-05-10 07:30:12', '', 0, 'http://localhost/mypham/?post_type=slide&#038;p=62', 0, 'slide', '', 0),
(63, 1, '2016-04-19 10:05:28', '2016-04-19 03:05:28', '', 'baner-1600x400', '', 'inherit', 'open', 'closed', '', 'baner-1600x400', '', '', '2016-04-19 10:05:41', '2016-04-19 03:05:41', '', 62, 'http://localhost/mypham/wp-content/uploads/2016/04/baner-1600x400.jpg', 0, 'attachment', 'image/jpeg', 0),
(65, 1, '2016-04-19 13:04:16', '2016-04-19 06:04:16', 'Mọi thắc mắc Quý khách sẽ được giải đáp trong vòng 1-2 ngày', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2016-04-19 13:04:16', '2016-04-19 06:04:16', '', 4, 'http://localhost/mypham/?p=65', 0, 'revision', '', 0),
(67, 1, '2016-04-21 13:45:17', '2016-04-21 06:45:17', '<p><iframe src="//www.youtube.com/embed/Y15-Tsn5lr4" width="600" height="300" allowfullscreen="allowfullscreen"></iframe></p>', 'Video giới thiệu sản phẩm cty mỹ phẩm Hoàng Hưng Long Cosmetic', '', 'publish', 'open', 'open', '', 'video-gioi-thieu-san-pham-cty-my-pham-hoang-hung-long-cosmetic', '', '', '2016-05-29 12:52:31', '2016-05-29 05:52:31', '', 0, 'http://localhost/mypham/?p=67', 0, 'post', '', 0),
(69, 1, '2016-04-21 13:45:17', '2016-04-21 06:45:17', '<iframe src="//www.youtube.com/embed/Y15-Tsn5lr4" width="300" height="280" allowfullscreen="allowfullscreen"></iframe>', 'Video giới thiệu sản phẩm cty mỹ phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2016-04-21 13:45:17', '2016-04-21 06:45:17', '', 67, 'http://localhost/mypham/?p=69', 0, 'revision', '', 0),
(70, 1, '2016-04-21 13:46:21', '2016-04-21 06:46:21', '<iframe src="//www.youtube.com/embed/Y15-Tsn5lr4" width="600" height="300" allowfullscreen="allowfullscreen"></iframe>', 'Video giới thiệu sản phẩm cty mỹ phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2016-04-21 13:46:21', '2016-04-21 06:46:21', '', 67, 'http://localhost/mypham/?p=70', 0, 'revision', '', 0),
(72, 1, '2016-04-21 13:48:28', '2016-04-21 06:48:28', '<p><iframe src="//www.youtube.com/embed/m5jI5kpoHhI" width="600" height="300" allowfullscreen="allowfullscreen"></iframe></p>', 'Đêm sự kiện 26-12-2015 Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'publish', 'open', 'open', '', 'dem-su-kien-26-12-2015-my-pham-hoang-hung-long-cosmetic', '', '', '2016-05-29 12:36:58', '2016-05-29 05:36:58', '', 0, 'http://localhost/mypham/?p=72', 0, 'post', '', 0),
(73, 1, '2016-04-21 13:48:28', '2016-04-21 06:48:28', '<iframe src="//www.youtube.com/embed/m5jI5kpoHhI" width="600" height="300" allowfullscreen="allowfullscreen"></iframe>', 'Đêm sự kiện 26-12-2015 Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '72-revision-v1', '', '', '2016-04-21 13:48:28', '2016-04-21 06:48:28', '', 72, 'http://localhost/mypham/?p=73', 0, 'revision', '', 0),
(74, 1, '2016-04-21 13:49:21', '2016-04-21 06:49:21', '<p><iframe src="https://www.youtube.com/embed/Y15-Tsn5lr4?controls=0&amp;showinfo=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>', 'Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'publish', 'open', 'open', '', '74', '', '', '2016-05-12 12:40:45', '2016-05-12 05:40:45', '', 0, 'http://localhost/mypham/?p=74', 0, 'post', '', 0),
(75, 1, '2016-04-21 13:49:21', '2016-04-21 06:49:21', '<iframe src="//www.youtube.com/embed/9plfxBLqIbw" width="600" height="300" allowfullscreen="allowfullscreen"></iframe>', '', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2016-04-21 13:49:21', '2016-04-21 06:49:21', '', 74, 'http://localhost/mypham/?p=75', 0, 'revision', '', 0),
(76, 1, '2016-04-25 04:21:35', '2016-04-24 21:21:35', '<iframe src="//www.youtube.com/embed/9plfxBLqIbw" width="600" height="300" allowfullscreen="allowfullscreen"></iframe>', 'Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2016-04-25 04:21:35', '2016-04-24 21:21:35', '', 74, 'http://localhost/mypham/?p=76', 0, 'revision', '', 0),
(77, 1, '2016-04-25 04:49:47', '2016-04-24 21:49:47', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2>\n<p><strong>Bạn thân mến,</strong></p>\n<div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div>\n<div> </div>\n<div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div>\n<table border="0" width="722">\n<tbody>\n<tr>\n<td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td>\n<td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td>\n</tr>\n</tbody>\n</table>\n<div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div>\n<table border="0" width="722" cellspacing="0" cellpadding="0">\n<tbody>\n<tr>\n<td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td>\n<td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td>\n</tr>\n</tbody>\n</table>\n<p>&nbsp;</p>', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'inherit', 'closed', 'closed', '', '48-autosave-v1', '', '', '2016-04-25 04:49:47', '2016-04-24 21:49:47', '', 48, 'http://localhost/mypham/?p=77', 0, 'revision', '', 0),
(78, 1, '2016-04-25 04:39:31', '2016-04-24 21:39:31', '', '', '', 'inherit', 'open', 'closed', '', 'olympus-digital-camera', '', '', '2016-04-25 04:39:47', '2016-04-24 21:39:47', '', 48, 'http://localhost/mypham/wp-content/uploads/2016/04/thuong.jpg', 0, 'attachment', 'image/jpeg', 0),
(79, 1, '2016-04-25 04:40:34', '2016-04-24 21:40:34', '', 'image', '', 'inherit', 'open', 'closed', '', 'image', '', '', '2016-04-25 04:41:08', '2016-04-24 21:41:08', '', 48, 'http://localhost/mypham/wp-content/uploads/2016/04/image.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2016-04-25 04:41:14', '2016-04-24 21:41:14', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2>\r\n<strong>Bạn thân mến,</strong>\r\n<div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div>\r\n<div>Tuy nhiên, dù tình trạng có tồi tệ đến mức nào đi nữa thì tôi chắc chắn với bạn rằng: bạn vẫn hoàn toàn <strong>CÓ THỂ</strong> và hơn ai hết bạn là người <strong>CÓ QUYỀN</strong> được sở hữu làn da mịn màng vì bạn đã dành thời gian đọc bài viết này.\r\nHãy chia sẻ với chúng tôi những gì bạn đang phải chịu đựng vì chứng da dầu, có phải bạn luôn gặp những rắc rối vì những cảm giác dưới đây, đúng không?</div>\r\nQuanh năm suốt tháng lúc nào da cũng bóng loáng những dầu là dầu?\r\nTóc bạn rất hay bị bết dính?\r\nBạn phải thay giặt chăn, ga, chiếu, gối rất thường xuyên?\r\nLỗ chân lông trên da bạn to hơn những người khác?\r\nMụn mọc khắp nơi trên cơ thể bạn, đặc biệt là ở mặt, lưng và vai?\r\nMụn bọc nổi lên rất to, đau nhức, rất khó chịu?\r\nBạn luôn có cảm giác khuôn mặt mình như dơ dáy, nhờn nhờn khó chịu?\r\nBạn mất tự tin vì làn da của mình?\r\n<div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div>\r\n<table border="0" width="722">\r\n<tbody>\r\n<tr>\r\n<td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương\r\n<em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td>\r\n<td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div>\r\n<table border="0" width="722" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="278" height="300" />\r\nNgô Thị Xuân Thủy\r\n<em>(Nhân viên Marketing Cty Anova – Long An)</em></td>\r\n<td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-04-25 04:41:14', '2016-04-24 21:41:14', '', 48, 'http://localhost/mypham/?p=80', 0, 'revision', '', 0),
(81, 1, '2016-04-25 04:42:23', '2016-04-24 21:42:23', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2>\r\n<strong>Bạn thân mến,</strong>\r\n<div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div>\r\n<div>Tuy nhiên, dù tình trạng có tồi tệ đến mức nào đi nữa thì tôi chắc chắn với bạn rằng: bạn vẫn hoàn toàn <strong>CÓ THỂ</strong> và hơn ai hết bạn là người <strong>CÓ QUYỀN</strong> được sở hữu làn da mịn màng vì bạn đã dành thời gian đọc bài viết này.\r\nHãy chia sẻ với chúng tôi những gì bạn đang phải chịu đựng vì chứng da dầu, có phải bạn luôn gặp những rắc rối vì những cảm giác dưới đây, đúng không?</div>\r\nQuanh năm suốt tháng lúc nào da cũng bóng loáng những dầu là dầu?\r\nTóc bạn rất hay bị bết dính?\r\nBạn phải thay giặt chăn, ga, chiếu, gối rất thường xuyên?\r\nLỗ chân lông trên da bạn to hơn những người khác?\r\nMụn mọc khắp nơi trên cơ thể bạn, đặc biệt là ở mặt, lưng và vai?\r\nMụn bọc nổi lên rất to, đau nhức, rất khó chịu?\r\nBạn luôn có cảm giác khuôn mặt mình như dơ dáy, nhờn nhờn khó chịu?\r\nBạn mất tự tin vì làn da của mình?\r\n<div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div>\r\n<table border="0" width="722">\r\n<tbody>\r\n<tr>\r\n<td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương\r\n<em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td>\r\n<td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div>\r\n<table border="0" width="722" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" />\r\nNgô Thị Xuân Thủy\r\n<em>(Nhân viên Marketing Cty Anova – Long An)</em></td>\r\n<td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-04-25 04:42:23', '2016-04-24 21:42:23', '', 48, 'http://localhost/mypham/?p=81', 0, 'revision', '', 0),
(82, 1, '2016-04-25 04:44:50', '2016-04-24 21:44:50', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2>\r\n<strong>Bạn thân mến,</strong>\r\n<div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div>\r\n<div>Tuy nhiên, dù tình trạng có tồi tệ đến mức nào đi nữa thì tôi chắc chắn với bạn rằng: bạn vẫn hoàn toàn <strong>CÓ THỂ</strong> và hơn ai hết bạn là người <strong>CÓ QUYỀN</strong> được sở hữu làn da mịn màng vì bạn đã dành thời gian đọc bài viết này.\r\nHãy chia sẻ với chúng tôi những gì bạn đang phải chịu đựng vì chứng da dầu, có phải bạn luôn gặp những rắc rối vì những cảm giác dưới đây, đúng không?</div>\r\n<div></div>\r\n<div>Quanh năm suốt tháng lúc nào da cũng bóng loáng những dầu là dầu?</div>\r\nTóc bạn rất hay bị bết dính?\r\nBạn phải thay giặt chăn, ga, chiếu, gối rất thường xuyên?\r\nLỗ chân lông trên da bạn to hơn những người khác?\r\nMụn mọc khắp nơi trên cơ thể bạn, đặc biệt là ở mặt, lưng và vai?\r\nMụn bọc nổi lên rất to, đau nhức, rất khó chịu?\r\nBạn luôn có cảm giác khuôn mặt mình như dơ dáy, nhờn nhờn khó chịu?\r\nBạn mất tự tin vì làn da của mình?\r\n<div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div>\r\n<table border="0" width="722">\r\n<tbody>\r\n<tr>\r\n<td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương\r\n<em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td>\r\n<td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div>\r\n<table border="0" width="722" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" />\r\nNgô Thị Xuân Thủy\r\n<em>(Nhân viên Marketing Cty Anova – Long An)</em></td>\r\n<td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-04-25 04:44:50', '2016-04-24 21:44:50', '', 48, 'http://localhost/mypham/?p=82', 0, 'revision', '', 0),
(83, 1, '2016-04-25 04:47:13', '2016-04-24 21:47:13', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Quanh năm suốt tháng lúc nào da cũng bóng loáng những dầu là dầu?</div><p>Tóc bạn rất hay bị bết dính? Bạn phải thay giặt chăn, ga, chiếu, gối rất thường xuyên? Lỗ chân lông trên da bạn to hơn những người khác? Mụn mọc khắp nơi trên cơ thể bạn, đặc biệt là ở mặt, lưng và vai? Mụn bọc nổi lên rất to, đau nhức, rất khó chịu? Bạn luôn có cảm giác khuôn mặt mình như dơ dáy, nhờn nhờn khó chịu? Bạn mất tự tin vì làn da của mình?</p><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-04-25 04:47:13', '2016-04-24 21:47:13', '', 48, 'http://localhost/mypham/?p=83', 0, 'revision', '', 0),
(84, 1, '2016-04-25 04:47:39', '2016-04-24 21:47:39', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Nước hoa nữ Jolie Dion Emotion 50ml', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-04-25 04:47:39', '2016-04-24 21:47:39', '', 48, 'http://localhost/mypham/?p=84', 0, 'revision', '', 0),
(85, 1, '2016-04-25 04:58:41', '2016-04-24 21:58:41', '', 'ming01', '', 'inherit', 'open', 'closed', '', 'ming01', '', '', '2016-04-25 04:59:01', '2016-04-24 21:59:01', '', 40, 'http://localhost/mypham/wp-content/uploads/2016/04/ming01.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2016-04-25 04:59:22', '2016-04-24 21:59:22', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table><p>&nbsp;</p>', 'Kem dưỡng trắng da se khít lỗ chân lông 35gr', '', 'inherit', 'closed', 'closed', '', '40-autosave-v1', '', '', '2016-04-25 04:59:22', '2016-04-24 21:59:22', '', 40, 'http://localhost/mypham/?p=86', 0, 'revision', '', 0),
(87, 1, '2016-04-25 04:59:24', '2016-04-24 21:59:24', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Kem dưỡng trắng da se khít lỗ chân lông 35gr', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2016-04-25 04:59:24', '2016-04-24 21:59:24', '', 40, 'http://localhost/mypham/?p=87', 0, 'revision', '', 0),
(88, 1, '2016-04-25 05:00:41', '2016-04-24 22:00:41', '', 'mig02', '', 'inherit', 'open', 'closed', '', 'mig02', '', '', '2016-04-25 05:00:45', '2016-04-24 22:00:45', '', 48, 'http://localhost/mypham/wp-content/uploads/2016/04/mig02.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2016-04-25 05:01:41', '2016-04-24 22:01:41', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\n\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n280k/150gr', 'Kem dưỡng trắng da se khít lỗ chân lông', '', 'inherit', 'closed', 'closed', '', '44-autosave-v1', '', '', '2016-04-25 05:01:41', '2016-04-24 22:01:41', '', 44, 'http://localhost/mypham/?p=89', 0, 'revision', '', 0),
(91, 1, '2016-04-25 05:02:33', '2016-04-24 22:02:33', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế  từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Kem dưỡng trắng da se khít lỗ chân lông', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2016-04-25 05:02:33', '2016-04-24 22:02:33', '', 44, 'http://localhost/mypham/?p=91', 0, 'revision', '', 0),
(92, 1, '2016-04-25 05:07:21', '2016-04-24 22:07:21', '', 'img03', '', 'inherit', 'open', 'closed', '', 'img03', '', '', '2016-04-25 05:07:48', '2016-04-24 22:07:48', '', 44, 'http://localhost/mypham/wp-content/uploads/2016/04/img03.jpg', 0, 'attachment', 'image/jpeg', 0),
(93, 1, '2016-04-25 05:20:36', '2016-04-24 22:20:36', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\n❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗\n\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n280k/150gr', 'Kem dưỡng trắng toàn thân zoley 150gr', '', 'inherit', 'closed', 'closed', '', '36-autosave-v1', '', '', '2016-04-25 05:20:36', '2016-04-24 22:20:36', '', 36, 'http://localhost/mypham/?p=93', 0, 'revision', '', 0),
(94, 1, '2016-04-25 05:20:53', '2016-04-24 22:20:53', '', 'img05', '', 'inherit', 'open', 'closed', '', 'img05', '', '', '2016-04-25 05:20:59', '2016-04-24 22:20:59', '', 36, 'http://localhost/mypham/wp-content/uploads/2016/04/img05.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(95, 1, '2016-04-25 05:21:48', '2016-04-24 22:21:48', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone –</p><p>Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄</p><p>Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet –</p><p>Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016 –</p><p>Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone –</p><p>Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄</p><p>Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet –</p><p>Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016 – Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng toàn thân zoley 150gr', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2016-04-25 05:21:48', '2016-04-24 22:21:48', '', 36, 'http://localhost/mypham/?p=95', 0, 'revision', '', 0),
(96, 1, '2016-04-25 05:25:08', '2016-04-24 22:25:08', 'Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\nSản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n\n280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone\n\n– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY\nCực chất dạng tinh chất trà xanh..\nSản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.\n\n130k/330ml\nzoley-gel-tay-te-bao-chet\n\n– Bước 2: Tắm lại bằng Sữa tắm Gold Plus\nĐược kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường.\n\n190k/900ml\nsua-tam-gold-plus-2016\n\n– Bước 3: Lao khô thoa kem body kbone lên toàn thân\nVới công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích.\n280k/150gr', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '41-autosave-v1', '', '', '2016-04-25 05:25:08', '2016-04-24 22:25:08', '', 41, 'http://localhost/mypham/?p=96', 0, 'revision', '', 0),
(97, 1, '2016-04-25 05:26:17', '2016-04-24 22:26:17', '', 'img06', '', 'inherit', 'open', 'closed', '', 'img06-2', '', '', '2016-04-25 05:26:21', '2016-04-24 22:26:21', '', 41, 'http://localhost/mypham/wp-content/uploads/2016/04/img06.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2016-04-25 05:26:27', '2016-04-24 22:26:27', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2016-04-25 05:26:27', '2016-04-24 22:26:27', '', 41, 'http://localhost/mypham/?p=98', 0, 'revision', '', 0),
(99, 1, '2016-04-25 05:27:26', '2016-04-24 22:27:26', '', 'img04', '', 'inherit', 'open', 'closed', '', 'img04-2', '', '', '2016-04-25 05:27:31', '2016-04-24 22:27:31', '', 1, 'http://localhost/mypham/wp-content/uploads/2016/04/img04.jpg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2016-04-25 05:27:34', '2016-04-24 22:27:34', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. ❄ Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối.😉❗ 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng toàn thân zoley 150gr', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2016-04-25 05:27:34', '2016-04-24 22:27:34', '', 1, 'http://localhost/mypham/?p=100', 0, 'revision', '', 0),
(101, 1, '2016-04-25 05:29:44', '2016-04-24 22:29:44', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Kem dưỡng trắng da se khít lỗ chân lông 35gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-se-khit-lo-chan-long-35gr-2', '', '', '2016-04-25 05:29:44', '2016-04-24 22:29:44', '', 0, 'http://localhost/mypham/?p=101', 0, 'post', '', 0),
(102, 1, '2016-04-25 05:29:44', '2016-04-24 22:29:44', '<h2><strong>Phẩy tay da hết nhờn, bạn có tin?</strong></h2><p><strong>Bạn thân mến,</strong></p><div>Là người sở hữu làm da nhờn khiến bạn luôn cảm thấy khó chịu, nhất là vào những hôm nóng nực, da mặt bạn càng trở nên bóng nhờn như bôi mỡ<strong>, </strong>gây mất thẩm mỹ, bứt dứt và rất khó chịu. Không chỉ vậy, da nhờn dễ bắt bụi, làm tắc nghẽn lỗ chân lông, là thủ phạm gây nên mụn trứng cá. Làn da nhờn sẽ luôn làm bạn cảm thấy khó chịu, thiếu tự tin. Không những vậy, nguy cơ bị viêm da và nổi mụn của làn da nhờn cũng cao hơn các loại da khác.</div><div> </div><div>Nếu chính xác bạn đang phải sống trong tình trạng như trên, chắc chắn là da bạn thuộc loại da nhờn. Mời bạn cùng chia sẻ với những người cùng hoàn cảnh để hiều hơn về chính làn da của mình bạn nhé.</div><table border="0" width="722"><tbody><tr><td><img class="alignnone size-medium wp-image-78" src="http://localhost/mypham/wp-content/uploads/2016/04/thuong-300x230.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="230" />Huỳnh Thị Thương <em>(Nhân viên bán hàng, Cty Icom – Tp.HCM)</em></td><td><em>"Tôi là Huỳnh Thị Thương, chẳng biết từ bao giờ, da tôi ngày càng nhờn và mụn thì ngày càng dày thêm. Khủng hoảng mỗi lần soi gương và nhìn thấy “đèn pin” mọc chi chít trên làn da không tỳ vết trước đây của mình. Nghe mách nước của bạn bè, tôi đã dùng qua nhiều biện pháp “đông tây y kết hợp” mà tình hình cũng chỉ khả quan hơn trong thời gian ngắn và rồi kết quả lại đâu vào đấy. Một lần tình cờ, lang thang trên mạng tôi biết có bộ sản phẩm Phấn nụ dưỡng da nhờn, tôi đánh liều dùng thử. Ai ngờ tôi kết sản phầm này ngay lần sử dụng đầu tiên.</em></td></tr></tbody></table><div><em>Làn da tôi bây giờ hoàn toàn trơn láng, mịn màng, chính tôi cũng còn ngạc nhiên vì làn da của mình đã đẹp đến như vậy (như bạn thấy đó). Qua nhân viên tư vấn, tôi còn được biết Phấn nụ vốn chỉ có<strong> cung phi, mỹ nữ của triều đình mới có để dùng</strong>. Đặc biệt, phấn nụ còn là <strong>sản phẩm hoàn toàn được bào chế từ những nguyên liệu từ thiên nhiên</strong> nên rất hiền, <strong>không gây kích ứng hay bất kì một phản ứng bất lợi đối với làn da</strong> mỏng manh của chúng ta"</em></div><table border="0" width="722" cellspacing="0" cellpadding="0"><tbody><tr><td><img class="alignnone size-medium wp-image-79" src="http://localhost/mypham/wp-content/uploads/2016/04/image-278x300.jpg" alt="Ngô Thị Xuân Thủy" width="300" height="300" /> Ngô Thị Xuân Thủy <em>(Nhân viên Marketing Cty Anova – Long An)</em></td><td><em> "Tôi là Ngô Thị Xuân Thủy cũng là một trong những khách hàng thường xuyên và lâu năm của Phấn nụ cung đình Huế. Da tôi cũng thuộc loại da nhờn, nó đã gây cho tôi khá nhiều rắc rối mà đặc biệt, vào mùa nóng, kinh khủng lắm, da lúc nào cũng như bôi mỡ, khó chịu không tả nổi. Tôi biết bộ phấn nụ dưỡng da nhờn cao cấp cũng rất tình cờ từ một người quen ở Huế, nghe giới thiệu cũng tò mò muốn dùng thử nhưng dùng rồi ghiền luôn. Phải nói <strong>phấn nụ hút nhờn rất tốt,</strong> cảm giác vô cùng dễ chịu. Điều tôi kết nhất ở phấn nụ dưỡng da nhờn là không chỉ dưỡng da mà đây <strong>còn là một mỹ phẩm trang điểm</strong> tuyệt vời với những thành phần hoàn toàn thiên nhiên nên <strong>rất lành với da</strong>, <strong>không có bất kì phản ứng hóa học</strong> nào với da mặt của bạn.</em>"</td></tr></tbody></table>', 'Kem dưỡng trắng da se khít lỗ chân lông 35gr', '', 'inherit', 'closed', 'closed', '', '101-revision-v1', '', '', '2016-04-25 05:29:44', '2016-04-24 22:29:44', '', 101, 'http://localhost/mypham/?p=102', 0, 'revision', '', 0),
(103, 1, '2016-04-25 05:31:27', '2016-04-24 22:31:27', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-2', '', '', '2016-04-25 05:31:27', '2016-04-24 22:31:27', '', 0, 'http://localhost/mypham/?p=103', 0, 'post', '', 0),
(104, 1, '2016-04-25 05:31:02', '2016-04-24 22:31:02', '', 'img07', '', 'inherit', 'open', 'closed', '', 'img07-2', '', '', '2016-04-25 05:31:25', '2016-04-24 22:31:25', '', 103, 'http://localhost/mypham/wp-content/uploads/2016/04/img07.jpg', 0, 'attachment', 'image/jpeg', 0),
(105, 1, '2016-04-25 05:31:27', '2016-04-24 22:31:27', '<p>Hướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150grHướng dẫn 3 bước làm đẹp cùng kem dưỡng trắng toàn thân body kbone</p><p>– Bước 1: TẨY TẾ BÀO CHẾT GEL ZOLEY Cực chất dạng tinh chất trà xanh.. Sản phẩm dạng gel vô cùng dịu da và an toàn giúp da mau chóng loại bỏ đi tế bào chết giúp da sáng và mịn màng.ĐẶC BIỆT với dạng gel siêu mềm không gây xây xát da như dạng muối. 130k/330ml zoley-gel-tay-te-bao-chet</p><p>– Bước 2: Tắm lại bằng Sữa tắm Gold Plus Được kết hợp từ bột vàng 24K, Ngọc Trai và hương Chanel coco giúp tăng cường độ ẩm cho làn da bạn làm mịn da, lấy đi bụi bẩn và ngăn chặn các tác nhân gây hại của môi trường. 190k/900ml sua-tam-gold-plus-2016</p><p>– Bước 3: Lao khô thoa kem body kbone lên toàn thân Với công thức hoàn toàn mới nên kem tan nhanh và thấm sâu vào da, làm tan nếp nhăn trên da, xóa mờ vết thâm nám để giúp cho bạn có được làn da trắng hồng tự nhiên và rất an toàn khi sử dụng. Chất kem mịn màng không bết dính và cho bạn cảm giác mát lạnh thoải mái. Bạn dùng kbone ngày hay đêm tùy thích. 280k/150gr</p>', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2016-04-25 05:31:27', '2016-04-24 22:31:27', '', 103, 'http://localhost/mypham/?p=105', 0, 'revision', '', 0),
(116, 1, '2016-04-24 16:34:58', '2016-04-24 09:34:58', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-6', '', '', '2016-04-24 17:22:11', '2016-04-24 10:22:11', '', 0, 'http://localhost/mypham/?p=116', 0, 'post', '', 0),
(118, 1, '2016-04-24 16:34:58', '2016-04-24 09:34:58', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '116-revision-v1', '', '', '2016-04-24 16:34:58', '2016-04-24 09:34:58', '', 116, 'http://localhost/mypham/?p=118', 0, 'revision', '', 0),
(121, 1, '2016-04-24 17:10:42', '2016-04-24 10:10:42', '', 'img12', '', 'inherit', 'open', 'closed', '', 'img12-2', '', '', '2016-04-24 17:10:51', '2016-04-24 10:10:51', '', 0, 'http://localhost/mypham/wp-content/uploads/2016/04/img12-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(122, 1, '2016-04-24 17:11:19', '2016-04-24 10:11:19', '', 'img14', '', 'inherit', 'open', 'closed', '', 'img14', '', '', '2016-04-24 17:11:23', '2016-04-24 10:11:23', '', 0, 'http://localhost/mypham/wp-content/uploads/2016/04/img14.jpg', 0, 'attachment', 'image/jpeg', 0),
(123, 1, '2016-04-24 17:12:46', '2016-04-24 10:12:46', '', 'img15', '', 'inherit', 'open', 'closed', '', 'img15', '', '', '2016-04-24 17:12:50', '2016-04-24 10:12:50', '', 0, 'http://localhost/mypham/wp-content/uploads/2016/04/img15.jpg', 0, 'attachment', 'image/jpeg', 0),
(125, 1, '2016-04-24 17:18:53', '2016-04-24 10:18:53', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-3', '', '', '2016-04-24 17:18:53', '2016-04-24 10:18:53', '', 0, 'http://localhost/mypham/?p=125', 0, 'post', '', 0),
(126, 1, '2016-04-24 17:18:53', '2016-04-24 10:18:53', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2016-04-24 17:18:53', '2016-04-24 10:18:53', '', 125, 'http://localhost/mypham/?p=126', 0, 'revision', '', 0),
(127, 1, '2016-04-24 17:23:13', '2016-04-24 10:23:13', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-4', '', '', '2016-04-24 17:23:13', '2016-04-24 10:23:13', '', 0, 'http://localhost/mypham/?p=127', 0, 'post', '', 0),
(128, 1, '2016-04-24 17:23:05', '2016-04-24 10:23:05', '', 'img13', '', 'inherit', 'open', 'closed', '', 'img13', '', '', '2016-04-24 17:23:10', '2016-04-24 10:23:10', '', 127, 'http://localhost/mypham/wp-content/uploads/2016/04/img13.jpg', 0, 'attachment', 'image/jpeg', 0),
(129, 1, '2016-04-24 17:23:13', '2016-04-24 10:23:13', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2016-04-24 17:23:13', '2016-04-24 10:23:13', '', 127, 'http://localhost/mypham/?p=129', 0, 'revision', '', 0),
(130, 1, '2016-04-24 17:39:40', '2016-04-24 10:39:40', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-5', '', '', '2016-04-25 07:58:04', '2016-04-25 00:58:04', '', 0, 'http://localhost/mypham/?p=130', 0, 'post', '', 0),
(131, 1, '2016-04-24 17:39:31', '2016-04-24 10:39:31', '', 'img15', '', 'inherit', 'open', 'closed', '', 'img15-2', '', '', '2016-04-24 17:39:36', '2016-04-24 10:39:36', '', 130, 'http://localhost/mypham/wp-content/uploads/2016/04/img15-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(132, 1, '2016-04-24 17:39:40', '2016-04-24 10:39:40', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2016-04-24 17:39:40', '2016-04-24 10:39:40', '', 130, 'http://localhost/mypham/?p=132', 0, 'revision', '', 0),
(133, 1, '2016-04-24 17:40:18', '2016-04-24 10:40:18', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-7', '', '', '2016-04-25 08:03:32', '2016-04-25 01:03:32', '', 0, 'http://localhost/mypham/?p=133', 0, 'post', '', 0),
(135, 1, '2016-04-24 17:40:19', '2016-04-24 10:40:19', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '133-revision-v1', '', '', '2016-04-24 17:40:19', '2016-04-24 10:40:19', '', 133, 'http://localhost/mypham/?p=135', 0, 'revision', '', 0),
(136, 1, '2016-04-24 17:43:54', '2016-04-24 10:43:54', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-8', '', '', '2016-04-25 07:59:24', '2016-04-25 00:59:24', '', 0, 'http://localhost/mypham/?p=136', 0, 'post', '', 0),
(138, 1, '2016-04-24 17:43:54', '2016-04-24 10:43:54', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '136-revision-v1', '', '', '2016-04-24 17:43:54', '2016-04-24 10:43:54', '', 136, 'http://localhost/mypham/?p=138', 0, 'revision', '', 0),
(139, 1, '2016-04-24 17:47:52', '2016-04-24 10:47:52', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-9', '', '', '2016-04-24 17:55:44', '2016-04-24 10:55:44', '', 0, 'http://localhost/mypham/?p=139', 0, 'post', '', 0),
(141, 1, '2016-04-24 17:47:52', '2016-04-24 10:47:52', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2016-04-24 17:47:52', '2016-04-24 10:47:52', '', 139, 'http://localhost/mypham/?p=141', 0, 'revision', '', 0),
(142, 1, '2016-04-24 17:48:45', '2016-04-24 10:48:45', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-10', '', '', '2016-04-24 17:48:45', '2016-04-24 10:48:45', '', 0, 'http://localhost/mypham/?p=142', 0, 'post', '', 0),
(143, 1, '2016-04-24 17:48:45', '2016-04-24 10:48:45', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2016-04-24 17:48:45', '2016-04-24 10:48:45', '', 142, 'http://localhost/mypham/?p=143', 0, 'revision', '', 0),
(144, 1, '2016-04-24 17:55:39', '2016-04-24 10:55:39', '', 'img01', '', 'inherit', 'open', 'closed', '', 'img01', '', '', '2016-04-24 17:55:42', '2016-04-24 10:55:42', '', 139, 'http://localhost/mypham/wp-content/uploads/2016/04/img01.jpg', 0, 'attachment', 'image/jpeg', 0),
(145, 1, '2016-04-25 07:57:57', '2016-04-25 00:57:57', '', 'img02', '', 'inherit', 'open', 'closed', '', 'img02', '', '', '2016-04-25 07:58:02', '2016-04-25 00:58:02', '', 130, 'http://localhost/mypham/wp-content/uploads/2016/04/img02.jpg', 0, 'attachment', 'image/jpeg', 0),
(146, 1, '2016-04-25 07:59:16', '2016-04-25 00:59:16', '', 'img04', '', 'inherit', 'open', 'closed', '', 'img04-3', '', '', '2016-04-25 07:59:19', '2016-04-25 00:59:19', '', 136, 'http://localhost/mypham/wp-content/uploads/2016/04/img04-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(147, 1, '2016-04-25 08:00:22', '2016-04-25 01:00:22', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-11', '', '', '2016-04-25 08:00:22', '2016-04-25 01:00:22', '', 0, 'http://localhost/mypham/?p=147', 0, 'post', '', 0),
(148, 1, '2016-04-25 08:00:18', '2016-04-25 01:00:18', '', 'img03', '', 'inherit', 'open', 'closed', '', 'img03-2', '', '', '2016-04-25 08:00:21', '2016-04-25 01:00:21', '', 147, 'http://localhost/mypham/wp-content/uploads/2016/04/img03-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(149, 1, '2016-04-25 08:00:22', '2016-04-25 01:00:22', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '147-revision-v1', '', '', '2016-04-25 08:00:22', '2016-04-25 01:00:22', '', 147, 'http://localhost/mypham/?p=149', 0, 'revision', '', 0),
(150, 1, '2016-04-25 08:01:34', '2016-04-25 01:01:34', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'publish', 'open', 'open', '', 'kem-duong-trang-da-toan-than-ban-ngay-kbone-150gr-12', '', '', '2016-04-25 08:01:34', '2016-04-25 01:01:34', '', 0, 'http://localhost/mypham/?p=150', 0, 'post', '', 0),
(151, 1, '2016-04-25 08:01:31', '2016-04-25 01:01:31', '', 'img05', '', 'inherit', 'open', 'closed', '', 'img05-2', '', '', '2016-04-25 08:01:31', '2016-04-25 01:01:31', '', 150, 'http://localhost/mypham/wp-content/uploads/2016/04/img05-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(152, 1, '2016-04-25 08:01:34', '2016-04-25 01:01:34', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '150-revision-v1', '', '', '2016-04-25 08:01:34', '2016-04-25 01:01:34', '', 150, 'http://localhost/mypham/?p=152', 0, 'revision', '', 0),
(153, 1, '2016-04-25 08:02:08', '2016-04-25 01:02:08', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '127-autosave-v1', '', '', '2016-04-25 08:02:08', '2016-04-25 01:02:08', '', 127, 'http://localhost/mypham/?p=153', 0, 'revision', '', 0),
(154, 1, '2016-04-25 08:02:17', '2016-04-25 01:02:17', '', 'Kem dưỡng trắng da toàn thân ban ngày kbone 150gr', '', 'inherit', 'closed', 'closed', '', '125-autosave-v1', '', '', '2016-04-25 08:02:17', '2016-04-25 01:02:17', '', 125, 'http://localhost/mypham/?p=154', 0, 'revision', '', 0),
(156, 1, '2016-04-25 09:39:09', '2016-04-25 02:39:09', '', 'kem dưỡng trắng da trị mụn cao cấp 50gr', '', 'publish', 'open', 'closed', '', 'kem-duong-trang-da-tri-mun-cao-cap-50gr-2', '', '', '2016-05-10 14:09:04', '2016-05-10 07:09:04', '', 0, 'http://localhost/mypham/?post_type=product&#038;p=156', 0, 'product', '', 0),
(157, 1, '2016-04-25 09:38:54', '2016-04-25 02:38:54', '', 'img01', '', 'inherit', 'open', 'closed', '', 'img01-2', '', '', '2016-04-25 09:38:59', '2016-04-25 02:38:59', '', 156, 'http://localhost/mypham/wp-content/uploads/2016/04/img01-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(158, 1, '2016-04-25 09:39:48', '2016-04-25 02:39:48', '', 'kem dưỡng trắng da trị mụn cao cấp 50gr', '', 'publish', 'open', 'closed', '', 'kem-duong-trang-da-tri-mun-cao-cap-50gr-3', '', '', '2016-05-10 13:27:02', '2016-05-10 06:27:02', '', 0, 'http://localhost/mypham/?post_type=product&#038;p=158', 0, 'product', '', 1),
(160, 1, '2016-04-25 09:44:36', '2016-04-25 02:44:36', '', 'k8imsubu', '', 'inherit', 'open', 'closed', '', 'k8imsubu', '', '', '2016-04-25 09:44:42', '2016-04-25 02:44:42', '', 158, 'http://localhost/mypham/wp-content/uploads/2016/04/k8imsubu.jpg', 0, 'attachment', 'image/jpeg', 0),
(161, 1, '2016-04-25 09:46:29', '2016-04-25 02:46:29', '', 'Kem dưỡng trắng da trị mụn cao cấp 50gr', '', 'publish', 'open', 'closed', '', 'kem-duong-trang-da-tri-mun-cao-cap-50gr-4', '', '', '2016-05-10 14:06:58', '2016-05-10 07:06:58', '', 0, 'http://localhost/mypham/?post_type=product&#038;p=161', 0, 'product', '', 0),
(163, 1, '2016-04-25 09:46:23', '2016-04-25 02:46:23', '', 'img06', '', 'inherit', 'open', 'closed', '', 'img06-3', '', '', '2016-04-25 09:46:26', '2016-04-25 02:46:26', '', 161, 'http://localhost/mypham/wp-content/uploads/2016/04/img06-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(165, 1, '2016-04-25 12:35:59', '2016-04-25 05:35:59', '', 'id-post', '', 'publish', 'closed', 'closed', '', 'acf_id-post', '', '', '2016-04-26 11:04:39', '2016-04-26 04:04:39', '', 0, 'http://localhost/mypham/?post_type=acf&#038;p=165', 0, 'acf', '', 0),
(167, 1, '2016-04-25 12:39:53', '2016-04-25 05:39:53', '', 'img07', '', 'inherit', 'open', 'closed', '', 'img07-3', '', '', '2016-04-25 13:13:47', '2016-04-25 06:13:47', '', 161, 'http://localhost/mypham/wp-content/uploads/2016/04/img07-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(168, 1, '2016-04-25 13:14:05', '2016-04-25 06:14:05', '<p><strong>– CÔNG DỤNG CỦA KEM DƯỠNG TRẮNG DA TRỊ MỤN</strong></p><ul><li>Với Tinh Chất Hạt Đậu siêu ngừa mụn, yến mạch cùng các loại Vitamin, thảo dược có tác dụng làm giảm chất nhờn, kháng viêm, diệt khuẩn &amp; loại bỏ cồi mụn…</li><li>Thành phần Ngọc Trai thiên nhiên, giúp se khít lỗ chân lông giúp cho da bạn mịn màng &amp; trắng hồng tự nhiên hơn.</li></ul>', 'Kem dưỡng trắng da trị mụn cao cấp 50gr', '<p>Với Tinh Chất Hạt Đậu siêu ngừa mụn, yến mạch cùng các loại Vitamin</p>', 'publish', 'open', 'closed', '', 'kem-duong-trang-da-tri-mun-cao-cap-50gr-5', '', '', '2016-05-10 12:21:46', '2016-05-10 05:21:46', '', 0, 'http://localhost/mypham/?post_type=product&#038;p=168', 0, 'product', '', 1),
(170, 1, '2016-04-26 11:13:49', '2016-04-26 04:13:49', '', 'Sản phẩm ZOLEY', '', 'publish', 'open', 'open', '', 'san-pham-zoley', '', '', '2016-04-26 11:13:49', '2016-04-26 04:13:49', '', 0, 'http://localhost/mypham/?p=170', 0, 'post', '', 0),
(171, 1, '2016-04-26 11:13:33', '2016-04-26 04:13:33', '', 'imgzoley', '', 'inherit', 'open', 'closed', '', 'imgzoley', '', '', '2016-04-26 11:13:41', '2016-04-26 04:13:41', '', 170, 'http://localhost/mypham/wp-content/uploads/2016/04/imgzoley.jpg', 0, 'attachment', 'image/jpeg', 0),
(172, 1, '2016-04-26 11:13:49', '2016-04-26 04:13:49', '', 'Sản phẩm ZOLEY', '', 'inherit', 'closed', 'closed', '', '170-revision-v1', '', '', '2016-04-26 11:13:49', '2016-04-26 04:13:49', '', 170, 'http://localhost/mypham/?p=172', 0, 'revision', '', 0),
(175, 1, '2016-04-29 12:58:15', '2016-04-29 05:58:15', '', 'Order &ndash; Tháng Tư 29, 2016 @ 12:58 Chiều', '', 'wc-processing', 'closed', 'closed', 'order_5722f7f6e8c0f', 'don-hang-apr-29-2016-0558-am', '', '', '2016-04-29 12:58:15', '2016-04-29 05:58:15', '', 0, 'http://localhost/mypham/?post_type=shop_order&#038;p=175', 0, 'shop_order', '', 2),
(180, 1, '2016-05-07 11:18:29', '2016-05-07 04:18:29', '', 'Duong trang da', '', 'publish', 'closed', 'closed', '', 'duong-trang-da', '', '', '2016-05-10 14:23:25', '2016-05-10 07:23:25', '', 0, 'http://localhost/mypham/?post_type=slide&#038;p=180', 0, 'slide', '', 0),
(182, 1, '2016-05-08 05:27:25', '2016-05-07 22:27:25', '', 'hinh01', '', 'inherit', 'closed', 'closed', '', 'hinh01', '', '', '2016-05-08 05:27:35', '2016-05-07 22:27:35', '', 168, 'http://localhost/mypham/wp-content/uploads/2016/04/hinh01.jpg', 0, 'attachment', 'image/jpeg', 0),
(183, 1, '2016-05-09 05:55:56', '2016-05-08 22:55:56', '<p><strong>– CÔNG DỤNG CỦA KEM DƯỠNG TRẮNG DA TRỊ MỤN</strong></p><ul><li>Với Tinh Chất Hạt Đậu siêu ngừa mụn, yến mạch cùng các loại Vitamin, thảo dược có tác dụng làm giảm chất nhờn, kháng viêm, diệt khuẩn &amp; loại bỏ cồi mụn…</li><li>Thành phần Ngọc Trai thiên nhiên, giúp se khít lỗ chân lông giúp cho da bạn mịn màng &amp; trắng hồng tự nhiên hơn.</li></ul>', 'Kem dưỡng trắng da trị mụn cao cấp 50gr', '<ul><li>Với Tinh Chất Hạt Đậu siêu ngừa mụn, yến mạch cùng các loại Vitamin, thảo dược có tác dụng làm giảm chất nhờn, kháng viêm, diệt khuẩn &amp; loại bỏ cồi mụn…</li><li>Thành phần Ngọc Trai thiên nhiên, giúp se khít lỗ chân lông giúp cho da bạn mịn màng &amp; trắng hồng tự nhiên hơn.</li></ul>', 'inherit', 'closed', 'closed', '', '168-autosave-v1', '', '', '2016-05-09 05:55:56', '2016-05-08 22:55:56', '', 168, 'http://localhost/mypham/?p=183', 0, 'revision', '', 0),
(184, 1, '2016-05-09 07:02:29', '2016-05-09 00:02:29', '', 'Hướng dẫn mua hàng', '', 'publish', 'closed', 'closed', '', 'huong-dan-mua-hang', '', '', '2016-05-09 07:02:29', '2016-05-09 00:02:29', '', 0, 'http://localhost/mypham/?page_id=184', 0, 'page', '', 0),
(185, 1, '2016-05-09 07:02:29', '2016-05-09 00:02:29', '', 'Hướng dẫn mua hàng', '', 'inherit', 'closed', 'closed', '', '184-revision-v1', '', '', '2016-05-09 07:02:29', '2016-05-09 00:02:29', '', 184, 'http://localhost/mypham/?p=185', 0, 'revision', '', 0),
(186, 1, '2016-05-09 12:43:18', '2016-05-09 05:43:18', '', '9cycxykk', '', 'inherit', 'closed', 'closed', '', '9cycxykk', '', '', '2016-05-09 12:43:18', '2016-05-09 05:43:18', '', 168, 'http://localhost/mypham/wp-content/uploads/2016/04/9cycxykk.jpg', 0, 'attachment', 'image/jpeg', 0),
(190, 1, '2016-05-10 11:52:49', '2016-05-10 04:52:49', '', 'img01', '', 'inherit', 'closed', 'closed', '', 'img01-3', '', '', '2016-05-10 11:53:02', '2016-05-10 04:53:02', '', 168, 'http://localhost/mypham/wp-content/uploads/2016/04/img01-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(191, 1, '2016-05-10 11:53:15', '2016-05-10 04:53:15', '', 'img02', '', 'inherit', 'closed', 'closed', '', 'img02-2', '', '', '2016-05-10 11:53:20', '2016-05-10 04:53:20', '', 168, 'http://localhost/mypham/wp-content/uploads/2016/04/img02-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(192, 1, '2016-05-10 11:53:26', '2016-05-10 04:53:26', '', 'img03', '', 'inherit', 'closed', 'closed', '', 'img03-3', '', '', '2016-05-10 11:53:31', '2016-05-10 04:53:31', '', 168, 'http://localhost/mypham/wp-content/uploads/2016/04/img03-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(194, 1, '2016-05-10 12:28:07', '2016-05-10 05:28:07', '', 'img06', '', 'inherit', 'closed', 'closed', '', 'img06-4', '', '', '2016-05-10 12:28:07', '2016-05-10 05:28:07', '', 161, 'http://localhost/mypham/wp-content/uploads/2016/04/img06-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(198, 1, '2016-05-10 14:06:46', '2016-05-10 07:06:46', '', 'img07', '', 'inherit', 'closed', 'closed', '', 'img07-4', '', '', '2016-05-10 14:06:56', '2016-05-10 07:06:56', '', 161, 'http://localhost/mypham/wp-content/uploads/2016/04/img07-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(199, 1, '2016-05-10 14:08:14', '2016-05-10 07:08:14', '', 'kem dưỡng trắng da trị mụn cao cấp 50gr', '', 'inherit', 'closed', 'closed', '', '158-autosave-v1', '', '', '2016-05-10 14:08:14', '2016-05-10 07:08:14', '', 158, 'http://localhost/mypham/?p=199', 0, 'revision', '', 0),
(200, 1, '2016-05-10 14:11:24', '2016-05-10 07:11:24', '', 'img05', '', 'inherit', 'closed', 'closed', '', 'img05-3', '', '', '2016-05-10 14:11:32', '2016-05-10 07:11:32', '', 29, 'http://localhost/mypham/wp-content/uploads/2016/04/img05-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(201, 1, '2016-05-10 14:17:45', '2016-05-10 07:17:45', '<p>KEM DƯỠNG CHẤT COLAGEN CHỐNG LÃO HÓA DA</p><p>– Công dụng : Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng. Kem giúp cân bằng độ ẩm cho da và sắc tố da. Không bắt nắng và hiệu quả sau 7 ngày sử dụng.<br /> – Thành phần : Do được chiết xuất từ Vitamin B3, C, E và dưỡng chất Collagen giúp nuôi dưỡng da tốt nhất, chống lão hóa da, làm tan nếp nhăn, se khít lỗ chân lông và duy trì làn da mịn màng tươi trẻ.</p><p>– Cách sử dụng : Rửa da sạch bằng nước ấm, lau khô . Thoa một lượng kem vừa đủ toàn da mặt rồi nhẹ nhàng massage toàn mặt cho kem thấm vào da. Nên thoa kem ngày 1 lấn đến 2 lần để đạt kết quả tốt nhất.</p><p>– Chỉ tiêu kích ứng : Đặc biệt không kích ứng da, thích hợp với mọi loại da.</p><p>**Lưu ý khi mua</p><p>– Chúng tôi giao sản phẩm tận nơi đến khách hàng</p><p>– Vui lòng kiểm tra kỹ trước khi nhận</p><p>– Trong 3 ngày sử dụng nếu bị dị ứng có quyền trả lại và được hoàn tiền</p><p>&gt;&gt; Sản phẩm độc quyền của Cty được Sở Y Tế cấp phép an toàn và được phép lưu hành ra thị trường với số CÔNG BỐ SẢN PHẨM: 003829/14/CBMP-HCM.</p>', 'Kem dưỡng chất chống lão hóa 10gr', 'Kem tan nhanh thấm sâu vào da nên việc hấp thụ chuyển đổi tế bào rất nhanh. Đối với các loại da sần sùi, hư mốc, nhiều nếp nhăn cũng được xóa sạch sau thời gian sử dụng', 'inherit', 'closed', 'closed', '', '23-autosave-v1', '', '', '2016-05-10 14:17:45', '2016-05-10 07:17:45', '', 23, 'http://localhost/mypham/?p=201', 0, 'revision', '', 0),
(202, 1, '2016-05-10 14:23:14', '2016-05-10 07:23:14', '', 'baner_MY-PHAM-DUONG-TRANG-1', '', 'inherit', 'closed', 'closed', '', 'baner_my-pham-duong-trang-1', '', '', '2016-05-10 14:23:22', '2016-05-10 07:23:22', '', 180, 'http://localhost/mypham/wp-content/uploads/2016/05/baner_MY-PHAM-DUONG-TRANG-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(203, 1, '2016-05-12 12:40:16', '2016-05-12 05:40:16', '<p><iframe src="//www.youtube.com/embed/9plfxBLqIbw" width="600" height="300" allowfullscreen="allowfullscreen"></iframe></p>', 'Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2016-05-12 12:40:16', '2016-05-12 05:40:16', '', 74, 'http://localhost/mypham/?p=203', 0, 'revision', '', 0),
(204, 1, '2016-05-12 12:40:45', '2016-05-12 05:40:45', '<p><iframe src="https://www.youtube.com/embed/Y15-Tsn5lr4?controls=0&amp;showinfo=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>', 'Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2016-05-12 12:40:45', '2016-05-12 05:40:45', '', 74, 'http://localhost/mypham/?p=204', 0, 'revision', '', 0),
(205, 1, '2016-05-12 12:41:00', '2016-05-12 05:41:00', '<p><iframe src="https://www.youtube.com/embed/Y15-Tsn5lr4?controls=0&amp;showinfo=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>', 'Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '74-autosave-v1', '', '', '2016-05-12 12:41:00', '2016-05-12 05:41:00', '', 74, 'http://localhost/mypham/?p=205', 0, 'revision', '', 0),
(208, 1, '2016-05-29 12:36:58', '2016-05-29 05:36:58', '<p><iframe src="//www.youtube.com/embed/m5jI5kpoHhI" width="600" height="300" allowfullscreen="allowfullscreen"></iframe></p>', 'Đêm sự kiện 26-12-2015 Mỹ Phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '72-revision-v1', '', '', '2016-05-29 12:36:58', '2016-05-29 05:36:58', '', 72, 'http://localhost/mypham/?p=208', 0, 'revision', '', 0),
(209, 1, '2016-05-29 12:52:31', '2016-05-29 05:52:31', '<p><iframe src="//www.youtube.com/embed/Y15-Tsn5lr4" width="600" height="300" allowfullscreen="allowfullscreen"></iframe></p>', 'Video giới thiệu sản phẩm cty mỹ phẩm Hoàng Hưng Long Cosmetic', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2016-05-29 12:52:31', '2016-05-29 05:52:31', '', 67, 'http://localhost/mypham/?p=209', 0, 'revision', '', 0),
(210, 1, '2016-06-02 12:31:56', '2016-06-02 05:31:56', '', 'Order &ndash; Tháng Sáu 2, 2016 @ 12:31 Chiều', 'zz', 'wc-processing', 'closed', 'closed', 'order_574fc4cc5cfea', 'don-hang-jun-02-2016-0531-am', '', '', '2016-06-02 12:31:56', '2016-06-02 05:31:56', '', 0, 'http://localhost/mypham/?post_type=shop_order&#038;p=210', 0, 'shop_order', '', 2),
(211, 1, '2016-06-10 10:59:45', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-06-10 10:59:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/mypham/?p=211', 0, 'post', '', 0),
(212, 1, '2016-06-10 11:00:23', '2016-06-10 04:00:23', '', 'Sơ đồ Website', '', 'publish', 'closed', 'closed', '', 'so-do-website', '', '', '2016-06-10 11:00:23', '2016-06-10 04:00:23', '', 0, 'http://localhost/mypham/?page_id=212', 0, 'page', '', 0),
(213, 1, '2016-06-10 11:00:23', '2016-06-10 04:00:23', '', 'Sơ đồ Website', '', 'inherit', 'closed', 'closed', '', '212-revision-v1', '', '', '2016-06-10 11:00:23', '2016-06-10 04:00:23', '', 212, 'http://localhost/mypham/?p=213', 0, 'revision', '', 0),
(214, 1, '2016-06-10 11:01:35', '2016-06-10 04:01:35', ' ', '', '', 'publish', 'closed', 'closed', '', '214', '', '', '2016-06-10 11:01:35', '2016-06-10 04:01:35', '', 0, 'http://localhost/mypham/?p=214', 6, 'nav_menu_item', '', 0),
(215, 1, '2016-06-10 11:10:51', '2016-06-10 04:10:51', ' ', '', '', 'publish', 'closed', 'closed', '', '215', '', '', '2016-06-10 11:10:51', '2016-06-10 04:10:51', '', 0, 'http://localhost/mypham/?p=215', 1, 'nav_menu_item', '', 0),
(216, 1, '2016-06-10 11:10:52', '2016-06-10 04:10:52', ' ', '', '', 'publish', 'closed', 'closed', '', '216', '', '', '2016-06-10 11:10:52', '2016-06-10 04:10:52', '', 0, 'http://localhost/mypham/?p=216', 2, 'nav_menu_item', '', 0),
(217, 1, '2016-06-10 11:10:52', '2016-06-10 04:10:52', ' ', '', '', 'publish', 'closed', 'closed', '', '217', '', '', '2016-06-10 11:10:52', '2016-06-10 04:10:52', '', 0, 'http://localhost/mypham/?p=217', 3, 'nav_menu_item', '', 0),
(218, 1, '2016-06-10 11:10:52', '2016-06-10 04:10:52', ' ', '', '', 'publish', 'closed', 'closed', '', '218', '', '', '2016-06-10 11:10:52', '2016-06-10 04:10:52', '', 0, 'http://localhost/mypham/?p=218', 4, 'nav_menu_item', '', 0),
(219, 1, '2016-06-10 11:10:53', '2016-06-10 04:10:53', ' ', '', '', 'publish', 'closed', 'closed', '', '219', '', '', '2016-06-10 11:10:53', '2016-06-10 04:10:53', '', 0, 'http://localhost/mypham/?p=219', 5, 'nav_menu_item', '', 0),
(220, 1, '2016-06-10 11:10:53', '2016-06-10 04:10:53', ' ', '', '', 'publish', 'closed', 'closed', '', '220', '', '', '2016-06-10 11:10:53', '2016-06-10 04:10:53', '', 0, 'http://localhost/mypham/?p=220', 6, 'nav_menu_item', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorised', 'uncategorised', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'Sản Phẩm', 'san-pham', 0),
(7, 'Sản Phẩm KBONE', 'kbone', 0),
(8, 'Nước Hoa', 'nuoc-hoa', 0),
(9, 'Nhãn Hiệu ZOLEY', 'zoley', 0),
(10, 'Sản Phẩm Khác', 'san-pham-khac', 0),
(11, 'Tin tức', 'tin-tuc', 0),
(12, 'Sự kiện', 'su-kien', 0),
(13, 'Góc Chia Sẽ', 'goc-chia-se', 0),
(14, 'menu-footer', 'menu-footer', 0),
(15, 'video', 'video', 0),
(16, 'slider-index', 'slider', 0),
(17, 'san pham con', 'san-pham-con', 0),
(18, 'san pham', 'san-pham-kbone', 0),
(19, 'Kem duong da', 'kem-duong-da', 0),
(20, 'Tin Hot', 'tin-hot', 0),
(21, 'Sự kiện hot', 'su-kien-hot', 0),
(22, 'Bài viêt cho ản phẩm', 'product-vs-posts', 0),
(23, 'Video hot', 'video-hot', 0),
(24, 'take-care', 'take-care', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 11, 0),
(1, 12, 0),
(1, 13, 0),
(23, 2, 0),
(23, 6, 0),
(23, 7, 0),
(23, 8, 0),
(23, 9, 0),
(23, 10, 0),
(27, 2, 0),
(27, 6, 0),
(27, 7, 0),
(27, 8, 0),
(27, 9, 0),
(27, 10, 0),
(28, 2, 0),
(28, 6, 0),
(28, 7, 0),
(28, 8, 0),
(28, 9, 0),
(28, 10, 0),
(29, 2, 0),
(29, 6, 0),
(29, 7, 0),
(29, 9, 0),
(29, 10, 0),
(31, 2, 0),
(31, 6, 0),
(31, 7, 0),
(31, 9, 0),
(31, 10, 0),
(36, 11, 0),
(36, 13, 0),
(40, 12, 0),
(40, 13, 0),
(41, 11, 0),
(41, 13, 0),
(44, 11, 0),
(44, 12, 0),
(44, 13, 0),
(48, 12, 0),
(48, 13, 0),
(52, 14, 0),
(53, 14, 0),
(54, 14, 0),
(55, 14, 0),
(56, 14, 0),
(62, 16, 0),
(67, 15, 0),
(67, 23, 0),
(72, 15, 0),
(72, 23, 0),
(74, 15, 0),
(101, 11, 0),
(101, 13, 0),
(103, 12, 0),
(103, 13, 0),
(116, 20, 0),
(125, 20, 0),
(127, 20, 0),
(130, 20, 0),
(133, 20, 0),
(136, 21, 0),
(139, 21, 0),
(142, 21, 0),
(147, 21, 0),
(150, 21, 0),
(156, 2, 0),
(156, 6, 0),
(156, 7, 0),
(156, 9, 0),
(158, 2, 0),
(158, 6, 0),
(158, 9, 0),
(161, 2, 0),
(161, 6, 0),
(161, 7, 0),
(161, 8, 0),
(168, 2, 0),
(168, 6, 0),
(168, 7, 0),
(168, 8, 0),
(168, 9, 0),
(170, 22, 0),
(180, 16, 0),
(214, 14, 0),
(215, 24, 0),
(216, 24, 0),
(217, 24, 0),
(218, 24, 0),
(219, 24, 0),
(220, 24, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'product_type', '', 0, 9),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_cat', '', 0, 9),
(7, 7, 'product_cat', '', 6, 8),
(8, 8, 'product_cat', '', 6, 5),
(9, 9, 'product_cat', '', 6, 8),
(10, 10, 'product_cat', '', 6, 5),
(11, 11, 'category', '', 13, 5),
(12, 12, 'category', '', 13, 5),
(13, 13, 'category', '', 0, 8),
(14, 14, 'nav_menu', '', 0, 6),
(15, 15, 'category', '', 0, 3),
(16, 16, 'slideshow', '', 0, 2),
(17, 17, 'product_cat', '', 9, 0),
(18, 18, 'product_cat', '', 7, 0),
(19, 19, 'product_cat', '', 9, 0),
(20, 20, 'category', '', 0, 5),
(21, 21, 'category', '', 0, 5),
(22, 22, 'category', '', 0, 1),
(23, 23, 'category', '', 0, 2),
(24, 24, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'diepbnh'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', ''),
(13, 1, 'show_welcome_panel', '0'),
(15, 1, 'wp_dashboard_quick_press_last_post_id', '211'),
(16, 1, 'manageedit-shop_ordercolumnshidden', 'a:1:{i:0;s:15:"billing_address";}'),
(17, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce&hidetb=1&wplink=1&advImgDetails=show'),
(18, 1, 'wp_user-settings-time', '1463031644'),
(19, 1, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:1:{s:32:"006f52e9102a8d3be2fe5614f42ba989";a:9:{s:10:"product_id";i:168;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:10:"line_total";d:500;s:8:"line_tax";i:0;s:13:"line_subtotal";i:500;s:17:"line_subtotal_tax";i:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}}}'),
(20, 1, 'closedpostboxes_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
(21, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:30:"woocommerce_endpoints_nav_link";i:1;s:21:"add-post-type-product";i:2;s:12:"add-post_tag";i:3;s:15:"add-product_cat";i:4;s:15:"add-product_tag";}'),
(24, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:5:"4.4.1";}'),
(25, 1, 'ai_bpm_ignore_notice', 'true'),
(26, 1, 'woo_dynamic_gallery-plugin_framework_global_box-opened', '1'),
(35, 1, 'woo_dynamic_gallery-wc_dgallery_style_settings', 'a:1:{i:0;s:26:"wc_dgallery_dimensions_box";}'),
(36, 1, 'session_tokens', 'a:2:{s:64:"df97435e96d8454b25c62c0d0f2925a60ec8868245d0728b748ce777875bff3d";a:4:{s:10:"expiration";i:1465703979;s:2:"ip";s:3:"::1";s:2:"ua";s:65:"Mozilla/5.0 (Windows NT 6.3; rv:46.0) Gecko/20100101 Firefox/46.0";s:5:"login";i:1465531179;}s:64:"954bbf1e930aa0667ccd2cf263e574ed1156cc2390d3929b6e5653e3b20344ae";a:4:{s:10:"expiration";i:1465706888;s:2:"ip";s:3:"::1";s:2:"ua";s:65:"Mozilla/5.0 (Windows NT 6.3; rv:46.0) Gecko/20100101 Firefox/46.0";s:5:"login";i:1465534088;}}'),
(37, 1, 'nav_menu_recently_edited', '24');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'bnhdiep', '$P$Ba9Y2JLOsjJ6.HoF6DwqvHpYdV9.lp/', 'diepbnh', 'hongdiepbach@gmail.com', '', '2016-04-13 05:11:28', '', 0, 'diepbnh');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

DROP TABLE IF EXISTS `wp_woocommerce_api_keys`;
CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

DROP TABLE IF EXISTS `wp_woocommerce_attribute_taxonomies`;
CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` longtext COLLATE utf8mb4_unicode_ci,
  `attribute_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

DROP TABLE IF EXISTS `wp_woocommerce_downloadable_product_permissions`;
CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) NOT NULL,
  `download_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

DROP TABLE IF EXISTS `wp_woocommerce_order_itemmeta`;
CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_qty', '2'),
(2, 1, '_tax_class', ''),
(3, 1, '_product_id', '31'),
(4, 1, '_variation_id', '0'),
(5, 1, '_line_subtotal', '600'),
(6, 1, '_line_total', '600'),
(7, 1, '_line_subtotal_tax', '0'),
(8, 1, '_line_tax', '0'),
(9, 1, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(10, 2, '_qty', '1'),
(11, 2, '_tax_class', ''),
(12, 2, '_product_id', '29'),
(13, 2, '_variation_id', '0'),
(14, 2, '_line_subtotal', '300'),
(15, 2, '_line_total', '300'),
(16, 2, '_line_subtotal_tax', '0'),
(17, 2, '_line_tax', '0'),
(18, 2, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

DROP TABLE IF EXISTS `wp_woocommerce_order_items`;
CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) NOT NULL,
  `order_item_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, 'KEM DƯỠNG TRẮNG DA TRỊ MỤN CAO CẤP KBONE', 'line_item', 175),
(2, 'kem dưỡng trắng da trị mụn cao cấp 50gr', 'line_item', 210);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

DROP TABLE IF EXISTS `wp_woocommerce_sessions`;
CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:18:{s:4:"cart";s:306:"a:1:{s:32:"006f52e9102a8d3be2fe5614f42ba989";a:9:{s:10:"product_id";i:168;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:10:"line_total";d:500;s:8:"line_tax";i:0;s:13:"line_subtotal";i:500;s:17:"line_subtotal_tax";i:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}}";s:15:"applied_coupons";s:6:"a:0:{}";s:23:"coupon_discount_amounts";s:6:"a:0:{}";s:27:"coupon_discount_tax_amounts";s:6:"a:0:{}";s:21:"removed_cart_contents";s:6:"a:0:{}";s:19:"cart_contents_total";d:500;s:5:"total";i:0;s:8:"subtotal";i:500;s:15:"subtotal_ex_tax";i:500;s:9:"tax_total";i:0;s:5:"taxes";s:6:"a:0:{}";s:14:"shipping_taxes";s:6:"a:0:{}";s:13:"discount_cart";i:0;s:17:"discount_cart_tax";i:0;s:14:"shipping_total";i:0;s:18:"shipping_tax_total";i:0;s:9:"fee_total";i:0;s:4:"fees";s:6:"a:0:{}";}', 1465703998);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

DROP TABLE IF EXISTS `wp_woocommerce_tax_rates`;
CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) NOT NULL,
  `tax_rate_country` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

DROP TABLE IF EXISTS `wp_woocommerce_tax_rate_locations`;
CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) NOT NULL,
  `location_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_termmeta`
--

DROP TABLE IF EXISTS `wp_woocommerce_termmeta`;
CREATE TABLE `wp_woocommerce_termmeta` (
  `meta_id` bigint(20) NOT NULL,
  `woocommerce_term_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_termmeta`
--

INSERT INTO `wp_woocommerce_termmeta` (`meta_id`, `woocommerce_term_id`, `meta_key`, `meta_value`) VALUES
(1, 6, 'order', '0'),
(2, 6, 'display_type', ''),
(3, 6, 'thumbnail_id', '0'),
(4, 7, 'order', '0'),
(5, 7, 'display_type', ''),
(6, 7, 'thumbnail_id', '0'),
(7, 8, 'order', '0'),
(8, 8, 'display_type', ''),
(9, 8, 'thumbnail_id', '0'),
(10, 9, 'order', '0'),
(11, 9, 'display_type', ''),
(12, 9, 'thumbnail_id', '0'),
(13, 10, 'order', '0'),
(14, 10, 'display_type', ''),
(15, 10, 'thumbnail_id', '0'),
(16, 6, 'product_count_product_cat', '9'),
(17, 9, 'product_count_product_cat', '8'),
(18, 8, 'product_count_product_cat', '5'),
(19, 7, 'product_count_product_cat', '8'),
(20, 10, 'product_count_product_cat', '5'),
(21, 17, 'order', '0'),
(22, 17, 'display_type', ''),
(23, 17, 'thumbnail_id', '0'),
(24, 18, 'order', '0'),
(25, 18, 'display_type', ''),
(26, 18, 'thumbnail_id', '0'),
(27, 19, 'order', '0'),
(28, 19, 'display_type', 'subcategories'),
(29, 19, 'thumbnail_id', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_ai_contact`
--
ALTER TABLE `wp_ai_contact`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(191));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(191),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`(191)),
  ADD KEY `tax_rate_state` (`tax_rate_state`(191)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(191)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type` (`location_type`),
  ADD KEY `location_type_code` (`location_type`,`location_code`(90));

--
-- Indexes for table `wp_woocommerce_termmeta`
--
ALTER TABLE `wp_woocommerce_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `woocommerce_term_id` (`woocommerce_term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_ai_contact`
--
ALTER TABLE `wp_ai_contact`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1589;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=985;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_termmeta`
--
ALTER TABLE `wp_woocommerce_termmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
